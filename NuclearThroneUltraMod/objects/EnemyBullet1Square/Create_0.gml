event_inherited();
image_speed = 0.4;
alarm[2] = 1
onlyHitPlayerTeam = false;
typ = 3 //0 = nothing, 1 = deflectable, 2 = destructable 3 = immune to melee
dmg = 3
if scrIsHardMode()//HARD MODE
	dmg = 4;
depth = -2;
alarm[3] = 240;
blinkTime = 5;
draw = true;