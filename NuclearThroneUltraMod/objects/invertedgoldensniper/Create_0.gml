/// @description Init

// Inherit the parent event
event_inherited();
alarm[0] = 0;
tellTime -= 3;
actTime -= 4;
raddrop = 14
maxhealth = 20
my_health = maxhealth;
EnemyHealthAdjustments();
spr_idle = sprInvertedGoldSniperIdle
spr_walk = sprInvertedGoldSniperWalk
spr_hurt = sprInvertedGoldSniperHurt
spr_dead = sprInvertedGoldSniperDead