event_inherited();

spr_idle = sprCrown13IdleEnemy
spr_walk = sprCrown13WalkEnemy
spr_hurt = sprCrown13HurtEnemy
spr_dead = sprCrown13DeadEnemy
alarm[2] = 30;
alarm[3] = 55;
alarm[4] = 120;