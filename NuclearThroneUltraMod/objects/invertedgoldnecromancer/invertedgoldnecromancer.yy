{
  "$GMObject":"",
  "%Name":"InvertedGoldNecromancer",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":10,"eventType":7,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":2,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"InvertedGoldNecromancer",
  "overriddenProperties":[],
  "parent":{
    "name":"Labs",
    "path":"folders/Objects/Enemies/Labs.yy",
  },
  "parentObjectId":{
    "name":"Necromancer",
    "path":"objects/Necromancer/Necromancer.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprInvertedGoldNecromancerIdle",
    "path":"sprites/sprInvertedGoldNecromancerIdle/sprInvertedGoldNecromancerIdle.yy",
  },
  "spriteMaskId":{
    "name":"mskBandit",
    "path":"sprites/mskBandit/mskBandit.yy",
  },
  "visible":true,
}