event_inherited();
maxhealth = 16
EnemyHealthAdjustments();
spr_idle = sprTeapotMissileIdle
spr_walk = sprTeapotMissileIdle
spr_hurt = sprTeapotMissileHurt
spr_dead = sprScrapBossMissileDead
explodeOnHitTime = 50;
maxSpeed += 0.2;
scrTarget()

alarm[2] = 2;
alarm[0] = 30;