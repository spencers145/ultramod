/// @description Deflect
if sprite_index == sprSlugDisappear
	sprite_index = sprESlugDisappear;
else if sprite_index == sprSlugBulletHighDamage || sprite_index == sprSlugBullet
	sprite_index = sprESlugBullet;
