event_inherited();
friction = 0.3

typ = 2 //0 = normal, 1 = deflectable, 2 = destructable

offx = random(2)-1
offy = random(2)-1

alarm[0]=120;
alarm[11] = 1;
dmg = 17;
xprev = x;
yprev = y;
hitEntities = [];