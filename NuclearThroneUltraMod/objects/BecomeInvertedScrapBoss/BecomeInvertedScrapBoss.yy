{
  "$GMObject":"",
  "%Name":"BecomeInvertedScrapBoss",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":1,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"BecomeInvertedScrapBoss",
  "overriddenProperties":[],
  "parent":{
    "name":"Scrap",
    "path":"folders/Objects/Enemies/Boss/Scrap.yy",
  },
  "parentObjectId":{
    "name":"BecomeScrapBoss",
    "path":"objects/BecomeScrapBoss/BecomeScrapBoss.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprInvertedScrapBossSleep",
    "path":"sprites/sprInvertedScrapBossSleep/sprInvertedScrapBossSleep.yy",
  },
  "spriteMaskId":{
    "name":"mskScrapBoss",
    "path":"sprites/mskScrapBoss/mskScrapBoss.yy",
  },
  "visible":true,
}