ammo -= 1

alarm[0] = time

if instance_exists(creator)
{
x = creator.x
y = creator.y
	//FIRING
	//snd_play_fire(sndShotgun)

	repeat(8 - min(2,ammo))
	{
		with instance_create(x,y,Bullet7)
		{
			motion_add(point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+(random(50)-25)*other.creator.accuracy,10+other.ammo+random(6))
			image_angle = direction
			team = other.team
			scrCopyWeaponMod(other);
		}
	}
	BackCont.viewx2 += lengthdir_x(13,point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+180)*UberCont.opt_shake
	BackCont.viewy2 += lengthdir_y(13,point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+180)*UberCont.opt_shake
	BackCont.shake += 9
	creator.wkick = 8
	with creator
	{
		if object_index != Player || !skill_got[2]
		{
			scrMoveContactSolid(point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y) + 180,1);
			motion_add(point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+180,1)
		}	
	}
}


if ammo <= 0
	instance_destroy();