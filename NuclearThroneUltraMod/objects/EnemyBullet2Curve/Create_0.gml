/// @description Increase damage

// Inherit the parent event
event_inherited();
dmg += 1;
curveSpeed = 0.09;