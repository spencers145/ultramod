/// @description Init
event_inherited();
image_speed = 0.4;
friction = 0.1;
typ = 2;
dmg = 4;
projectilePush = 0.23;
hitEntities = [];
alarm[1] = 2;