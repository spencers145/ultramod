event_inherited();
image_speed = 0.4

if instance_exists(Player)
{
	if Player.skill_got[17] = 1
	{
		image_speed = max(0.18,0.25-(Player.betterlaserbrain*0.6))
		hits ++;
	}
}
dmg=25;
typ = 0 //0 = nothing, 1 = deflectable, 2 = destructable, 3 = deflectable

walled = 0
shk = 7;
