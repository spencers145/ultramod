/// @description Slower

// Inherit the parent event
event_inherited();
raddrop += 5;
maxhealth = 20;
maxSpeed -= 0.25;
acc -= 0.1;
actTime -= 5;
alarm[1] += 14
EnemyHealthAdjustments();
spr_idle = sprCursedSquareBotIdle;
spr_walk = sprCursedSquareBotWalk;
spr_hurt = sprCursedSquareBotHurt;
spr_dead = sprCursedSquareBotDead;
spr_fire = sprCursedSquareBotFire;
inRange = false;