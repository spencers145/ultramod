/// @description Other sprite

// Inherit the parent event
event_inherited();

spr_hide = sprInvertedGraveyardFishHide;
spr_rise = sprInvertedGraveyardFishRise;
alarm[0] -= 30;
maxSpeed = 7;
alarm[3] = 40;