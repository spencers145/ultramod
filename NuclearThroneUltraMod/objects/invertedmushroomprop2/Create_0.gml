/// @description Init
maxhealth = 4;
spr_idle = sprInvertedMushroomProp2
spr_hurt = sprInvertedMushroomProp2Hurt
spr_dead = sprInvertedMushroomProp2Dead
mySize = 1
// Inherit the parent event
event_inherited();

snd_hurt = sndHitPlant
shadowSprite = shd16;
