{
  "$GMObject":"",
  "%Name":"PopoSlowFieldFill",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":3,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":8,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"PopoSlowFieldFill",
  "overriddenProperties":[],
  "parent":{
    "name":"Ability",
    "path":"folders/Objects/Player/Ability.yy",
  },
  "parentObjectId":{
    "name":"All",
    "path":"objects/All/All.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprSlowFieldFill",
    "path":"sprites/sprSlowFieldFill/sprSlowFieldFill.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}