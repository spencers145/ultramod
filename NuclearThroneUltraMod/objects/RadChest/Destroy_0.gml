if my_health <= 0
{
with instance_create(x,y,MovingCorpse)
{
mySize = other.mySize
mask_index = other.mask_index
sprite_index = other.spr_dead
}
scrRaddrop();
if canOpenMind && instance_exists(Player)
{
	scrChestOpenMindReload(Player);
}
}

repeat(4)
{
with instance_create(x,y,Smoke)
motion_add(random(360),random(3))
}
instance_create(x,y,ExploderExplo)
if !instance_exists(GenCont)
snd_play(sndEXPChest,0,true)
Sleep(1)


scrChestOasis();

