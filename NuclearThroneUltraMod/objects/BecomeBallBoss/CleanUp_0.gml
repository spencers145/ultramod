/// @description Stop loop sound
if audio_is_playing(sndSheepLoop)
	audio_stop_sound(sndSheepLoop);
