/// @description zigzagoffset

// Inherit the parent event
event_inherited();

offset = 1;
angle = 90;
time = 1;
tdir = 0.1;
alarm[3] = 1;