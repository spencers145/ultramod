/// @description Yes

// Inherit the parent event
event_inherited();
image_speed = 0.3;
image_index = irandom(image_number);
if instance_exists(Player) && Player.area == 123
	sprite_index = sprInvertedLeaf
else if instance_exists(BackCont) && BackCont.area == 123
	sprite_index = sprInvertedLeaf;