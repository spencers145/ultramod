if image_index == 1
{
	snd_play_2d(sndClick);
with option2
instance_destroy();
UberCont.optY = 0;
selected = 1
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+8,__view_get( e__VW.YView, 0 )+32,MusVolSlider)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+8,__view_get( e__VW.YView, 0 )+40,SfxVolSlider)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+8,__view_get( e__VW.YView, 0 )+48,AmbVolSlider)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+8,__view_get( e__VW.YView, 0 )+56,ThreeDAudioToggle)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+72,FullScreenToggle)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("00")),__view_get( e__VW.YView, 0 )+80,CursorUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("0")),__view_get( e__VW.YView, 0 )+88,SideArtUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("16X")),__view_get( e__VW.YView, 0 )+96,ResolutionScaleUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+104,DamageIndicatorToggle)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+112,CameraFollowToggle)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+120,HighQualityToggle)
//instance_create(view_xview+view_wview/2+10+string_width("OFF"),view_yvi*0.5+136,GamePadToggle)
//instance_create(view_xview+view_wview/2+10+string_width("100%"),view_yv*0.5w+144,AutoAimUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("100%")),__view_get( e__VW.YView, 0 )+166,ShakeUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("100%")),__view_get( e__VW.YView, 0 )+174,FreezeFrameUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("100%")),__view_get( e__VW.YView, 0 )+182,LoadingScreenSpeedUpDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+190,MouseCPToggle)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+198,BossIntroToggle);
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+206,TimerToggle);
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+214,FPSToggle);
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+222,DeleteSave);
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5+10+string_width(string_hash_to_newline("OFF")),__view_get( e__VW.YView, 0 )+230,ToggleRemapper);
var yy = 0;
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+40,KeyUp)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+48,KeyDown)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+56,KeyLeft)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+64,KeyRight)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+72,KeySwap)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )*0.5,__view_get( e__VW.YView, 0 )+yy+80,KeyPickup)
instance_create(__view_get( e__VW.XView, 0 )+__view_get( e__VW.WView, 0 )-__view_get( e__VW.WView, 0 )/4,__view_get( e__VW.YView, 0 )+yy+40,KeyReset)


}
else if StatsSelect.selected = 1 or CreditsSelect.selected = 1  or OptionSelect2.selected = 1 or UpdateChecker.selected = 1
{
alarm[0] = 2;
with option
	instance_destroy()
}