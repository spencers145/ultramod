maxhealth = 10

spr_idle = sprInvertedToxicMushroomProp
spr_hurt = sprInvertedToxicMushroomPropHurt
spr_dead = sprInvertedToxicMushroomPropDead
mySize = 1

event_inherited()

prevhealth = my_health;
snd_hurt = sndHitPlant

alarm[0] = 15;