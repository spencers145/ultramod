/// @description Spreading more fire
if instance_exists(Player) && Player.skill_got[43]
{
	scrMoodSwingFlameSpread(64, dmg - 2);
}