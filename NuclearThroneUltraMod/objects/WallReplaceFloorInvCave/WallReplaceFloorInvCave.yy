{
  "$GMObject":"",
  "%Name":"WallReplaceFloorInvCave",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"WallReplaceFloorInvCave",
  "overriddenProperties":[],
  "parent":{
    "name":"SurvivalArena",
    "path":"folders/Objects/SurvivalArena.yy",
  },
  "parentObjectId":{
    "name":"WallReplaceFloor",
    "path":"objects/WallReplaceFloor/WallReplaceFloor.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"mskWallFloorReplacer",
    "path":"sprites/mskWallFloorReplacer/mskWallFloorReplacer.yy",
  },
  "spriteMaskId":{
    "name":"mskWallFloorReplacer",
    "path":"sprites/mskWallFloorReplacer/mskWallFloorReplacer.yy",
  },
  "visible":false,
}