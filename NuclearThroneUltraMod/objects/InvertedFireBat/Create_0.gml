raddrop = 12
maxhealth = 9
meleedamage = 2
mySize = 1
event_inherited()
fireProof = true;
spr_idle = sprInvertedFireBatIdle
spr_walk = sprInvertedFireBatIdle
spr_hurt = sprInvertedFireBatHurt
spr_dead = sprInvertedFireBatDead
spr_fire = sprInvertedFireBatFire

snd_melee = sndGoldScorpionMelee;
snd_dead = sndFrogExplode

walk=0;
dodge=0;
//behavior
alarm[1] = 10+random(10)

if instance_exists(Player)
motion_add(point_direction(Player.x,Player.y,x,y),1)

canDodge = false;
actTime = 14;
nukeIt = false;
loops = GetPlayerLoops();
if loops > 0
	actTime = 10;
if loops > 12
	nukeIt = true;