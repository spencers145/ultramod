/// @description Init

// Inherit the parent event
event_inherited();

alarm[0] = 0;
raddrop = 30
maxhealth = 120
my_health = maxhealth;
EnemyHealthAdjustments();
meleedamage = 2

spr_idle = sprGoldJungleFlyIdle
spr_walk = sprGoldJungleFlyWalk
spr_hurt = sprGoldJungleFlyHurt
spr_dead = sprGoldJungleFlyDead
snd_hurt = sndGoldFlyHurt
maxAmmo = 12;
firerate = 1;
actTime = 4;
acc = 3;
constAcc = 1.1;