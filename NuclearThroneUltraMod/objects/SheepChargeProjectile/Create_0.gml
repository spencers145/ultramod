/// @description 0 image_speed

// Inherit the parent event
event_inherited();
image_speed = 0;
bloomSprite = sprSheepDashBloom;
sheepPower = 1;
alpha = 1;
maxAlarm = 2;
snd_play(sndSheepChargeFire,0.3);
imageIndex = 0;
friction = 0.2;