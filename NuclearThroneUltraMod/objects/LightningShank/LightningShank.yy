{
  "$GMObject":"",
  "%Name":"LightningShank",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":11,"eventType":2,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":10,"eventType":7,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"LightningShank",
  "overriddenProperties":[],
  "parent":{
    "name":"Melee",
    "path":"folders/Objects/Projectiles/Melee.yy",
  },
  "parentObjectId":{
    "name":"ShankParent",
    "path":"objects/ShankParent/ShankParent.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprLightningShank",
    "path":"sprites/sprLightningShank/sprLightningShank.yy",
  },
  "spriteMaskId":{
    "name":"mskShank",
    "path":"sprites/mskShank/mskShank.yy",
  },
  "visible":true,
}