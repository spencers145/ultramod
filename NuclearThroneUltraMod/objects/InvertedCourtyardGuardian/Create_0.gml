event_inherited();
raddrop = 9
maxhealth = 9;
EnemyHealthAdjustments();

spr_idle = sprInvertedCourtyardGuardianIdle
spr_walk = sprInvertedCourtyardGuardianIdle
spr_hurt = sprInvertedCourtyardGuardianHurt
spr_fire = sprInvertedCourtyardGuardianFire
spr_dead = sprInvertedCourtyardGuardianDead


//behavior
actTime = 13;
acc = 1.2;
projectileSpeed = 3.3;
maxSpeed = 5;

maxammo = 4;
ammo = maxammo;
angleStep = 360/maxammo;
distance = 126;
originX = x;
originY = y;