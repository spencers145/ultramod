/// @description Revive with given position
instance_create(tx,ty,ReviveFX)
with instance_create(tx,ty,Freak)
{
	droprate = 4;
	existTime = 30;
	raddrop = 0;
	countKill = false;
	wasResurrected = true;
}