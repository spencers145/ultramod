/// @description Make it golden

// Inherit the parent event
event_inherited();

spr_idle = sprGoldSheepIdle;
spr_idle_b = sprGoldSheepIdleB;
spr_walk = sprGoldSheepWalk;
spr_hurt = sprGoldSheepHurt;
spr_dead = sprGoldSheepDead;