/// @description Init

// Inherit the parent event
event_inherited();

spr_idle = sprInvertedGoldenSnowTankIdle
spr_walk = sprInvertedGoldenSnowTankWalk
spr_hurt = sprInvertedGoldenSnowTankHurt
spr_dead = sprInvertedGoldenSnowTankDead

raddrop = 20
maxhealth = 60
EnemyHealthAdjustments();
maxSpeed = 2;
isInverted = true;