/// @description Inherit

// Inherit the parent event
event_inherited();
alarm[11] = 0;
alarm[5] = 1;
angle = direction;
angleDir = choose(20,-20);
maxSpeed += 4;
acc += 0.5;
alarm[6] = 180;