with Floor
{
if !position_meeting(x-16,y-16,Floor) instance_create(x-16,y-16,Wall)
if !position_meeting(x,y-16,Floor) instance_create(x,y-16,Wall)
if !position_meeting(x+16,y-16,Floor) instance_create(x+16,y-16,Wall)
if !position_meeting(x+32,y-16,Floor) instance_create(x+32,y-16,Wall)
if !position_meeting(x+32,y,Floor) instance_create(x+32,y,Wall)
if !position_meeting(x+32,y+16,Floor) instance_create(x+32,y+16,Wall)
if !position_meeting(x-16,y,Floor) instance_create(x-16,y,Wall)
if !position_meeting(x-16,y+16,Floor) instance_create(x-16,y+16,Wall)
if !position_meeting(x-16,y+32,Floor) instance_create(x-16,y+32,Wall)
if !position_meeting(x,y+32,Floor) instance_create(x,y+32,Wall)
if !position_meeting(x+16,y+32,Floor) instance_create(x+16,y+32,Wall)
if !position_meeting(x+32,y+32,Floor) instance_create(x+32,y+32,Wall)
}
room_speed = 30;
spawnarea = 0
with Floor
{
if random(12) < 1
instance_create(x+16,y+16,NightCactus)
}

with RadChest
instance_destroy()
with WeaponChest
instance_destroy()
with AmmoChest
instance_destroy()
with BigWeaponChest
instance_destroy()
with ChestOpen
instance_destroy()

if instance_exists(Vlambeer)
	with Vlambeer
	{
		mode = 1
		alarm[0] = 30
	}