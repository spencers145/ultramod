{
  "$GMObject":"",
  "%Name":"ShopWep5",
  "eventList":[],
  "managed":true,
  "name":"ShopWep5",
  "overriddenProperties":[],
  "parent":{
    "name":"Shops",
    "path":"folders/Objects/Player/Ability/Shops.yy",
  },
  "parentObjectId":{
    "name":"ShopWep",
    "path":"objects/ShopWep/ShopWep.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprShopMenuWep5",
    "path":"sprites/sprShopMenuWep5/sprShopMenuWep5.yy",
  },
  "spriteMaskId":{
    "name":"mskShopEliteWepChest",
    "path":"sprites/mskShopEliteWepChest/mskShopEliteWepChest.yy",
  },
  "visible":true,
}