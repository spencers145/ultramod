/// @description init
event_inherited();
aimOffset = 15;
ultramodded = false;
accuracy = 1;
shake = 3;
if instance_exists(Player)
	accuracy = Player.accuracy;