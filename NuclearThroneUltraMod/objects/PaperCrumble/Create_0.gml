/// @description Increase the damage

// Inherit the parent event
event_inherited();
dist = - 90;
dmg = 10;
rotation = irandom_range(25,45) * choose(1,-1);
friction = 0.15;