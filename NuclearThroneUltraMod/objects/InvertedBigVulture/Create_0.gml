/// @description Init

// Inherit the parent event
event_inherited();
raddrop = 150;

spr_idle = sprInvertedBigVultureIdle;
spr_walk = sprInvertedBigVultureWalk;
spr_hurt = sprInvertedBigVultureHurt;
spr_eat = sprInvertedBigVultureEat;
spr_dead = sprInvertedBigVultureDead

projectileSpeed += 1;
isInverted = true;
raddrop = 80
maxhealth = 130;
scrBossHealthBuff();
EnemyHealthAdjustments();
maxAmmo = 25;
fireRate = 1;
fireRate2 = 5;
chargeSpeed = 4.8;
maxChargeSpeed = 18;
maxSpeed = 3.2;
