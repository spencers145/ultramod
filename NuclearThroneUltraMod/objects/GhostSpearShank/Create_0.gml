event_inherited();
image_speed = 0.3

typ = 0 //0 = nothing, 1 = deflectable, 2 = destructable, 3 = deflectable

walled = 0
friction = 0.1

snd_wallhit=sndMeleeWall;
snd_hit=sndHitWall;
longarms = 0;
dmg=11;
wallPierce = 0;
alarm[4] = 6;