/// @description Init

// Inherit the parent event
event_inherited();
raddrop += 20;
maxhealth = 40;
maxSpeed += 0.5;
acc += 0.2;
actTime -= 40;
projectileSpeed += 2;
EnemyHealthAdjustments();
spr_idle = sprUltraOctaBotIdle;
spr_walk = sprUltraOctaBotWalk;
spr_hurt = sprUltraOctaBotHurt;
spr_dead = sprUltraOctaBotDead;
spr_fire = sprUltraOctaBotFire;