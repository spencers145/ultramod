/// @description Adjust

// Inherit the parent event
event_inherited();
nomscale -= 0.2;
wallScale += 0.2;
hitShrink += 0.5;
acc += 5;
maxSpeed += 4;
ptime = 3;
dmg -= 1;