/// @description Cancel bolt

// Inherit the parent event
event_inherited();
if other.team != team
	alarm[2] = 0;