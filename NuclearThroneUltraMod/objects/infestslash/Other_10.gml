/// @description Do stuff
var hitGuy = hitEntities[array_length(hitEntities)-1];
hitEnemy = true;
alarm[2] = 0;
with hitGuy
{
	BackCont.shake += 5
	snd_play(sndMeatExplo,0.1,true);
	instance_create(x,y,MeatExplosion);
	snd_play(sndTermite,0.3,true)
	var ang = random(360);
	var am = 2;
	var angStep = 360/am;
	repeat(am)
	{
		with instance_create(x,y,Termite)
		{
			motion_add(ang,4+irandom(8))
			team = other.team
		}
		ang += angStep;
	}

	ang = random(360);

	with instance_create(x,y,BloodStreak)
	image_angle = ang

	with instance_create(x,y,BloodStreak)
	image_angle = ang+120

	with instance_create(x,y,BloodStreak)
	image_angle = ang+240
		
}