{
  "$GMObject":"",
  "%Name":"SuperLightningCannonBall",
  "eventList":[
    {"$GMEvent":"","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":1,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"SuperLightningCannonBall",
  "overriddenProperties":[],
  "parent":{
    "name":"Projectiles",
    "path":"folders/Objects/Projectiles.yy",
  },
  "parentObjectId":{
    "name":"LightningCannonBall",
    "path":"objects/LightningCannonBall/LightningCannonBall.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprSuperLightningBall",
    "path":"sprites/sprSuperLightningBall/sprSuperLightningBall.yy",
  },
  "spriteMaskId":{
    "name":"mskFlakBullet",
    "path":"sprites/mskFlakBullet/mskFlakBullet.yy",
  },
  "visible":true,
}