/// @description Fix bones
if instance_number(Spiral) ==  4
{
	with Bones
	{
		depth = y*-1;
	}
}