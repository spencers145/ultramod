event_inherited()
raddrop = 9
maxhealth = 9
EnemyHealthAdjustments();
spr_idle = sprInvertedDiscGuyIdle
spr_walk = sprInvertedDiscGuyWalk
spr_hurt = sprInvertedDiscGuyHurt
spr_dead = sprInvertedDiscGuyDead
spr_fire = sprInvertedDiscGuyFire
actTime -= 1;
discSpeed = 5;
range = 260;
maxSpeed = 3.2;
acc = 1;