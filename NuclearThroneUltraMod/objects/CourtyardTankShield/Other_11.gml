/// @description Disable
if sprite_index != spr_dissapear
{
	sprite_index = spr_dissapear;
	image_index = 0;
	image_speed = 0.4;
	alarm[0] = 40;
}