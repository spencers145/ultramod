event_inherited();
distance = 32;
creator = noone;
image_speed = 0;
spr_default = sprCourtyardTankShield;
spr_deflect = sprCourtyardTankShieldDeflect;
spr_dissapear = sprCourtyardTankShieldDissappear;
spr_spawn = sprCourtyardTankShieldAppear;