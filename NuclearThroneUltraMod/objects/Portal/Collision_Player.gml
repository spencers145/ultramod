if sprite_index == sprProtoPortalDormant || sprite_index == sprInvertedPortalDormant
	exit;
if sprite_index != sprPortalSpawn && sprite_index != sprPinkPortalSpawn
{
if !instance_exists(PlayerInPortal)
{
	other.visible = false;
	with instance_create(x,y,PlayerInPortal)
	{
		depth = min(other.depth - 1,Player.depth);
		image_speed = 0.4;
		image_angle = Player.angle;
		right = Player.right;
		sprite_index = Player.spr_hurt;
	}
}
//in portal don't decrease skill
if other.race == 26
{
	with PlayerAlarms
	{
		alarm[6] += 1;
	}
}
if other.race == 15//Atom can no longer teleport
{
	with PlayerAlarms
	{
		alarm[8] += 200;
	}
}
with Ally
{
	my_health = 0;
}
with TinyKraken
{
	my_health = 0;
}
with YungCuzDupe
{
	my_health = 0;
	isAlkaline = false;
	strongspirit = false;
}
other.speed = 0;
if endgame = 100
{
snd_play(sndPortalCloseShort);

endgame = 12;//originally 30


}
}
if inverted{
with other
inverted=true//take me to the inverted universe
}

if Player.area == 8 && Player.subarea < 2 && ( instance_exists(Sheep))
{
	Player.banditland=true;
}
if instance_exists(CrownPed) && !closedTheVault
{
	//Close up the survival arena
	with Floor
	{
		if sprite_index == sprFloor100D
		{
			instance_destroy(id,false);
			var ar = Player.area;
			Player.area = 100;
			instance_create(x,y,Wall)
			instance_create(x+16,y,Wall);
			instance_create(x+16,y+16,Wall);
			instance_create(x+16,y+16,Wall);
			instance_create(x,y+16,Wall);
			Player.area = ar;
		}
	}
	closedTheVault = true;
}
with other
{
	alarm[3] = max(alarm[3],3);	
}

