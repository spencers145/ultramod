raddrop = 40
maxhealth = 86
meleedamage = 0
mySize = 3

var loops = GetPlayerLoops();
maxhealth += clamp(18*loops,0,200);
scrBossHealthBuff();

event_inherited()

if instance_exists(Player){
my_health=round( (1+(Player.loops*0.2))*my_health )//bandit loop 1: 1.1*4= 5(rounded up)
maxhealth=my_health;
}

spr_idle = sprInvertedBanditBossIdle
spr_walk = sprInvertedBanditBossWalk
spr_hurt = sprInvertedBanditBossHurt
spr_dead = sprInvertedBanditBossDead
spr_fire = sprInvertedBanditBossFire


snd_hurt = sndBigBanditHit
snd_dead = sndBigBanditDie

//behavior
ammo = 10
walk = 0
shot = 0
chargewait = 2
charge = 0
gunangle = random(360)
alarm[1] = 1
intro = 0
wkick = 0

scrTarget()
if target != noone && instance_exists(target)
gunangle = point_direction(x,y,target.x,target.y)

sndhalfhp = 0
sndtaunt = 0
tauntdelay = 0

oasis=false;

cam=true

with InvertedBanditBoss
{
if id!=other.id
other.cam=false;
}

if cam
instance_create(x,y,DramaCamera);

scrAddDrops(2);

if instance_exists(Player) && Player.skill_got[29]
{
	alarm[1]+=45;
	scrGiveSnooze();
}
instance_create(x,y,WallBreak);