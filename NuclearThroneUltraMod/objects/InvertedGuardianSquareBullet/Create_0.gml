/// @description zigzag offset

// Inherit the parent event
event_inherited();
offset = 1;
angle = 90;
time = 0;
tdir = 0.1;
alarm[4] = 1;
// alarm[3] += 60;