image_speed=0;
projectileHitBrake = 1.8;
enemyHitbrake = 5;
bloomSprite = sprSheepDashBloom;
imageIndex = 0;
baseDmg = 1.2;
dmg = baseDmg;
if instance_exists(Player)
{
	if scrIsHardMode()
		baseDmg += 0.1;
	var damageBoost = max(1,Player.skill_got[8]*1.1) + frac(Player.skill_got[5] * 1.2);
	dmg = baseDmg*damageBoost;
	if Player.skill_got[2]//Extra feet synergy why not
	{
		projectileHitBrake-= 0.13;
		enemyHitbrake -= 0.8;
		bloomSprite = sprSheepDashBloomExtraFeet;
		if (Player.skill_got[8])
		{
			bloomSprite = sprSheepDashBloomExtraFeetGammaGuts;
		}
	}
	else if  (Player.skill_got[8])
	{
		bloomSprite = sprSheepDashBloomGammaGuts;
	}
	if Player.skill_got[5]
	{
		projectileHitBrake -= 0.13;
		enemyHitbrake -= 0.8;
	}
    if Player.ultra_got[51]
    {
		sprite_index=sprSheepDashFXultra;
		projectileHitBrake-= 0.2;
		enemyHitbrake -= 1.4;
		dmg = baseDmg*damageBoost;
    }
    if Player.skill_got[5]==1
    {
		sprite_index=sprSheepDashFXthrn;
    }
    if Player.skill_got[5] && Player.ultra_got[51]
    {
		sprite_index=sprSheepDashFXultrathrn;
    }
}else{instance_destroy();}

alpha = 0;

maxReach = 0;
maxScale = 1;
thresholdBroken = false;
//TODO make this speed based
/*
	Where enemies and projectiles and walls slow you down
*

