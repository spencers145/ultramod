if !instance_exists(ShopWheel)
	instance_destroy();


if place_meeting(x,y,ShopSelector)
{
	image_index=1;
	if instance_exists(Player)
	{
		if ShopWheel.alarm[0] > 0
		{
			var i = index;
			with Player
			{
				if !(bwep == 0 && other.wep == 0) && KeyCont.key_spec[p] != 1 && KeyCont.key_spec[p] != 2// && hogWep[i] != 0
				{
					//Grab weapon (LMB)
					if bwep == 0
					{
						scrSwapWeps();	
					}
					var hwep = wep;
					var hcurse = curse;
					var hwm1 = wepmod1;
					var hwm2 = wepmod2;
					var hwm3 = wepmod3;
					var hwm4 = wepmod4;
					var hr = reload;
					var hq = queueshot;
					wep = hogWep[i]
					curse = hogCurse[i]
					wepmod1 = hogWepmod1[i]
					wepmod2 = hogWepmod2[i]
					wepmod3 = hogWepmod3[i]
					wepmod4 = hogWepmod4[i]
					isPermanent = hogIsPermanent[i];
					hasBeenEaten = hogHasBeenEaten[i];
					reload = hogReload[i];
					if wep != 0
					{
						snd_play_2d(wep_swap[wep])
						//reload = max(reload,wep_load[wep]);//Add reload time
					}
					else
					{
						snd_play_2d(wep_swap[hwep])
						scrSwapWeps();
					}
					scrWeaponHold();
					other.wep = hwep;
					other.reload = hr;
					other.queueshot = hq;
					hogWep[i] = hwep;
					hogCurse[i]	= hcurse;
					hogWepmod1[i] = hwm1;
					hogWepmod2[i] = hwm2;
					hogWepmod3[i] = hwm3;
					hogWepmod4[i] = hwm4;
					hogReload[i] = hr;
					hogQueueshot[i] = hq;
				}
			}
			instance_destroy();
		}
	}
}
else
	image_index=0;

