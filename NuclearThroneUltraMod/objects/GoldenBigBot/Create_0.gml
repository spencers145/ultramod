/// @description Golden!

// Inherit the parent event
event_inherited();
raddrop += 2;
fireDelay = 40;
maxhealth = 90;
maxSpeed -= 0.25;
acc += 0.2;
actTime -= 2;
projectileSpeed += 3;
EnemyHealthAdjustments();

spr_idle = sprGoldenBigBotIdle;
spr_walk = sprGoldenBigBotWalk;
spr_hurt = sprGoldenBigBotHurt;
spr_dead = sprGoldenBigBotDead;
spr_fire = sprGoldenBigBotFire;