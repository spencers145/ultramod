/// @description Less damage and can curve

// Inherit the parent event
event_inherited();

dmg = 2;
curveSpeed = -0.9;
alarm[2] = 5;