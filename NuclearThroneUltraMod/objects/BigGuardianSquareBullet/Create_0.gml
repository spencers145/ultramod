/// @description Typ 3

// Inherit the parent event
event_inherited();
dmg = 10;
typ = 3;
projectileToSpawn = GuardianBulletSpawn;
projectileToSpawnSprite = sprGuardianSquareBulletSpawn;
hitSprite = sprGuardianBulletHit;

scrInitDrops(1);