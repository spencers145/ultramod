event_inherited();
friction = 0;
typ = 2 //0 = normal, 1 = deflectable, 2 = destructable
hitEntities = [];
dmg = 30;
dmgAdd = 5;
trailColour = make_colour_rgb(0,255,0);
xprev = x;
yprev = y;
colX = x;
colY = y;
alarm[3] = 1;
alarm[4] = 120;