event_inherited();
friction = 0;
scale = 2;
image_xscale = scale;
image_yscale = scale;
rotationSpeed = 20;
growRate = 0.16;
bounce = false;
alarm[4] = 2;