alarm[1] = 10+random(8)
canDodge = true;
if loop
	alarm[1] -= 7;
scrTarget()
if target != noone
{
motion_add(point_direction(x,y,target.x,target.y),3);
//HAS A TARGET
var dis = point_distance(x,y,target.x,target.y)
if random(35) < 1 or (dis < 64 and random(5) < 1) or (dis > 160 and random(16) < 1)
{
//FLY

sprite_index = sprInvertedLilHunterLiftStart
image_index = 0
with BoltStick
	instance_destroy();
instance_change(InvertedLilHunterFly,false)
snd_play_2d(sndLilHunterLaunch)
	if random(6) < 1
	{
		snd_play_2d(sndLilHunterSummon)
		with Player
		{
			instance_create(x,y,IDPDSpawn);
		}	
	}
}
else
{
//DON'T FLY
if collision_line(x,y,target.x,target.y,Wall,0,0) < 0 || (dis < 250 && random(10) < 2)
{
//CAN SEE

if random(2) < 1
{
//FIRE

if point_distance(x,y,target.x,target.y) < 96 && random(2)<1
{
	//CLOSE RANGE
	gunangle = point_direction(x,y,target.x,target.y)+random(60)-30

	snd_play(sndEnemyFire)
	wkick = 8
	ang=gunangle-80;
	var r = irandom(min(8,loop))
	var angStep = 160/(6+r);
	repeat(6+r)
	{
		with instance_create(x,y,EnemyBullet1)
		{
			motion_add(other.ang,3)
			image_angle = direction
			team = other.team
		}
		ang+=angStep;
	}
}
else if random(4)<1
{
    //BOUNCER BULLETS circle
    gunangle = point_direction(x,y,target.x,target.y)+random(14)-7+30
    
    snd_play(sndBouncerShotgun)
    wkick = 8
	ang = gunangle - 80;
	var r = irandom(min(5,loop))
	var angStep = 160/(5+r);
	repeat(5+r)
	{
		with instance_create(x,y,EnemyBouncerBullet)
		{
			motion_add(other.ang,4)
			image_angle = direction
			team = other.team
		}
		ang+=angStep;
	}
	alarm[1] += 6;
    alarm[4] = 4;

}
else if point_distance(x,y,target.x,target.y) > 130 && random(4)<1
{
	//LONG SNIPE MISSILE
	gunangle = point_direction(x,y,target.x,target.y)+random(10)-5

	snd_play(sndEnemyFire)
	wkick = 8
    
    with instance_create(x,y,EnemyMissile)
    {
	    motion_add(other.gunangle,2+random(2))
	    image_angle = direction
	    team = other.team
    }
    
}
else
{
	//LONG RANGE
	gunangle = point_direction(x,y,target.x,target.y)+random(36)-18
	snd_play(sndEnemyFire)
	snd_play(sndEraser);
	wkick = 8
	var s = 5.5;
	repeat(10+irandom(min(loop,3)))
	{
		with instance_create(x,y,EnemyBullet5)
		{
			motion_add(other.gunangle,s)
			image_angle = direction
			team = other.team
		}
		s+= 0.5;
	}
}


}
else
{
//WALK
direction = point_direction(x,y,target.x,target.y)+random(20)-10
speed = 0.4
walk = 8+random(4)
gunangle = point_direction(x,y,target.x,target.y)
}
}
else if ((random(10) < 2 && !instance_exists(IDPDSpawn)) || (random(20) < 1))
{
	snd_play_2d(sndLilHunterSummon)
	with Player
	{
		instance_create(x,y,IDPDSpawn);
	}
}
else if random(30)<1
{

//CAN'T SEE FLY AWAY FLY

sprite_index = sprInvertedLilHunterLiftStart
image_index = 0
with BoltStick
	instance_destroy();
instance_change(InvertedLilHunterFly,false)
snd_play_2d(sndLilHunterLaunch)

}
else
{
//HAS NO TARGET
direction = point_direction(target.x,target.y,x,y)+random(20)-10
speed = 0.4
walk = 40+random(10)
gunangle = point_direction(x,y,target.x,target.y)
}

if target.x < x
right = -1
else if target.x > x
right = 1
}
}
else if random(10) < 1
{
//NO TARGET
motion_add(random(360),0.4)
walk = 10+random(4)
alarm[1] = walk+random(30)
gunangle = direction
if hspeed > 0
right = 1
else if hspeed < 0
right = -1
}

if walk > 0
motion_add(point_direction(UberCont.mouse__x,UberCont.mouse__y,x,y),0.3)

