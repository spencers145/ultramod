event_inherited()
raddrop = 8
maxhealth = 12
EnemyHealthAdjustments();
wakeTime = 10;
wakeObject = JungleAssassin;
wakeSound = sndJungleAssassinWake;
team = 1
target = -1
spr_idle = sprite_index
spr_hurt = sprJungleAssassinHurt
spr_dead = sprJungleAssassinDead
spr_walk = sprite_index
snd_hurt = sndJungleAssassinHurt
snd_dead = sndJungleAssassinDead
