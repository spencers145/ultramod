/// @description Collision
var chests = ds_list_create();
var al = instance_place_list(x,y,chestprop,chests,false);
	for (var i = 0; i < al; i++) {
		with chests[| i] {
			motion_add(point_direction(other.x,other.y,x,y),2);	
		}
	}
ds_list_destroy(chests);
alarm[5] = 5;