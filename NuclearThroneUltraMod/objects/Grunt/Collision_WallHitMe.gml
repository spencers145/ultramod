/// @description Roll bounce

// Inherit the parent event
if roll
{
	move_bounce_solid(false)
}
else
{
	event_inherited();
}
