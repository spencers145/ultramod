event_inherited();
friction = 0;
typ = 2 //0 = normal, 1 = deflectable, 2 = destructable
hitEntities = [];
dmg = 60;
dmgAdd = 5;
trailColour = make_colour_rgb(0,0,0);
xprev = x;
yprev = y;
colX = x;
colY = y;
alarm[3] = 1;
alarm[2] = 120;
canBreakWalls = 2;