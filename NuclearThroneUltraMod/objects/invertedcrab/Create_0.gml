/// @description Init

// Inherit the parent event
event_inherited();
maxAmmo = 10;
raddrop = 24
maxhealth = 9//13
spr_idle = sprInvertedCrabIdle
spr_walk = sprInvertedCrabWalk
spr_hurt = sprInvertedCrabHurt
spr_dead = sprInvertedCrabDead
spr_fire = sprInvertedCrabFire
actTime = 9;