event_inherited();
/*THIS IS HOW IT SHOULD WORK

damagers have a DAMAGE
their collision can be NORMAL, PIERCING or PIERCING AT OVERKILL (piercing checks per frame)
their type can be 0, DEFLECTABLE, DESTRUCTABLE or DEFLECTORS
they have a FORCE and can be 0 or DIRECTIONAL */

typ = 3//0 = normal, 1 = deflectable, 2 = destructable, 3 = deflectable
bskin=0;
dmg = 4;
if instance_exists(Player)
{
	if Player.bskin == 1
	{
		sprite_index = sprHorrorBBullet;
	}
	else if Player.bskin == 2
	{
		sprite_index = sprHorrorCBullet;
	}
}
cost = 0.5;
norecycle = choose(true,false);
radcost = 0;