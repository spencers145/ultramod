/// @description init
event_inherited();
aimOffset = 0;
accuracy = 1;
if instance_exists(Player)
	accuracy = Player.accuracy;