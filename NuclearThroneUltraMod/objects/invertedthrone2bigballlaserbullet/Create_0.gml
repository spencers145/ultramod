/// @description Init
/// @description Init

// Inherit the parent event
event_inherited();
projectileToSpawn = InvertedExploGuardianBullet;
projectileToSpawnSprite = sprInvertedGuardianBulletSpawn;
hitSprite = sprInvertedGuardianBulletHit;