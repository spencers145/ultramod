/// @description Change some functionality

// Inherit the parent event
event_inherited();
raddrop = 160
maxhealth = 800
scrBossHealthBuff();
EnemyHealthAdjustments();
easyCrystalLoss = false;
normalCrystal = LightningCrystal;
goldNormalCrystal = InvertedGoldCrystal;
goldAltCrystal = InvertedGoldCrystal;

spr_idle = sprInvertedHyperCrystalIdle
spr_walk = sprInvertedHyperCrystalIdle
spr_hurt = sprInvertedHyperCrystalHurt
spr_dead = sprInvertedHyperCrystalDead
spr_fire = sprHyperCrystalFire
//altCrystal = CursedCrystal;