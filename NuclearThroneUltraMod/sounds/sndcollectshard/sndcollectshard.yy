{
  "$GMSound":"",
  "%Name":"sndCollectShard",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":2.126406,
  "name":"sndCollectShard",
  "parent":{
    "name":"Menu",
    "path":"folders/Sounds/Menu.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndcollectshard.ogg",
  "type":0,
  "volume":1.0,
}