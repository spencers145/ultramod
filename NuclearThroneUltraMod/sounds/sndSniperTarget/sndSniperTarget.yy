{
  "$GMSound":"",
  "%Name":"sndSniperTarget",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.0,
  "name":"sndSniperTarget",
  "parent":{
    "name":"Regular",
    "path":"folders/Sounds/Enemies/Regular.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndSniperTarget",
  "type":0,
  "volume":1.0,
}