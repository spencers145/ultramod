{
  "$GMSound":"",
  "%Name":"sndPlantTBKill",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.343344,
  "name":"sndPlantTBKill",
  "parent":{
    "name":"Player",
    "path":"folders/Sounds/Player.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndPlantTBKill.wav",
  "type":0,
  "volume":1.0,
}