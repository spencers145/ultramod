{
  "$GMSound":"",
  "%Name":"sndBigVultureDeath",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":1.506395,
  "name":"sndBigVultureDeath",
  "parent":{
    "name":"BigVulture",
    "path":"folders/Sounds/Enemies/Boss/BigVulture.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBigVultureDeath.wav",
  "type":0,
  "volume":1.0,
}