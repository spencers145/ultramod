{
  "$GMSound":"",
  "%Name":"sndSledgeHit",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.256031,
  "name":"sndSledgeHit",
  "parent":{
    "name":"Melee",
    "path":"folders/Sounds/Weapons/Melee.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndSledgeHit",
  "type":0,
  "volume":1.0,
}