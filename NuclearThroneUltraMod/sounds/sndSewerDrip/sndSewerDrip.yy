{
  "$GMSound":"",
  "%Name":"sndSewerDrip",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndSewerDrip",
  "parent":{
    "name":"Enviroment",
    "path":"folders/Sounds/Enviroment.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndSewerDrip",
  "type":0,
  "volume":0.8,
}