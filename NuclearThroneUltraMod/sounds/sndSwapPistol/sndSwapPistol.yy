{
  "$GMSound":"",
  "%Name":"sndSwapPistol",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndSwapPistol",
  "parent":{
    "name":"Weapons",
    "path":"folders/Sounds/Weapons.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndSwapPistol",
  "type":0,
  "volume":1.0,
}