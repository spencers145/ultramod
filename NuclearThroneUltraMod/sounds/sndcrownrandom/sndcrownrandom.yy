{
  "$GMSound":"",
  "%Name":"sndCrownRandom",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.533991,
  "name":"sndCrownRandom",
  "parent":{
    "name":"Menu",
    "path":"folders/Sounds/Menu.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndcrownrandom.wav",
  "type":0,
  "volume":1.0,
}