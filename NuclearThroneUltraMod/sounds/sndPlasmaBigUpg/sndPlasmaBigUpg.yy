{
  "$GMSound":"",
  "%Name":"sndPlasmaBigUpg",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndPlasmaBigUpg",
  "parent":{
    "name":"plasma",
    "path":"folders/Sounds/Weapons/Energy/plasma.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndPlasmaBigUpg",
  "type":0,
  "volume":1.0,
}