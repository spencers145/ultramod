{
  "$GMSound":"",
  "%Name":"sndInspectorDeadM",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndInspectorDeadM",
  "parent":{
    "name":"Inspector",
    "path":"folders/Sounds/IDPD sounds/Inspector.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndInspectorDeadM",
  "type":0,
  "volume":1.0,
}