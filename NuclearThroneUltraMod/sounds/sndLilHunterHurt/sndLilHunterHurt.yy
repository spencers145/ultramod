{
  "$GMSound":"",
  "%Name":"sndLilHunterHurt",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndLilHunterHurt",
  "parent":{
    "name":"Regular",
    "path":"folders/Sounds/Enemies/Regular.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndLilHunterHurt",
  "type":0,
  "volume":1.0,
}