{
  "$GMSound":"",
  "%Name":"sndMutant10Crwn",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndMutant10Crwn",
  "parent":{
    "name":"Rebel",
    "path":"folders/Sounds/Player/Rebel.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant10Crwn",
  "type":0,
  "volume":1.0,
}