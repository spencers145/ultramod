{
  "$GMSound":"",
  "%Name":"sndOasisHorn",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.96,
  "name":"sndOasisHorn",
  "parent":{
    "name":"Menu",
    "path":"folders/Sounds/Menu.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndOasisHorn.wav",
  "type":0,
  "volume":1.0,
}