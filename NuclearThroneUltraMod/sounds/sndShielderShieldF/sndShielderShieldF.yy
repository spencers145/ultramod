{
  "$GMSound":"",
  "%Name":"sndShielderShieldF",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndShielderShieldF",
  "parent":{
    "name":"Shielder",
    "path":"folders/Sounds/IDPD sounds/Shielder.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndShielderShieldF",
  "type":0,
  "volume":1.0,
}