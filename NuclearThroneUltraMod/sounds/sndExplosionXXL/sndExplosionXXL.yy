{
  "$GMSound":"",
  "%Name":"sndExplosionXXL",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":2.192094,
  "name":"sndExplosionXXL",
  "parent":{
    "name":"TheThrone",
    "path":"folders/Sounds/Palace/TheThrone.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndExplosionXXL.wav",
  "type":0,
  "volume":1.0,
}