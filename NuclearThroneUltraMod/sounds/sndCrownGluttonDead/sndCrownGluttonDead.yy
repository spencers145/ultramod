{
  "$GMSound":"",
  "%Name":"sndCrownGluttonDead",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":2.390839,
  "name":"sndCrownGluttonDead",
  "parent":{
    "name":"Menu",
    "path":"folders/Sounds/Menu.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndCrownGluttonDead.ogg",
  "type":0,
  "volume":1.0,
}