{
  "$GMSound":"",
  "%Name":"sndOFire",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.291063,
  "name":"sndOFire",
  "parent":{
    "name":"Bullet",
    "path":"folders/Sounds/Weapons/Bullet.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndofire.ogg",
  "type":0,
  "volume":1.0,
}