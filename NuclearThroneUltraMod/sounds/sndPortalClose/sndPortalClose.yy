{
  "$GMSound":"",
  "%Name":"sndPortalClose",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":2.64,
  "name":"sndPortalClose",
  "parent":{
    "name":"Menu",
    "path":"folders/Sounds/Menu.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndPortalClose",
  "type":0,
  "volume":1.0,
}