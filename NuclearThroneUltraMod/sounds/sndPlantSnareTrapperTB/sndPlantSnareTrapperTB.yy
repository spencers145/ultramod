{
  "$GMSound":"",
  "%Name":"sndPlantSnareTrapperTB",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.052031,
  "name":"sndPlantSnareTrapperTB",
  "parent":{
    "name":"Player",
    "path":"folders/Sounds/Player.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndPlantSnareTrapperTB.wav",
  "type":0,
  "volume":1.0,
}