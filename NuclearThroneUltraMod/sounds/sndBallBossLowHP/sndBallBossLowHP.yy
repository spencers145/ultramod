{
  "$GMSound":"",
  "%Name":"sndBallBossLowHP",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":4.08575,
  "name":"sndBallBossLowHP",
  "parent":{
    "name":"BallBoss",
    "path":"folders/Sounds/Enemies/Boss/BallBoss.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBallBossLowHP.ogg",
  "type":0,
  "volume":1.0,
}