{
  "$GMSound":"",
  "%Name":"sndLuckyShotProc",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.378688,
  "name":"sndLuckyShotProc",
  "parent":{
    "name":"Fx",
    "path":"folders/Sounds/Fx.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndLuckyShotProc.wav",
  "type":0,
  "volume":1.0,
}