{
  "$GMSound":"",
  "%Name":"sndMutant2Valt",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":3.500476,
  "name":"sndMutant2Valt",
  "parent":{
    "name":"Crystal",
    "path":"folders/Sounds/Player/Crystal.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant2Valt",
  "type":0,
  "volume":1.0,
}