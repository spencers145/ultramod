{
  "$GMSound":"",
  "%Name":"sndFreakPopoReviveArea",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.965375,
  "name":"sndFreakPopoReviveArea",
  "parent":{
    "name":"Idpd",
    "path":"folders/Sounds/Enemies/Idpd.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndFreakPopoReviveArea.wav",
  "type":0,
  "volume":1.0,
}