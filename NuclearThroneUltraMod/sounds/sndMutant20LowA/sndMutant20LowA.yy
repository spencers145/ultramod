{
  "$GMSound":"",
  "%Name":"sndMutant20LowA",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.0,
  "name":"sndMutant20LowA",
  "parent":{
    "name":"BusinessHog",
    "path":"folders/Sounds/Player/BusinessHog.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant20LowA",
  "type":0,
  "volume":1.0,
}