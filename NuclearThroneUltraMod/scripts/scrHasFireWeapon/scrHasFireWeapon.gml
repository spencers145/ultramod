///scrHasFireWeapon();
// /@description
///@param
function scrHasFireWeapon(){
	with Player {
		return
		   wep == 50  || wep == 51  || wep == 52  || wep == 110
		|| wep == 111 || wep == 121 || wep == 122 || wep == 145
		|| wep == 153 || wep == 154 || wep == 155 || wep == 156
		|| wep == 157 || wep == 158 || wep == 159 || wep == 160
		|| wep == 161 || wep == 162 || wep == 174 || wep == 175
		|| wep == 176 || wep == 177 || wep == 179 || wep == 180
		|| wep == 183 || wep == 192 || wep == 203 || wep == 276
		|| wep == 287 || wep == 352 || wep == 392 || wep == 393
		|| wep == 394 || wep == 442 || wep == 454 || wep == 461
		|| wep == 462 || wep == 485 || wep == 501 || wep == 534
		|| wep == 573 || wep == 574 || wep == 615 || wep == 665
		|| wep == 633 || wep == 634 || wep == 638 || wep == 650
		|| wep == 656 || wep == 662 || wep == 713
		
		|| bwep == 50  || bwep == 51  || bwep == 52  || bwep == 110
		|| bwep == 111 || bwep == 121 || bwep == 122 || bwep == 145
		|| bwep == 153 || bwep == 154 || bwep == 155 || bwep == 156
		|| bwep == 157 || bwep == 158 || bwep == 159 || bwep == 160
		|| bwep == 161 || bwep == 162 || bwep == 174 || bwep == 175
		|| bwep == 176 || bwep == 177 || bwep == 179 || bwep == 180
		|| bwep == 183 || bwep == 192 || bwep == 203 || bwep == 276
		|| bwep == 287 || bwep == 352 || bwep == 392 || bwep == 393
		|| bwep == 394 || bwep == 442 || bwep == 454 || bwep == 461
		|| bwep == 462 || bwep == 485 || bwep == 501 || bwep == 534
		|| bwep == 573 || bwep == 574 || bwep == 615 || bwep == 665
		|| bwep == 633 || bwep == 634 || bwep == 638 || bwep == 650
		|| bwep == 656 || bwep == 662 || bwep == 713
		
		|| cwep == 50  || cwep == 51  || cwep == 52  || cwep == 110
		|| cwep == 111 || cwep == 121 || cwep == 122 || cwep == 145
		|| cwep == 153 || cwep == 154 || cwep == 155 || cwep == 156
		|| cwep == 157 || cwep == 158 || cwep == 159 || cwep == 160
		|| cwep == 161 || cwep == 162 || cwep == 174 || cwep == 175
		|| cwep == 176 || cwep == 177 || cwep == 179 || cwep == 180
		|| cwep == 183 || cwep == 192 || cwep == 203 || cwep == 276
		|| cwep == 287 || cwep == 352 || cwep == 392 || cwep == 393
		|| cwep == 394 || cwep == 442 || cwep == 454 || cwep == 461
		|| cwep == 462 || cwep == 485 || cwep == 501 || cwep == 534
		|| cwep == 573 || cwep == 574 || cwep == 615 || cwep == 665
		|| cwep == 633 || cwep == 634 || cwep == 638 || cwep == 650
		|| cwep == 656 || cwep == 662 || cwep == 713

	}
	return false;
}