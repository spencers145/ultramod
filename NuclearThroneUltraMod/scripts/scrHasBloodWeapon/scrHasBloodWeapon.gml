///scrHasBloodWeapon();
// /@description
///@param
function scrHasBloodWeapon(){
	with Player {
		return wep == 29 || wep == 60 || wep == 61 || wep == 83
		|| wep == 84 || wep == 109 || wep == 132 || wep == 142
		|| wep == 164 || wep == 327 || wep == 415 || wep == 425 || wep == 426
		|| wep == 434 || wep == 538 || wep == 539 || wep == 547 || wep == 548
		|| wep == 549 || wep == 599 || wep == 609 || wep == 548 || wep == 548
		|| wep == 612 || wep == 499 || wep == 626 || wep == 643 || wep == 644
		|| wep == 645 || wep = 658 || wep == 717 || wep == 718 || wep == 413 || wep == 274 || wep == 304
		
		|| bwep == 29 || bwep == 60 || bwep == 61 || bwep == 83
		|| bwep == 84 || bwep == 109 || bwep == 132 || bwep == 142
		|| bwep == 164 || bwep == 327 || bwep == 415 || bwep == 425 || bwep == 426
		|| bwep == 434 || bwep == 538 || bwep == 539 || bwep == 547 || bwep == 548
		|| bwep == 549 || bwep == 599 || bwep == 609 || bwep == 548 || bwep == 548
		|| bwep == 612 || bwep == 499 || bwep == 626 || bwep == 643 || bwep == 644
		|| bwep == 645 || bwep == 658 || bwep == 717 || bwep == 718 || bwep == 413 || bwep == 274 || bwep == 304
		
		|| cwep == 29 || cwep == 60 || cwep == 61 || cwep == 83
		|| cwep == 84 || cwep == 109 || cwep == 132 || cwep == 142
		|| cwep == 164 || cwep == 327 || cwep == 415 || cwep == 425 || cwep == 426
		|| cwep == 434 || cwep == 538 || cwep == 539 || cwep == 547 || cwep == 548
		|| cwep == 549 || cwep == 599 || cwep == 609 || cwep == 548 || cwep == 548
		|| cwep == 612 || cwep == 499 || cwep == 626 || cwep == 643 || cwep == 644
		|| cwep == 645 || cwep == 658 || cwep == 717 || cwep == 718 || cwep == 413 || cwep == 274 || cwep == 304
		
	}
	return false;
}