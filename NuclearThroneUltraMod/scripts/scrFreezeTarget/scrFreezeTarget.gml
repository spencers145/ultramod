///scrFreezeTarget();
// /@description
///@param
function scrFreezeTarget(stunTime,frostDamage = 2){
	if frozen == noone
	{
		frozen = instance_create(x,y,FrozenEnemy);
		with frozen
		{
			debrisAmount = frostDamage;
			spriteSize = max(other.sprite_width,other.sprite_height);
			var s = min(2,other.mySize);
			image_xscale=s*choose(1,-1);
			image_yscale=s;
			xx=other.x
			yy=other.y
			owner=other.id;
		}
	}
	else
	{
		with frozen {
			debrisAmount = min(debrisAmount + 0.25, 4);
		}
	}
	if alarm[1] > stunTime
		stunTime *= 0.5;
	if alarm[11] < 60
		alarm[11] += stunTime + 5;
	if alarm[1] > 0 && alarm[1] < 60
			alarm[1] += stunTime;
			
		if mySize < 8
		{
			speed = 0;
			walk = 0;
		}
		if (instance_exists(Player) && Player.skill_got[43] && Player.ultra_got[97] && !Player.altUltra && team != 0)
		{
			scrMoodSwingStun(7);
			scrMoodSwingFlameSpread(24);
		}
}