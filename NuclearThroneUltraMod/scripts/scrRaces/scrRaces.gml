function scrRaces() {
	race_name[0] = "[RANDOM]"
	race_pass[0] = "???"
	race_acti[0] = "???"
	race_butt[0] = ""
	race_butt_detail[0] = "";
	//race_back[0] = "";
	race_lock[0] = ""
	race_have[0] = 1
	race_bskin[0] = 0
	race_cskin[0] = 0
	race_dskin[0] = 0
	race_eskin[0] = 0
	race_fskin[0] = 0
	race_bskin_lock[0] = ""
	race_cskin_lock[0] = ""


	race_name[1] = "[FISH]"
	race_pass[1] = "GETS MORE <y>AMMO<y>"
	race_acti[1] = "<w>DODGE ROLL<w>"
	race_butt[1] = "<aq>WATER<aq> <w>BOOST<w>#MORE <y>AMMO<y> FROM DROPS"
	race_butt_detail[1] = "<aq>WATER<aq> <w>BOOST<w>#UNLIMITED <w>ROLLING<w>#GET EVEN MORE <y>AMMO<y>#FROM <y>AMMO<y> PICKUPS";
	//race_back[1] = "IF YOUR AMMO IS ABOVE 75%#INCREASE FIRE RATE#AND RELOAD SPEED";
	race_lock[1] = "UNLOCKED FROM THE START"
	race_have[1] = 1
	race_swep[1] = 1
	race_bskin[1] = 0
	race_cskin[1] = 0
	race_dskin[1] = 0
	race_eskin[1] = 0
	race_fskin[1] = 0
	race_bskin_lock[1] = "REACH LOOP#WITH GAMEMODE:# FISH'S PARTNER ONLY"
	race_cskin_lock[1] = "LOOP WITH#EVERY CHARACTER"


	race_name[2] = "[CRYSTAL]"
	race_pass[2] = "MORE MAX <r>HP<r>"
	race_acti[2] = "<p>SHIELD<p>"
	race_butt[2] = "LONGER <p>SHIELD<p>"
	race_butt_detail[2] = "75% LONGER <p>SHIELD<p>"
	//race_back[2] = "DAMAGE REDUCTION WHILE STANDING STILL";
	race_lock[2] = "UNLOCKED FROM THE START"
	race_have[2] = 1
	race_swep[2] = 1
	race_bskin[2] = 0
	race_cskin[2] = 0
	race_dskin[2] = 0
	race_eskin[2] = 0
	race_fskin[2] = 0
	race_bskin_lock[2] = "REACH CURSED CAVES#AS CRYSTAL"
	race_cskin_lock[2] = "REACH INVERTED CAVES#AS CRYSTAL"


	race_name[3] = "[EYES]"
	race_pass[3] = "SEES IN THE DARK"
	race_acti[3] = "<w>TELEKINESIS<w>"
	race_butt[3] = "STRONGER <w>TELEKINESIS<w>"
	race_butt_detail[3] = "DOUBLE <w>TELEKINESIS<w> STRENGTH";
	//race_back[3] = "ENEMIES CLOSE TO YOU TAKE DAMAGE";
	race_lock[3] = "REACH SEWERS TO UNLOCK"
	race_have[3] = 0
	race_swep[3] = 1
	race_bskin[3] = 0
	race_cskin[3] = 0
	race_dskin[3] = 0
	race_eskin[3] = 0
	race_fskin[3] = 0
	race_bskin_lock[3] = "REACH PIZZA SEWERS#AS EYES"
	race_cskin_lock[3] = "LOOP WITH EAGLE EYES#AS EYES"


	race_name[4] = "[MELTING]"
	race_pass[4] = "LESS MAX <r>HP<r>, MORE <g>RADS<g>"
	race_acti[4] = "EXPLODE CORPSES"
	race_butt[4] = "BIGGER <w>CORPSE<w> EXPLOSIONS"
	race_butt_detail[4] = "TRIPLE <r>BLOOD<r> <w>CORPSE<w> EXPLOSIONS";
	//race_back[4] = "CORPSE EXPLOSIONS CAN GIVE DROPS";
	race_lock[4] = "DIE TO UNLOCK"
	race_have[4] = 0
	race_swep[4] = 1
	race_bskin[4] = 0
	race_cskin[4] = 0
	race_dskin[4] = 0
	race_eskin[4] = 0
	race_fskin[4] = 0
	race_bskin_lock[4] = "LOOP WITHOUT#RHINO SKIN,#STRONG SPIRIT#ALKALINE SALIVA#BOUNCY FAT#NERVES OF STEEL#TOUGH SHELL#LAST WISH#CROWN OF PROTECTION#AND CROWN OF LIFE#AS MELTING"
	race_cskin_lock[4] = "REACH LOOP 3 ON#1HP EQUALITY#GAMEMODE#AS MELTING"


	race_name[5] = "[PLANT]"
	race_pass[5] = "IS <w>FASTER<w>"
	race_acti[5] = "<w>SNARE<w> ENEMIES"
	race_butt[5] = "<w>SNARE<w> FINISHES ENEMIES#UNDER 36% <r>HP<r>#<w>SNARE<w> IS SLIGHTLY BIGGER"
	race_butt_detail[5] = "<w>SNARE<w> FINISHES ENEMIES#UNDER 36% <r>HP<r>#SNARE IS 25% BIGGER";
	//race_back[5] = "ENEMIES KILLED WHILE SNARED GIVE MORE DROPS THIS IS AN ULTRA ";
	race_lock[5] = "KILL 100 ENEMIES TO UNLOCK"
	race_have[5] = 0
	race_swep[5] = 1
	race_bskin[5] = 0
	race_cskin[5] = 0
	race_dskin[5] = 0
	race_eskin[5] = 0
	race_fskin[5] = 0
	race_bskin_lock[5] = "REACH WONDERLAND#IN UNDER 8 MINUTES#AS PLANT"
	race_cskin_lock[5] = "KILL 2.000#ENEMIES IN ONE RUN#AS PLANT"


	race_name[6] = "[Y.V.]"
	race_pass[6] = "FASTER <w>RELOAD SPEED<w>"
	race_acti[6] = "POP POP"
	race_butt[6] = "BRRRAP"
	race_butt_detail[6] = "ACTIVE FIRES 4 TIMES#INSTEAD OF 2";
	//race_back[0] = "ADDITIONAL PROJECTILES FROM ACTIVE#PIERCE ENEMIES";
	race_lock[6] = "REACH Y.V.'S MANSION TO UNLOCK"
	race_have[6] = 0
	race_swep[6] = 39
	race_bskin[6] = 0
	race_cskin[6] = 0
	race_dskin[6] = 0
	race_eskin[6] = 0
	race_fskin[6] = 0
	race_bskin_lock[6] = "UNLOCK ALL#GOLDEN WEAPONS#FOR ONE CHARACTER"
	race_cskin_lock[6] = "UNLOCK#A GOLDEN WEAPON#FOR ALL CHARACTERS"


	race_name[7] = "[STEROIDS]"
	race_pass[7] = "IS LESS <w>ACCURATE<w>#ALL WEAPONS ARE FULLY AUTOMATIC"
	race_acti[7] = "<w>DUAL WIELDING<w>"
	race_butt[7] = "FIRING WITH ONE WEAPON#HAS A CHANCE TO#GIVE <y>AMMO<y> FOR THE OTHER"
	race_butt_detail[7] = "FIRING WITH ONE WEAPON#HAS A CHANCE TO#GIVE <y>AMMO<y> FOR THE OTHER#MORE EFFECTIVE WHEN#FIRING BOTH WEAPONS"
	//race_back[7] = "MISSING INCREASES#FIRE RATE AND RELOAD SPEED";
	race_lock[7] = "REACH LABS TO UNLOCK"
	race_have[7] = 0
	race_swep[7] = 1
	race_bskin[7] = 0
	race_cskin[7] = 0
	race_dskin[7] = 0
	race_eskin[7] = 0
	race_fskin[7] = 0
	race_bskin_lock[7] = "DEFEAT#TECHNOMANCER#AS STEROIDS"
	race_cskin_lock[7] = "DEFEAT#INVERTED BIG MACHINE#AS STEROIDS"


	race_name[8] = "[ROBOT]"
	race_pass[8] = "FINDS BETTER TECH"
	race_acti[8] = "HOLD TO <w>EAT WEAPONS<w>"
	race_butt[8] = "BETTER GUN <g>NUTRITION<g>"
	race_butt_detail[8] = "ANOTHER <r>HP<r>/<y>AMMO<y> DROP#AND MORE <g>RADS<g> WHEN <w>EATING<w> A WEAPON"
	//race_back[8] = "FIND EVEN BETTER WEAPONS";
	race_lock[8] = "REACH SCRAPYARD TO UNLOCK"
	race_have[8] = 0
	race_swep[8] = 1
	race_bskin[8] = 0
	race_cskin[8] = 0
	race_dskin[8] = 0
	race_eskin[8] = 0
	race_fskin[8] = 0
	race_bskin_lock[8] = "EAT A HYPER WEAPON"
	race_cskin_lock[8] = "EAT AN ULTRA WEAPON"


	race_name[9] = "[CHICKEN]"
	race_pass[9] = "HARD TO KILL"
	race_acti[9] = "HOLD FOR <w>SLOW MOTION<w>"
	race_butt[9] = "NORMAL <w>MOVEMENT SPEED<w>#AND NORMAL <w>RELOAD SPEED<w>#DURING <w>SLOWMOTION<w>"//"NORMAL RATE OF FIRE#DURING SLOW MOTION"
	race_butt_detail[9] = race_butt[9];
	//race_back[9] = "NORMAL RATE OF FIRE#DURING SLOW MOTION (this IS AN ULTRA MAKE CHANGE)";
	race_lock[9] = "OPEN A BIG HP CHEST"
	race_have[9] = 0
	race_swep[9] = 46
	race_bskin[9] = 0
	race_cskin[9] = 0
	race_dskin[9] = 0
	race_eskin[9] = 0
	race_fskin[9] = 0
	race_bskin_lock[9] = "ACQUIRE#THE BLACK SWORD"
	race_cskin_lock[9] = "ACQUIRE#THE DARK SWORD#LOOP WITH#CHICKEN SWORD"


	race_name[10] = "[REBEL]"
	race_pass[10] = "<p>PORTALS<p> <r>HEAL<r>"
	race_acti[10] = "SPAWN ALLIES"
	race_butt[10] = "HIGHER <w>ALLY RELOAD SPEED<w>"
	race_butt_detail[10] = "+50% <w>ALLY RELOAD SPEED<w>";
	//race_back[10] = "FASTER ALLIES#ALLIES TELEPORT TO YOU#WHEN LOST";
	race_lock[10] = "REACH FROZEN CITY TO UNLOCK"
	race_have[10] = 0
	race_swep[10] = 1
	race_bskin[10] = 0
	race_cskin[10] = 0
	race_dskin[10] = 0
	race_eskin[10] = 0
	race_fskin[10] = 0
	race_bskin_lock[10] = "COMPLETE A#MAIN AREA ON LOOP#WITHOUT FIRING#AS REBEL"
	race_cskin_lock[10] = "REACH B?N??T L?N?"


	race_name[11] = "[HUNTER]"
	race_pass[11] = "HIGHER PROJECTILE <w>VELOCITY<w>#BETTER <w>ACCURACY<w>"
	race_acti[11] = "MARK ENEMIES"
	race_butt[11] = "<w>MARKED<w> ENEMIES#TAKE 30% MORE <w>DAMAGE<w>"
	race_butt_detail[11] = race_butt[11];
	//race_back[11] = "MARKED ENEMIES THAT DIE#HAVE A HIGHER DROPRATE(this is an ultra?) MARKED ENEMIES THAT DIE#DECREATE RELOAD";
	race_lock[11] = "DEFLECT A SNIPER SHOT#INTO THE SNIPER"
	race_have[11] = 0
	race_swep[11] = 80//sniper rifle
	race_bskin[11] = 0
	race_cskin[11] = 0
	race_dskin[11] = 0
	race_eskin[11] = 0
	race_fskin[11] = 0
	race_bskin_lock[11] = "REACH THE PALACE#ONLY USING#ANY STARTING WEAPON#AS HUNTER"
	race_cskin_lock[11] = "KILL AN HARD MODE#ULTRA SNIPER BOT#AS HUNTER"


	race_name[12] = "[YUNG CUZ]"
	race_pass[12] = "FIND MORE <r>HP<r> CHESTS#MORE DUPES = MORE <y>AMMO<y> FROM PICKUPS"
	race_acti[12] = "<w>DUPLICATE<w>"
	race_butt[12] = "GAIN EVEN MORE <y>AMMO<y>#DEPENDING ON NUMBER OF DUPLICATES"
	race_butt_detail[12] = "EXTRA AMMO GAINED PER DUPLICATE#IS DOUBLED"
	//race_back[12] = "DECREASE COST OF BUDDY";
	race_lock[12] = "KILL A MIMIC"
	race_have[12] = 0
	race_swep[12] = 39//golden revolver
	race_bskin[12] = 0
	race_cskin[12] = 0
	race_dskin[12] = 0
	race_eskin[12] = 0
	race_fskin[12] = 0
	race_bskin_lock[12] = "GO TO THE CRIB"
	race_cskin_lock[12] = "AS YUNG CUZ#COMPLETE 3 AREAS#IN A ROW#WITHOUT HAVING#MORE THAN 1 MAX HP";


	race_name[13] = "[SHEEP]"
	race_pass[13] = "ENEMIES WON'T ATTACK UNTIL THEY SPOT YOU"
	race_acti[13] = "HOLD TO <pi>CHARGE<pi>"
	race_butt[13] = "BETTER <pi>CHARGE<pi>"
	race_butt_detail[13] = "BETTER <pi>CHARGE<pi>#MORE CHARGE DAMAGE#MORE CHARGE CONTROL#FASTER CHARGE"
	//race_back[13] = "IDK MORPH DAMAGE";
	race_lock[13] = "MORPH AN ENEMY"
	race_have[13] = 0
	race_swep[13] = 0//nothing
	race_bskin[13] = 0
	race_cskin[13] = 0
	race_dskin[13] = 0
	race_eskin[13] = 0
	race_fskin[13] = 0
	race_bskin_lock[13] = "SAVE A LOST SHEEP#AS SHEEP"
	race_cskin_lock[13] = "COMPLETE A#MAIN AREA#WITHOUT FIRING#AFTER THE SCRAPYARD#AS SHEEP"


	race_name[14] = "[PANDA]"
	race_pass[14] = "SMALLER HITBOX#<pi>RANDOMIZE<pi> WEAPONS THROWN IN A <p>PORTAL<p>"//"PRIMARY WEAPON IS RANDOMIZED#TO ONE OF THE SAME TIER#EACH AREA#EXCEPT YOUR STARTING WEAPON"
	race_acti[14] = "CLICK/HOLD <w>THROW<w> WEAPONS"
	race_butt[14] = "#SHORTER <w>THROWN<w> WEAPON RETURN DELAY#MORE <w>THROW<w> DAMAGE"
	race_butt_detail[14] = "#REDUCED <w>THROWN<w> WEAPON RETURN DELAY (0.83 SEC)#+20% MORE <w>THROW<w> DAMAGE"
	//race_back[14] = "NO LONGER GET RANDOM WEP(is ultra change)";
	race_lock[14] = "VISIT THE JUNGLE"
	race_have[14] = 0
	race_swep[14] = 200//panda stick
	race_bskin[14] = 0
	race_cskin[14] = 0
	race_dskin[14] = 0
	race_eskin[14] = 0
	race_fskin[14] = 0
	race_bskin_lock[14] = "CONVERT A#GOLDEN WEAPON#INSIDE OF A PORTAL"
	race_cskin_lock[14] = "LOOP BY#ONLY THROWING WEAPONS#AND NOT USING THEM"


	race_name[15] = "[ATOM]"
	race_pass[15] = "ITEMS DESPAWN QUICKER#YOU DEAL <g>CONTACT DAMAGE<g>"
	race_acti[15] = "<w>TELEPORT<w>"
	race_butt[15] = "EXPLOSIVE <p>TELEPORTATION<p>"
	race_butt_detail[15] = "UPON <p>TELEPORTING<p>#FIRE AN EXPLOSIVE LASER#TOWARDS YOUR PREVIOUS POSITION#CAUSE <g>PLASMA<g> AND <b>LIGHTNING<b> AT DESTINATION"
	//race_back[15] = "SHORT IMMUNITY TO CONTACT DAMAGE#AFTER TELEPORTING";
	race_lock[15] = "TAKE CRYSTAL'S THRONE BUTT"
	race_have[15] = 0
	race_swep[15] = 19//laser pistol
	race_bskin[15] = 0
	race_cskin[15] = 0
	race_dskin[15] = 0
	race_eskin[15] = 0
	race_fskin[15] = 0
	race_bskin_lock[15] = "COMPLETE A#MAIN AREA#AFTER THE SEWERS#WITHOUT FIRING#AS ATOM"
	race_cskin_lock[15] = "COMPLETE FIVE#AREAS IN A ROW#USING CROWN OF HASTE#AS ATOM"


	race_name[16] = "[VIKING]"
	race_pass[16] = "REDUCED <r>MAX HP<r>##<p>PORTALS<p> GIVE <gb>ARMOUR<gb>#TWO <gb>ARMOUR<gb> MAXIMUM"
	race_acti[16] = "SERPENT <gb>ARMOUR<gb> <w>STRIKE<w>"//ARMOUR STRIKE
	race_butt[16] = "INCREASE <gb>MAX ARMOUR<gb> BY ONE#FIRST <gb>ARMOUR<gb> <w>STRIKE<w> EACH AREA IS FREE"
	race_butt_detail[16] = race_butt[16];
	//race_back[16] = "ARMOUR NOW GETS TAKEN WHEN#SUFFERING LETHAL DAMAGE";
	race_lock[16] = "REGAIN STRONG SPIRIT 3 TIMES#IN ONE RUN"//"BREAK X AMOUNT OF WALLS"
	race_have[16] = 0
	race_swep[16] = 215//MDRFKIN AXE BITCHES
	race_bskin[16] = 0
	race_cskin[16] = 0
	race_dskin[16] = 0
	race_eskin[16] = 0
	race_fskin[16] = 0
	race_bskin_lock[16] = "COMPLETE A LEVEL#WITH 4 ARMOUR#AS VIKING"
	race_cskin_lock[16] = "KILL 16 ENEMIES#IN ONE ARMOUR STRIKE"


	race_name[17] = "[WEAPONSMITH]"//WEAPON SMITH
	race_pass[17] = "START WITH A RANDOM <w>WEAPON MOD<w>#ENEMIES DROP MORE WEAPONS#LOWER TIER WEAPONS"
	race_acti[17] = "UPGRADE WEAPONS"
	race_butt[17] = "GET AN EVEN HIGHER#TIER WEAPON WHEN COMBINING WEAPONS"
	race_butt_detail[17] = "GET AN EVEN HIGHER (+1)#TIER WEAPON WHEN COMBINING WEAPONS"
	//race_back[17] = "THE FIRST TIME YOU COMBINE#A WEAPON IN AN AREA#YOU DON'T LOSE THEM BUT INSTEAD DROP IT";
	race_lock[17] = "MODIFY A WEAPON"
	race_have[17] = 0
	race_swep[17] = 1
	race_bskin[17] = 0
	race_cskin[17] = 0
	race_dskin[17] = 0
	race_eskin[17] = 0
	race_fskin[17] = 0
	race_bskin_lock[17] = "HOLD TO COMBINE TWO#GOLDEN WEAPONS"
	race_cskin_lock[17] = "ADD FOUR MODS#TO ONE WEAPON"


	race_name[18] = "[ANGEL]"
	race_pass[18] = "FLY OVER WALLS"
	race_acti[18] = "<aq>DEFLECT<aq> ALL DAMAGE#COSTS <y>AMMO<y>"
	race_butt[18] = "ACTIVE ALSO <r>HEALS<r>#1<r>HP<r> EVERY OTHER USE"
	race_butt_detail[18] = race_butt[18];
	//race_back[18] = "FLYING OVER WALLS DRAINS AMMO#INSTEAD OF HP";
	race_lock[18] = "HAVE A SINGLE RUN WITH#ATLEAST THREE SURVIVAL MUTATIONS#(RHINO SKIN, ALKALINE SALIVA ETC.)"
	race_have[18] = 0
	race_swep[18] = 56
	race_bskin[18] = 0
	race_cskin[18] = 0
	race_dskin[18] = 0
	race_eskin[18] = 0
	race_fskin[18] = 0
	race_bskin_lock[18] = "TRY 10 DIFFERENT CROWNS#AT LEAST ONCE#(WITH ANY CHARACTER)"
	race_cskin_lock[18] = "DEFLECT A PROJECTILE#THAT IS DEFLECTED#BY A CROWN GUARDIANS#WITHOUT USING MELEE#AS ANGEL"


	race_name[19] = "[SKELETON]"
	race_pass[19] = "REDUCED <r>MAX HP<r>,#MOVES SLOWER,#LESS ACCURATE"
	race_acti[19] = "<r>BLOOD<r> <w>GAMBLE<w>"
	race_butt[19] = "BETTER <r>BLOOD<r> <w>GAMBLE<w> ODDS"
	race_butt_detail[19] = "33% BETTER <r>BLOOD<r> <w>GAMBLE<w> ODDS"
	//race_back[19] = "THE SLOWER YOU MOVE#THE FASTER YOU FIRE";
	race_lock[19] = "DIE IN THE LABS AS MELTING"
	race_have[19] = 0
	race_swep[19] = 56//allright we'll do a character with a rusty revolver
	race_bskin[19] = 0
	race_cskin[19] = 0
	race_dskin[19] = 0
	race_eskin[19] = 0
	race_fskin[19] = 0
	race_bskin_lock[19] = "HAVE THREE LIVES"
	race_cskin_lock[19] = "REACH THE VULCANO#ON 400% SCREENSHAKE#AS SKELETON"


	race_name[20] = "[BUSINESS HOG]"
	race_pass[20] = "BOSSES DROP <y>GOLDEN<y> WEAPON CHESTS"
	race_acti[20] = "HOLD FOR <g>SHOP<g>"
	race_butt[20] = "20% <w>DISCOUNT<w>"
	race_butt_detail[20] = "20% <w>DISCOUNT<w>"
	//race_back[20] = "WEAPON CHESTS SOMETIME BECOME#GOLDEN WEAPON CHESTS";
	race_lock[20] = "FIND AND STEAL#THE MONEY GUN"
	race_have[20] = 0
	race_swep[20] = 444//golden hand cannon
	race_bskin[20] = 0
	race_cskin[20] = 0
	race_dskin[20] = 0
	race_eskin[20] = 0
	race_fskin[20] = 0
	race_bskin_lock[20] = "REACH AND#BEAT THE THRONE#USING ONLY#GOLD WEAPONS#AS BUSINESS HOG"
	race_cskin_lock[20] = "SUCCESSFULLY USE#THE INVESTMENT ULTRA"


	race_name[21] = "[HORROR]"
	race_pass[21] = "EXTRA <g>MUTATION<g> CHOICE"//"BLAST ARMOUR, HEAT"
	race_acti[21] = "<g>RADIATION<g> BEAM"//"PORTAL STRIKE"
	race_butt[21] = "BEAM CHARGES QUICKER AND <r>HEALS<r>#RELEASING BEAM#FIRES A BIG BALL OF <g>RADIATION<g>"
	race_butt_detail[21] = "BEAM CHARGES 30% QUICKER#BEAM <r>HEALS<r> EVERY 2 SECONDS#SOMETIMES MISSED BEAM#WILL DROP <g>RADS<g>"
	//race_back[21] = "BEAM CHARGES QUICKER#GIVES AMMO";
	race_lock[21] = "REACH WONDERLAND"//"REACH PALACE"
	race_have[21] = 0
	race_swep[21] = 1
	race_bskin[21] = 0
	race_cskin[21] = 0
	race_dskin[21] = 0
	race_eskin[21] = 0
	race_fskin[21] = 0
	race_bskin_lock[21] = "REACH THE PALACE#WITHOUT LEVELING UP#AS HORROR"
	race_cskin_lock[21] = "COMPLETE A#MAIN AREA ON LOOP#USING ONLY THE BEAM"


	race_name[22] = "[ROGUE]"
	race_pass[22] = "BLAST ARMOUR, <b>HEAT<b>"//"WALK FOREVER"
	race_acti[22] = "<b>PORTAL STRIKE<b>"//"GASS, TOXIC IMMUNITY"
	race_butt[22] = "STRONGER <b>PORTAL STRIKE<b>#MORE <b>PORTAL STRIKE AMMO<b> SPAWNS"//"GASS SPREADS FASTER"
	race_butt_detail[22] = race_butt[22];
	//race_back[22] = "INCREASE MAXIMUM PORTAL STRIKE AMMO (this is an ult)";
	race_lock[22] = "REACH THE THRONE"
	race_have[22] = 0
	race_swep[22] = 312//ROGUE RIFLE
	race_bskin[22] = 0
	race_cskin[22] = 0
	race_dskin[22] = 0
	race_eskin[22] = 0
	race_fskin[22] = 0
	race_bskin_lock[22] = "SURVIVE IN#THE GAMEMODE#VAN FAN#FOR ATLEAST#30 SECONDS#AS ROGUE"
	race_cskin_lock[22] = "USE 8 PORTAL STRIKES#IN ONE AREA"


	race_name[23] = "[FROG]"
	race_pass[23] = "IMMUNE TO <g>TOXIC<g>";
	race_acti[23] = "HOLD AND RELEASE <g>TOXIC GAS<g>"
	if random(1000) < 1
		race_acti[23] = "SHIT & FART"
	race_butt[23] = "<g>TOXIC<g> SPREADS FASTER"
	race_butt_detail[23] = "<g>TOXIC<g> SPREADS FASTER#<g>TOXIC<g> LASTS LONGER#HOMING <g>TOXIC<g>#ACTIVE SPAWNS <g>TOXIC<g> TOWARDS YOUR AIM"
	//race_back[23] = "TOXIC GASS DEALS MORE DAMAGE (this is an ulta)";
	race_lock[23] = "KILL BALL MOM"
	race_have[23] = 0
	race_swep[23] = 348//FROG BLASTER
	race_bskin[23] = 0
	race_cskin[23] = 0
	race_dskin[23] = 0
	race_eskin[23] = 0
	race_fskin[23] = 0
	race_bskin_lock[23] = "USE TWO DIFFERENT#TOXIC WEAPONS#IN THE SAME RUN#AS FROG"
	race_cskin_lock[23] = "REACH AND BEAT#THE THRONE#AS FROG#ON THE GAMEMODE:#DISC ROOM#WITH SETTINGS:#ATLEAST 6 DISCS#ATLEAST 2 DAMAGE"


	race_name[24] = "[ELEMENTOR]"//ELEMENTAL
	race_pass[24] = "STRONGER <b>LIGHTNING<b>,#<aq>FROST<aq>, <r>FIRE<r> AND <p>KRAKEN<p>"
	race_acti[24] = "<w>TERRAFORM<w> AT THE COST OF <y>AMMO<y>"//TERRAFORM
	race_butt[24] = "CHEAPER TERRAFORM#YOUR WALLS <w>DEFLECT<w> ENEMY PROJECTILES"
	race_butt_detail[24] = "33% CHEAPER TERRAFORM#YOUR WALLS <w>DEFLECT<w> ENEMY PROJECTILES"
	//race_back[24] = "INCREASED CHANCE OF FINDING ELEMENTAL WEAPONS#LEVELS GENERATE MORE COVER";
	race_lock[24] = "REACH MUSHROOM LAND"//TAKE HEAVY HEART
	race_have[24] = 0
	race_swep[24] = 337//MINI LIGHTNING PISTOL//102//FROST PISTOL
	race_bskin[24] = 0
	race_cskin[24] = 0
	race_dskin[24] = 0
	race_eskin[24] = 0
	race_fskin[24] = 0
	race_bskin_lock[24] = "BURN A FROZEN ENEMY#AS ELEMENTOR"
	race_cskin_lock[24] = "SUMMON 4 TINY KRAKENS#IN ONE AREA#USING CROWN OF DROWNING#AS ELEMENTOR"


	race_name[25] = "[DOCTOR]"//MUTATION SMITH
	race_pass[25] = "ALL <g>MUTATIONS<g> ARE UPGRADED#GETS LESS <g>RADIATION<g> FROM ENEMIES"//SLIGHTLY BETTER MUTATIONS# LEVELS SLOWER
	race_acti[25] = "CONVERT <r>LIFE<r> TO <g>RADS<g>"
	race_butt[25] = "YOUR <w>ACTIVE<w> ALSO#DEALS DAMAGE TO ALL ENEMIES ON SCREEN#AND STEALS SOME OF THEIR <g>RADS<g>"
	race_butt_detail[25] = race_butt[25];
	//race_back[25] = "LESS HP COST?";
	race_lock[25] = "REACH LEVEL ULTRA"//REACH LEVEL ULTRA
	race_have[25] = 0
	race_swep[25] = 1
	race_bskin[25] = 0
	race_cskin[25] = 0
	race_dskin[25] = 0
	race_eskin[25] = 0
	race_fskin[25] = 0
	race_bskin_lock[25] = "REACH LEVEL 7#BEFORE THE LABS#WHILE USING#THE MAIN PATH#AS DOCTOR"
	race_cskin_lock[25] = "PROFESSIONAL#SELFCARE"

	race_name[26] = "[GOOD OL' HUMPHRY]"//RUSTY GRANDPA SOLDIER
	race_pass[26] = "LESS <r>MAX HP<r>#DEALING DAMAGE BUILDS UP <w>SKILL<w>##SKILL INCREASES ALL <w>DAMAGE DEALT<w>##SKILL DECREASES OVER TIME#AND WHEN YOU <aq>TAKE DAMAGE<aq>#"//"HITTING SHOTS BUILDS SKILL#MISSING RESETS SKILL#UNTIL YOU HIT A SHOT AGAIN#SKILL INCREASES DAMAGE"
	race_acti[26] = "<w>HALT<w> - COSTS 8 <w>SKILL<w>"//"SLOW AND SHRINK#ENEMY PROJECTILES#AT THE COST OF#unequipped AMMO"//DISCIPLINE those bullets
	race_butt[26] = "#<w>SKILL<w> DECREASES SLOWER"
	race_butt_detail[26] = "#<w>SKILL<w> DECREASES 25% SLOWER#KEEP 5% MORE SKILL WHEN GETTING HIT"
	//race_back[26] = "DECREASE COST OF ACTIVE";
	race_lock[26] = "BEAT BIG FISH"
	race_have[26] = 0
	race_swep[26] = 148
	race_bskin[26] = 0
	race_cskin[26] = 0
	race_dskin[26] = 0
	race_eskin[26] = 0
	race_fskin[26] = 0
	race_bskin_lock[26] = "PERFORM 20 CLOSE DODGES#USING EXTRA FEET#IN ONE RUN#AS HUMPHRY"//"COMPLETE A#MAIN AREA#AFTER THE SCRAPYARD#WITHOUT MISSING#AS GOOD OL'HUMPHRY"
	race_cskin_lock[26] = "REACH 500 SKILL!"
	
	race_name[27] = "[HANDS]"
	race_pass[27] = "HIGHER CHANCE FOR <w>INVERTED PORTALS<w>#<w>INVERTED PORTALS<w> (OVER) <r>HEAL<r> 2<r>HP<r>"
	race_acti[27] = "CLICK/HOLD TO <w>GRAB<w>#CHESTS/WEAPONS/ENEMIES"
	race_butt[27] = "<w>PUSH<w> ENEMIES INSTEAD#YOUR HAND DEALS MORE DAMAGE"
	race_butt_detail[27] = race_butt[27];
	//race_back[27] = "MORE DAMAGE OR SOMETHING";
	race_lock[27] = "KILL THE INVERTED THRONE"
	race_have[27] = 0
	race_swep[27] = 443
	race_bskin[27] = 0
	race_cskin[27] = 0
	race_dskin[27] = 0
	race_eskin[27] = 0
	race_fskin[27] = 0
	race_bskin_lock[27] = "SLAP#THE PURPLE DRAGON#TO DEATH#(KILL WITH ACTIVE)"
	race_cskin_lock[27] = "SUCKERPUNCH#THREE IDPD VANS#INTO OBLIVION#IN A SINGLE RUN#(KILL WITH ACTIVE&THRONE BUTT)"


	racemax = 27;
}
