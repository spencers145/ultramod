///scrCrownOfDeath();
// /@description
///@param
function scrCrownOfDeath(am = 3,dis = 26, reduc = 2){
	if scrIsCrown(3)//Crown of death
	{
		var ang = random(360);
		var angStep = 360/am;
		repeat(am)
		{
			with instance_create(x+lengthdir_x(dis,ang),y+lengthdir_y(dis,ang),SmallExplosion)
			{
				dmg -= reduc;
				alarm[0] = 0;
				alarm[2] = 0;
			}
			ang += angStep;
		}
	}
}