{
  "$GMSprite":"",
  "%Name":"sprUltraBigDogSleepHurt",
  "bboxMode":1,
  "bbox_bottom":95,
  "bbox_left":0,
  "bbox_right":95,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"9610e627-2ac5-4d9c-afde-aac503d47ca5","name":"9610e627-2ac5-4d9c-afde-aac503d47ca5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b4243482-e06e-42d5-988c-a21f63079271","name":"b4243482-e06e-42d5-988c-a21f63079271","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9db14b93-242c-4813-ade5-bf90aa9b3754","name":"9db14b93-242c-4813-ade5-bf90aa9b3754","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":96,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"2f2507c6-2672-4c20-ae25-8e5518cf6c14","blendMode":0,"displayName":"default","isLocked":false,"name":"2f2507c6-2672-4c20-ae25-8e5518cf6c14","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprUltraBigDogSleepHurt",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigDog",
    "path":"folders/Sprites/Enemies/Boss/BigDog.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprUltraBigDogSleepHurt",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprUltraBigDogSleepHurt",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9610e627-2ac5-4d9c-afde-aac503d47ca5","path":"sprites/sprUltraBigDogSleepHurt/sprUltraBigDogSleepHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"067ad44d-f378-4e60-94e6-0bfb3eeec37b","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b4243482-e06e-42d5-988c-a21f63079271","path":"sprites/sprUltraBigDogSleepHurt/sprUltraBigDogSleepHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6b9518a2-3aa0-4e89-87ad-b9962b315526","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9db14b93-242c-4813-ade5-bf90aa9b3754","path":"sprites/sprUltraBigDogSleepHurt/sprUltraBigDogSleepHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5778e572-571e-4b67-8037-a07ec19ce6b0","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":48,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Scrapyard",
    "path":"texturegroups/Scrapyard",
  },
  "type":0,
  "VTile":false,
  "width":96,
}