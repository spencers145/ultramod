{
  "$GMSprite":"",
  "%Name":"sprWeaponChest",
  "bboxMode":0,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":15,
  "bbox_top":1,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"bfae9776-fa15-4335-8c77-c5622ea3226e","name":"bfae9776-fa15-4335-8c77-c5622ea3226e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1bbf836d-50ac-45c2-a0ba-304911d4c6a9","name":"1bbf836d-50ac-45c2-a0ba-304911d4c6a9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8ec3a42a-05b2-4cc9-af3a-7a4abbdbe83e","name":"8ec3a42a-05b2-4cc9-af3a-7a4abbdbe83e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"85046816-4763-4c71-a498-819de605fafc","name":"85046816-4763-4c71-a498-819de605fafc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9679deff-09cb-4290-b835-92708a6bc4c3","name":"9679deff-09cb-4290-b835-92708a6bc4c3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"38942c72-337b-4f0b-bf09-d4bff7ee5549","name":"38942c72-337b-4f0b-bf09-d4bff7ee5549","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4bc63778-7126-46bf-a149-682331c09955","name":"4bc63778-7126-46bf-a149-682331c09955","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"2f8c2a1d-a845-4336-8746-a687aa620781","blendMode":0,"displayName":"default","isLocked":false,"name":"2f8c2a1d-a845-4336-8746-a687aa620781","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprWeaponChest",
  "nineSlice":{
    "$GMNineSliceData":"",
    "bottom":0,
    "enabled":false,
    "guideColour":[4294902015,4294902015,4294902015,4294902015,],
    "highlightColour":1728023040,
    "highlightStyle":0,
    "left":0,
    "resourceType":"GMNineSliceData",
    "resourceVersion":"2.0",
    "right":0,
    "tileMode":[
      0,
      0,
      0,
      0,
      0,
    ],
    "top":0,
  },
  "origin":4,
  "parent":{
    "name":"Chests",
    "path":"folders/Sprites/Chests.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprWeaponChest",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprWeaponChest",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bfae9776-fa15-4335-8c77-c5622ea3226e","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e468603f-7cb6-4d54-ac3c-0ff61a82f8e6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1bbf836d-50ac-45c2-a0ba-304911d4c6a9","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e01ba1c-36e2-465a-8862-166a6b8dafb4","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8ec3a42a-05b2-4cc9-af3a-7a4abbdbe83e","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d5f14e50-d1b1-4150-9ca6-7b8bf1660ddf","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"85046816-4763-4c71-a498-819de605fafc","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3ddb32db-1c8b-4438-a985-7fe7f26f32c4","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9679deff-09cb-4290-b835-92708a6bc4c3","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9a55df1e-3e24-4bf1-af52-f32e0f75f545","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"38942c72-337b-4f0b-bf09-d4bff7ee5549","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f186e518-7e8a-4dc7-97c2-27b52a095748","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4bc63778-7126-46bf-a149-682331c09955","path":"sprites/sprWeaponChest/sprWeaponChest.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9ff8d0c9-fd7c-4a2b-91c5-d400d2ca184f","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":16,
}