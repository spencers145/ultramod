{
  "$GMSprite":"",
  "%Name":"sprVenuzMenu",
  "bboxMode":1,
  "bbox_bottom":29,
  "bbox_left":8,
  "bbox_right":25,
  "bbox_top":6,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"650a590c-3c7b-4c9a-8bfa-2d6724f27b20","name":"650a590c-3c7b-4c9a-8bfa-2d6724f27b20","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"942631ea-932a-4980-90fb-a29d95abc203","name":"942631ea-932a-4980-90fb-a29d95abc203","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"35f431b5-0991-4c63-9998-e5a64057e20c","name":"35f431b5-0991-4c63-9998-e5a64057e20c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"35952b67-fea1-482d-b624-e57a17da0cc3","name":"35952b67-fea1-482d-b624-e57a17da0cc3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d0dd255b-98e7-4a53-a3d0-31b26d3a81d0","name":"d0dd255b-98e7-4a53-a3d0-31b26d3a81d0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e35b60d5-95d8-4c6d-aed0-26e0ce66e9c0","name":"e35b60d5-95d8-4c6d-aed0-26e0ce66e9c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3b4ce489-27c3-486b-9579-a6e550b5819d","name":"3b4ce489-27c3-486b-9579-a6e550b5819d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"34418e66-df51-4147-b809-92e21dd2018c","name":"34418e66-df51-4147-b809-92e21dd2018c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e5d17526-ec09-476c-be7e-7696c2cca341","name":"e5d17526-ec09-476c-be7e-7696c2cca341","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0ad68bd1-7719-4ad7-b8df-3cd65835d903","name":"0ad68bd1-7719-4ad7-b8df-3cd65835d903","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7843364b-0c22-47b3-bc07-5387c5406759","name":"7843364b-0c22-47b3-bc07-5387c5406759","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"82d7adae-c1ac-4d36-aeac-f098d6a128d4","name":"82d7adae-c1ac-4d36-aeac-f098d6a128d4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7ca77d68-0dec-487f-9bcd-513a5c26c271","name":"7ca77d68-0dec-487f-9bcd-513a5c26c271","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c0cdb7c9-be18-42ce-8ca3-77ae1dc44d98","name":"c0cdb7c9-be18-42ce-8ca3-77ae1dc44d98","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f4e8a54d-75dd-4a0f-97ca-d3e1a69c6892","name":"f4e8a54d-75dd-4a0f-97ca-d3e1a69c6892","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bb6c7394-6de2-4a6c-b161-f641f933b83b","name":"bb6c7394-6de2-4a6c-b161-f641f933b83b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"60d500a1-0c56-4888-b584-59a2bdac717d","name":"60d500a1-0c56-4888-b584-59a2bdac717d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3c1ad9c7-1616-47b8-8485-0a2151196530","name":"3c1ad9c7-1616-47b8-8485-0a2151196530","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"71753990-4037-4d89-bd68-5381637da480","name":"71753990-4037-4d89-bd68-5381637da480","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b2ff0a81-e2d5-456f-ac47-654447c9e984","name":"b2ff0a81-e2d5-456f-ac47-654447c9e984","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0e46700e-088f-4038-8466-03291b2680c3","name":"0e46700e-088f-4038-8466-03291b2680c3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a8745cc1-27fc-463e-a7b1-7448d8dc6a33","name":"a8745cc1-27fc-463e-a7b1-7448d8dc6a33","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3cb8576f-8439-4b4e-88c1-83539acd2fff","name":"3cb8576f-8439-4b4e-88c1-83539acd2fff","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6bf117d1-8f44-4903-9e1d-5e0f89c59034","name":"6bf117d1-8f44-4903-9e1d-5e0f89c59034","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fdf79d94-f4e4-4cfb-b715-5da4635956de","name":"fdf79d94-f4e4-4cfb-b715-5da4635956de","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c801d60f-deb6-4035-a3ce-5c4f4a78563b","name":"c801d60f-deb6-4035-a3ce-5c4f4a78563b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"068f8da0-b3f0-4469-8619-6bf9ebea45dd","name":"068f8da0-b3f0-4469-8619-6bf9ebea45dd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0cea1a14-40b6-49e8-b4bb-f4ecd6e46fd1","name":"0cea1a14-40b6-49e8-b4bb-f4ecd6e46fd1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2f81a99d-64c9-4eff-b604-77b58db0937f","name":"2f81a99d-64c9-4eff-b604-77b58db0937f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5adc7d35-7736-4abc-9d82-eae40dbf00dc","name":"5adc7d35-7736-4abc-9d82-eae40dbf00dc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f37c4b98-6a33-45ae-a41c-2abe69ad5d4f","name":"f37c4b98-6a33-45ae-a41c-2abe69ad5d4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"93d511c4-7b4b-494f-b8d2-94e9adb0b694","name":"93d511c4-7b4b-494f-b8d2-94e9adb0b694","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"09c37b1f-d49a-4c52-ae70-fd9cebdc3d2d","name":"09c37b1f-d49a-4c52-ae70-fd9cebdc3d2d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4d78e519-6aff-4784-8b1c-769aa995051f","blendMode":0,"displayName":"default","isLocked":false,"name":"4d78e519-6aff-4784-8b1c-769aa995051f","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprVenuzMenu",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"MenuChar",
    "path":"folders/Sprites/Menu/MenuChar.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":33.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"650a590c-3c7b-4c9a-8bfa-2d6724f27b20","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8e2859e0-6aa4-4d94-bf6c-a02a0ef5027a","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"942631ea-932a-4980-90fb-a29d95abc203","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c3dca42b-c0b2-4ddc-bb5e-7c8c3096c55e","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"35f431b5-0991-4c63-9998-e5a64057e20c","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae3e653e-70bb-4452-997f-1d01fc8166e7","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"35952b67-fea1-482d-b624-e57a17da0cc3","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0476db4f-7635-4b43-8656-e22c9e590a67","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d0dd255b-98e7-4a53-a3d0-31b26d3a81d0","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"273d9968-6d32-487e-b174-7b3eb96588ee","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e35b60d5-95d8-4c6d-aed0-26e0ce66e9c0","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"22b0f49a-4495-44c9-a225-cac14c76175f","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3b4ce489-27c3-486b-9579-a6e550b5819d","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9d21ca1a-33ca-4684-8f4c-840d70c9a27e","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"34418e66-df51-4147-b809-92e21dd2018c","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f0ff5cb1-3df7-4cb0-967c-fdb498bbfb9a","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e5d17526-ec09-476c-be7e-7696c2cca341","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cbeed0ac-0747-48bb-ac2b-36191d3dc8e6","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0ad68bd1-7719-4ad7-b8df-3cd65835d903","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e373bd06-c4f5-4b90-a0a0-07003e0e82c2","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7843364b-0c22-47b3-bc07-5387c5406759","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7c8cbaf5-a3f5-4dbe-bf8d-1d7e8f54484f","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"82d7adae-c1ac-4d36-aeac-f098d6a128d4","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"eb202965-3e77-422b-b8ea-ad821d885e3d","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7ca77d68-0dec-487f-9bcd-513a5c26c271","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"46a692e1-6e0a-4864-a285-90a9c0425265","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0cdb7c9-be18-42ce-8ca3-77ae1dc44d98","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5931a922-862c-4cb6-b09a-b8d9bc1fdc74","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f4e8a54d-75dd-4a0f-97ca-d3e1a69c6892","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f3a782f4-3b02-4280-b6a6-8d3c07d588e8","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bb6c7394-6de2-4a6c-b161-f641f933b83b","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c42dcb9e-1465-459f-aae6-b6d47ddfcde5","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"60d500a1-0c56-4888-b584-59a2bdac717d","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"50fb5c5c-faef-490b-baf5-de94ce197755","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3c1ad9c7-1616-47b8-8485-0a2151196530","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cd93bec5-ec8b-43d1-9ad8-0a6f94e0a598","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"71753990-4037-4d89-bd68-5381637da480","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"950a95cf-4e6e-491f-89a2-8fafa9a72b00","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b2ff0a81-e2d5-456f-ac47-654447c9e984","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"717928d7-d4a5-4536-aaa9-df80b096ff53","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0e46700e-088f-4038-8466-03291b2680c3","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f223da92-b863-4e58-9a33-568f51a49d76","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a8745cc1-27fc-463e-a7b1-7448d8dc6a33","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9ad5f722-a976-4e7a-90a8-4191405ea2b8","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3cb8576f-8439-4b4e-88c1-83539acd2fff","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"204ebdae-50af-4d11-8946-a3c92a778e35","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6bf117d1-8f44-4903-9e1d-5e0f89c59034","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d10f709f-6358-4cdf-9c41-8b5eb2c2604d","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fdf79d94-f4e4-4cfb-b715-5da4635956de","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f04549a3-6a46-4b8d-90f4-3fbd8ce82618","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c801d60f-deb6-4035-a3ce-5c4f4a78563b","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95787754-69a8-49c2-9ce3-93f7558c24fb","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"068f8da0-b3f0-4469-8619-6bf9ebea45dd","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d9ecafe3-6ca5-41ee-9f91-d6649cecbb68","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0cea1a14-40b6-49e8-b4bb-f4ecd6e46fd1","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c67f3fb7-4a64-4862-a1af-ae058cee6529","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2f81a99d-64c9-4eff-b604-77b58db0937f","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"efe61f3a-4806-46e1-b6ac-391a340cc63a","IsCreationKey":false,"Key":28.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5adc7d35-7736-4abc-9d82-eae40dbf00dc","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d6127e06-105e-4188-959e-b26d5bec58aa","IsCreationKey":false,"Key":29.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f37c4b98-6a33-45ae-a41c-2abe69ad5d4f","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0a576647-3821-47a0-a035-950cca40b8e4","IsCreationKey":false,"Key":30.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"93d511c4-7b4b-494f-b8d2-94e9adb0b694","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"65247dfe-b0bd-40c2-b6d3-1671111d2bd1","IsCreationKey":false,"Key":31.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"09c37b1f-d49a-4c52-ae70-fd9cebdc3d2d","path":"sprites/sprVenuzMenu/sprVenuzMenu.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8eddf836-4d77-4755-a444-d537a050850a","IsCreationKey":false,"Key":32.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":32,
}