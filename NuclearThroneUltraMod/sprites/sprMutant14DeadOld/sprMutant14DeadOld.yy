{
  "$GMSprite":"",
  "%Name":"sprMutant14DeadOld",
  "bboxMode":0,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":7,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"8ee5be0c-e45f-45cf-bb8f-b278e85c8570","name":"8ee5be0c-e45f-45cf-bb8f-b278e85c8570","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b1c5b393-f2a2-4deb-9c80-8c873d7b704a","name":"b1c5b393-f2a2-4deb-9c80-8c873d7b704a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fc66c2f8-d3c5-4409-ac9e-c62b4e73863f","name":"fc66c2f8-d3c5-4409-ac9e-c62b4e73863f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9c201954-2923-4879-8c1e-243c67de6602","name":"9c201954-2923-4879-8c1e-243c67de6602","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"80f29243-9cfb-46ed-b68d-cf9547b7d287","name":"80f29243-9cfb-46ed-b68d-cf9547b7d287","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"52bcb1a1-611b-44da-8f91-98e9847bf3b8","name":"52bcb1a1-611b-44da-8f91-98e9847bf3b8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"23af0c74-c788-4a7d-a4b4-eedeeb7a5e91","blendMode":0,"displayName":"default","isLocked":false,"name":"23af0c74-c788-4a7d-a4b4-eedeeb7a5e91","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant14DeadOld",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Panda",
    "path":"folders/Sprites/Player/Custom/Panda.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant14DeadOld",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant14DeadOld",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8ee5be0c-e45f-45cf-bb8f-b278e85c8570","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"55f3e73b-904a-4fa9-99c8-e8c3d99a0904","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b1c5b393-f2a2-4deb-9c80-8c873d7b704a","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9c02cadf-95e3-4778-ba78-0569cf317247","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fc66c2f8-d3c5-4409-ac9e-c62b4e73863f","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"05f87e98-0314-4e46-9f78-2e9a2c30b3ee","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9c201954-2923-4879-8c1e-243c67de6602","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"82989456-437b-4a08-9f29-c8d92e97c4b4","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"80f29243-9cfb-46ed-b68d-cf9547b7d287","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"421b6552-747b-4e76-ba67-60e6f652a685","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"52bcb1a1-611b-44da-8f91-98e9847bf3b8","path":"sprites/sprMutant14DeadOld/sprMutant14DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a541635d-12ba-4135-9e25-65aea980fbcc","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":9,
    "yorigin":9,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":24,
}