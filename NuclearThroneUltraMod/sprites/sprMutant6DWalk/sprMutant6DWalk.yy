{
  "$GMSprite":"",
  "%Name":"sprMutant6DWalk",
  "bboxMode":1,
  "bbox_bottom":19,
  "bbox_left":0,
  "bbox_right":17,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"aa4f40d3-e13c-47dc-aef2-aa89ee1a3789","name":"aa4f40d3-e13c-47dc-aef2-aa89ee1a3789","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4a57af28-d41a-4f15-bd6d-f4629baf40ed","name":"4a57af28-d41a-4f15-bd6d-f4629baf40ed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3b981354-4047-4f01-9abb-270ed5484aff","name":"3b981354-4047-4f01-9abb-270ed5484aff","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a435d16-b065-4d7d-af08-de332cf2a2e4","name":"1a435d16-b065-4d7d-af08-de332cf2a2e4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f00410f0-248e-4737-9f49-6258989331f0","name":"f00410f0-248e-4737-9f49-6258989331f0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"46d108f7-f51a-43bd-adb0-53af9035cf9f","name":"46d108f7-f51a-43bd-adb0-53af9035cf9f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":20,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"71330c62-f60b-47fa-878d-791b05b908e2","blendMode":0,"displayName":"default","isLocked":false,"name":"71330c62-f60b-47fa-878d-791b05b908e2","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant6DWalk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"YV",
    "path":"folders/Sprites/Player/YV.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant6DWalk",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant6DWalk",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"aa4f40d3-e13c-47dc-aef2-aa89ee1a3789","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"437c3100-c6cf-487e-9389-b88302885399","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4a57af28-d41a-4f15-bd6d-f4629baf40ed","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c7ad3437-5097-41b2-87f9-2422ad792aef","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3b981354-4047-4f01-9abb-270ed5484aff","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"694bfa05-89b0-42b6-a598-e7b44ca32856","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a435d16-b065-4d7d-af08-de332cf2a2e4","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"357feed4-da52-4a44-9836-8db940a2e550","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f00410f0-248e-4737-9f49-6258989331f0","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cd1f38bc-5145-4482-9972-afef87c3f715","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"46d108f7-f51a-43bd-adb0-53af9035cf9f","path":"sprites/sprMutant6DWalk/sprMutant6DWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e2029cf-3221-443a-84f9-795cb7356eda","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":9,
    "yorigin":10,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":18,
}