{
  "$GMSprite":"",
  "%Name":"sprCrown11DeadEnemy",
  "bboxMode":1,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":15,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"0592e512-4e30-411e-a6bd-c7a117887fc2","name":"0592e512-4e30-411e-a6bd-c7a117887fc2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"62babe40-45db-442b-b73e-1409336d0edc","name":"62babe40-45db-442b-b73e-1409336d0edc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"95e0777b-c9ac-4692-9ee2-a47bc5b43b0b","name":"95e0777b-c9ac-4692-9ee2-a47bc5b43b0b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e0c1a0ac-7b25-4ac3-a26d-72b50810cc65","name":"e0c1a0ac-7b25-4ac3-a26d-72b50810cc65","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"1079212e-e18f-4c5c-9da6-3b4dd77bb3a2","blendMode":0,"displayName":"default","isLocked":false,"name":"1079212e-e18f-4c5c-9da6-3b4dd77bb3a2","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprCrown11DeadEnemy",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"CrownVault",
    "path":"folders/Sprites/CrownVault.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprCrown11DeadEnemy",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprCrown11DeadEnemy",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0592e512-4e30-411e-a6bd-c7a117887fc2","path":"sprites/sprCrown11DeadEnemy/sprCrown11DeadEnemy.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9c8b76f4-4826-4ac2-9ea0-29f48d6377fe","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"62babe40-45db-442b-b73e-1409336d0edc","path":"sprites/sprCrown11DeadEnemy/sprCrown11DeadEnemy.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fdc761ac-e151-4d96-9dda-d8b650dc1509","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"95e0777b-c9ac-4692-9ee2-a47bc5b43b0b","path":"sprites/sprCrown11DeadEnemy/sprCrown11DeadEnemy.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"93763b54-d0e8-4166-876f-8676896d6206","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e0c1a0ac-7b25-4ac3-a26d-72b50810cc65","path":"sprites/sprCrown11DeadEnemy/sprCrown11DeadEnemy.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2e8b396f-6b41-4e83-bb43-99e212b3fbd6","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"CrownVault",
    "path":"texturegroups/CrownVault",
  },
  "type":0,
  "VTile":false,
  "width":16,
}