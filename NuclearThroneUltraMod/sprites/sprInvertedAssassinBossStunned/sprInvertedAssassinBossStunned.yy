{
  "$GMSprite":"",
  "%Name":"sprInvertedAssassinBossStunned",
  "bboxMode":0,
  "bbox_bottom":23,
  "bbox_left":3,
  "bbox_right":24,
  "bbox_top":4,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"e0759f56-a203-4414-9bdb-1ee14a88a7e9","name":"e0759f56-a203-4414-9bdb-1ee14a88a7e9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0e5e1912-f69b-4e5b-99a1-e2a04ff6702d","name":"0e5e1912-f69b-4e5b-99a1-e2a04ff6702d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"068df8f5-119d-4a19-a647-21ca463d03f3","name":"068df8f5-119d-4a19-a647-21ca463d03f3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dfdf7c1b-561a-4e62-9b53-c6b2f31f0309","name":"dfdf7c1b-561a-4e62-9b53-c6b2f31f0309","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"86c3730a-3182-47ee-a94a-5c99174763b8","name":"86c3730a-3182-47ee-a94a-5c99174763b8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"49959c0b-aedb-4e7c-837d-69b4f2c17f8a","name":"49959c0b-aedb-4e7c-837d-69b4f2c17f8a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"89bcc77e-b895-4d20-a0d1-241f56130326","name":"89bcc77e-b895-4d20-a0d1-241f56130326","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2a65b924-67a4-4466-8ff9-b1942a4a2f12","name":"2a65b924-67a4-4466-8ff9-b1942a4a2f12","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a5a6d1a8-6849-4442-b651-2708a971b9c1","name":"a5a6d1a8-6849-4442-b651-2708a971b9c1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2c85bbe6-b35e-47cc-a266-fe2a53123c45","name":"2c85bbe6-b35e-47cc-a266-fe2a53123c45","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"16a5fb2b-f31f-496d-a854-98f09ce9b2fb","name":"16a5fb2b-f31f-496d-a854-98f09ce9b2fb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"132ed78f-63b2-48b0-85cd-b577a552b62f","name":"132ed78f-63b2-48b0-85cd-b577a552b62f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3184545-5d54-436d-94aa-fe0818c197ba","name":"c3184545-5d54-436d-94aa-fe0818c197ba","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"23e706cb-2866-40b5-aad4-aeff0a131059","name":"23e706cb-2866-40b5-aad4-aeff0a131059","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f4d6ca25-324c-4fe4-819d-b158758b6c64","name":"f4d6ca25-324c-4fe4-819d-b158758b6c64","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":28,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"d5ee37c3-e5ca-483c-9acb-57bc6b7c0fe8","blendMode":0,"displayName":"default","isLocked":false,"name":"d5ee37c3-e5ca-483c-9acb-57bc6b7c0fe8","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedAssassinBossStunned",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"AssassinBoss",
    "path":"folders/Sprites/Enemies/Boss/AssassinBoss.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedAssassinBossStunned",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":15.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedAssassinBossStunned",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e0759f56-a203-4414-9bdb-1ee14a88a7e9","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c4dd676e-579e-40b8-8cb7-3e824a60a2aa","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0e5e1912-f69b-4e5b-99a1-e2a04ff6702d","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a1915c61-6bfd-4e06-9ff2-94495cd65988","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"068df8f5-119d-4a19-a647-21ca463d03f3","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"93a04c04-4447-4cbf-ab67-9d88ed0c0a66","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dfdf7c1b-561a-4e62-9b53-c6b2f31f0309","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"14426367-2413-48fa-9954-7f3c4d19e766","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"86c3730a-3182-47ee-a94a-5c99174763b8","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aec3819c-640c-4680-92d0-a7494937307b","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"49959c0b-aedb-4e7c-837d-69b4f2c17f8a","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0137673d-368d-4b36-98f7-2775300e307b","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"89bcc77e-b895-4d20-a0d1-241f56130326","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"eb59e5b0-98bc-4457-a840-a6dd57f48fee","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2a65b924-67a4-4466-8ff9-b1942a4a2f12","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e94e9032-a711-4473-9132-175d703c9972","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a5a6d1a8-6849-4442-b651-2708a971b9c1","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d2190f33-270a-4da6-a36f-bad3bbe48186","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2c85bbe6-b35e-47cc-a266-fe2a53123c45","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"493cdcad-2cde-4f25-b969-3112b1c4512a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"16a5fb2b-f31f-496d-a854-98f09ce9b2fb","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"03a13b26-e5fc-4ab6-a2cd-7245f0f24efd","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"132ed78f-63b2-48b0-85cd-b577a552b62f","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4ca8515a-0979-4564-91f2-e04f5efc140b","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3184545-5d54-436d-94aa-fe0818c197ba","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4c9d86e4-bca5-4989-b088-812cdf45cd24","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"23e706cb-2866-40b5-aad4-aeff0a131059","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2692e9a5-a8e4-43b5-8554-35b605ab69a3","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f4d6ca25-324c-4fe4-819d-b158758b6c64","path":"sprites/sprInvertedAssassinBossStunned/sprInvertedAssassinBossStunned.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"64c44395-301f-4f21-aad0-93acd896c7a5","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":14,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Scrapyard",
    "path":"texturegroups/Scrapyard",
  },
  "type":0,
  "VTile":false,
  "width":26,
}