{
  "$GMSprite":"",
  "%Name":"sprInvertedCubeGuardianFire",
  "bboxMode":0,
  "bbox_bottom":89,
  "bbox_left":8,
  "bbox_right":68,
  "bbox_top":20,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"056da59c-7d56-4072-b2f2-96ab632a0188","name":"056da59c-7d56-4072-b2f2-96ab632a0188","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"842eac22-b701-496b-8c59-93c5af3174d4","name":"842eac22-b701-496b-8c59-93c5af3174d4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5906e08-4528-4e56-aa43-297681a57764","name":"c5906e08-4528-4e56-aa43-297681a57764","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7cb37eda-60af-48b7-9a41-c837badd99c0","name":"7cb37eda-60af-48b7-9a41-c837badd99c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"de5796cd-6810-40d7-8adb-44b2d0632039","name":"de5796cd-6810-40d7-8adb-44b2d0632039","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cf58d171-6037-4299-91f2-bef5efb53770","name":"cf58d171-6037-4299-91f2-bef5efb53770","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f255bc14-fc31-42dc-b4dd-161971481dc1","name":"f255bc14-fc31-42dc-b4dd-161971481dc1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"91d0c78b-c6df-4467-8194-fb08ec229338","name":"91d0c78b-c6df-4467-8194-fb08ec229338","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a0f553ee-d159-48e0-9549-756d74de7eb6","name":"a0f553ee-d159-48e0-9549-756d74de7eb6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"543a64cb-551c-4806-8e1d-12f21e58e283","name":"543a64cb-551c-4806-8e1d-12f21e58e283","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5faf92c3-7b2f-4f74-909d-09f971b57259","name":"5faf92c3-7b2f-4f74-909d-09f971b57259","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d661dc90-f157-462d-be8e-eac8d87dff60","name":"d661dc90-f157-462d-be8e-eac8d87dff60","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":100,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5269b1c6-2687-4cc8-aa83-d0485794dc3c","blendMode":0,"displayName":"default","isLocked":false,"name":"5269b1c6-2687-4cc8-aa83-d0485794dc3c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedCubeGuardianFire",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"InvertedPalaceEnemies",
    "path":"folders/Sprites/Palace/Palace Enemy/InvertedPalaceEnemies.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedCubeGuardianFire",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":28.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedCubeGuardianFire",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9113d499-43c1-4b5c-be93-48230560f244","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e5ddac3b-b1d8-461c-ad39-b995c09abf4e","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"056da59c-7d56-4072-b2f2-96ab632a0188","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bf096b6b-f30b-46a2-ae60-cbb6fb65e7b4","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"842eac22-b701-496b-8c59-93c5af3174d4","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"430645d3-7e8e-4539-ac10-d3a3fe300c5e","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5906e08-4528-4e56-aa43-297681a57764","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"13828c94-cdbc-412f-8e28-12d74de2a327","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7f53ed92-303a-4582-8016-6e20ce020961","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7cb37eda-60af-48b7-9a41-c837badd99c0","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"77996a48-dacd-4ac1-a18e-4f188f34bbb9","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f18c4d98-c698-45ac-9adb-e565f2115851","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"de5796cd-6810-40d7-8adb-44b2d0632039","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f4d533d7-a70c-484b-82be-a799542dc90b","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cf58d171-6037-4299-91f2-bef5efb53770","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"449c41bf-8b2b-47c7-88ae-f4d8078b11d5","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f255bc14-fc31-42dc-b4dd-161971481dc1","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"53a85bae-e23c-4f51-975b-2b6b3ea3a691","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a76914c3-2e40-4e63-87f5-028574c1b9bd","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"97d7bfd6-a98d-4b90-af15-bc23fc5802a0","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aecd49ab-df28-41c2-85f2-7ee9078c9b48","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91d0c78b-c6df-4467-8194-fb08ec229338","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"41b86447-7619-465f-a95d-89ead040695e","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0b6dcf35-9a76-4841-9b2e-6e2ecfc35c21","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a0f553ee-d159-48e0-9549-756d74de7eb6","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"de1122c4-4b5f-450a-af61-671b9d87c907","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9aeef8ac-c94d-4ad1-9d69-dd5832b45647","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b283b32d-9c59-41be-b6eb-910474520673","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"543a64cb-551c-4806-8e1d-12f21e58e283","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5eac5104-f17a-4a5d-8940-7114553deae4","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aa3d6a99-8274-4d8a-afe4-fbe76323eed8","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a74917b0-71ee-4451-8426-670413de5df3","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e18ae3c-8822-4b4e-8a96-0a8382413684","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d047697a-6f3a-4a52-bf85-98ba9254c2c2","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5faf92c3-7b2f-4f74-909d-09f971b57259","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"892642d0-0565-4418-8a9c-8a7256aa686e","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0b02210a-4397-443f-90a5-6915e76ee8a8","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d661dc90-f157-462d-be8e-eac8d87dff60","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"290fa376-146c-467f-a224-da0298f02a19","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","path":"sprites/sprInvertedCubeGuardianFire/sprInvertedCubeGuardianFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1534e051-749f-45b6-b36b-042a802b8617","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":41,
    "yorigin":70,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":80,
}