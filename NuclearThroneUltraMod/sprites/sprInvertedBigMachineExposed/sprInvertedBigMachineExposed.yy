{
  "$GMSprite":"",
  "%Name":"sprInvertedBigMachineExposed",
  "bboxMode":0,
  "bbox_bottom":123,
  "bbox_left":0,
  "bbox_right":157,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"32b6300e-3565-48a4-ba8e-29d268ee421e","name":"32b6300e-3565-48a4-ba8e-29d268ee421e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5989a543-99c2-471c-af8b-c05664240bf1","name":"5989a543-99c2-471c-af8b-c05664240bf1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3ec85e41-937f-450b-9004-b7e606fef894","name":"3ec85e41-937f-450b-9004-b7e606fef894","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"df801dad-e2e8-4b8c-93b1-a8d76dde6add","name":"df801dad-e2e8-4b8c-93b1-a8d76dde6add","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":124,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"2b0729dd-1abd-4d9c-9df2-4ed0f5989606","blendMode":0,"displayName":"default","isLocked":false,"name":"2b0729dd-1abd-4d9c-9df2-4ed0f5989606","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedBigMachineExposed",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigMachine",
    "path":"folders/Sprites/Enemies/Boss/BigMachine.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"32b6300e-3565-48a4-ba8e-29d268ee421e","path":"sprites/sprInvertedBigMachineExposed/sprInvertedBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"70612566-d07c-4f02-98c3-28184e4f9536","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5989a543-99c2-471c-af8b-c05664240bf1","path":"sprites/sprInvertedBigMachineExposed/sprInvertedBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d6fb1626-92a4-4ced-aeba-27567b48c9ee","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3ec85e41-937f-450b-9004-b7e606fef894","path":"sprites/sprInvertedBigMachineExposed/sprInvertedBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a40eb04e-7b1f-4e40-acaa-af34941dd8ff","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"df801dad-e2e8-4b8c-93b1-a8d76dde6add","path":"sprites/sprInvertedBigMachineExposed/sprInvertedBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"300b1bfe-4dd2-4be4-8515-1ff08ae52f40","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":79,
    "yorigin":62,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedLabs",
    "path":"texturegroups/InvertedLabs",
  },
  "type":0,
  "VTile":false,
  "width":158,
}