{
  "$GMSprite":"",
  "%Name":"sprBoomHandsTryout",
  "bboxMode":0,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"78ace55a-a7e7-4034-81c2-2e05560e0bf9","name":"78ace55a-a7e7-4034-81c2-2e05560e0bf9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"273afcba-3914-49c2-a7ae-86ba892f925b","name":"273afcba-3914-49c2-a7ae-86ba892f925b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"05a2d5a6-c545-4f21-aa91-6834c1d3e9eb","name":"05a2d5a6-c545-4f21-aa91-6834c1d3e9eb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5cf29562-ef41-4c47-ad70-2698d2d8d73d","name":"5cf29562-ef41-4c47-ad70-2698d2d8d73d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5d57e385-9eb9-4ca5-a378-d2939ac1c26a","name":"5d57e385-9eb9-4ca5-a378-d2939ac1c26a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3f435863-d7e5-471c-96fc-29a00b12e740","name":"3f435863-d7e5-471c-96fc-29a00b12e740","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2f33a09-f37b-4812-92f4-07c3cf10644b","name":"d2f33a09-f37b-4812-92f4-07c3cf10644b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"88d7487e-8fe2-41f2-bac2-cb2fe72af074","name":"88d7487e-8fe2-41f2-bac2-cb2fe72af074","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c2a615fb-c588-4a82-8ac4-7f0425a46309","name":"c2a615fb-c588-4a82-8ac4-7f0425a46309","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6761f66d-934e-4868-81bd-a54693f1b243","name":"6761f66d-934e-4868-81bd-a54693f1b243","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"85da263e-0e0e-4935-8e54-69dd5766f2c1","name":"85da263e-0e0e-4935-8e54-69dd5766f2c1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9bbe4212-fee7-40c3-ad3b-cf9bb9d69818","name":"9bbe4212-fee7-40c3-ad3b-cf9bb9d69818","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"8fea9336-17fc-4a7b-a616-84f98fb56472","blendMode":0,"displayName":"default","isLocked":false,"name":"8fea9336-17fc-4a7b-a616-84f98fb56472","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"ce55eff5-d180-4705-bb7d-ba577d86d8e6","blendMode":0,"displayName":"default","isLocked":false,"name":"ce55eff5-d180-4705-bb7d-ba577d86d8e6","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprBoomHandsTryout",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Ultras",
    "path":"folders/Sprites/Menu/Ultras.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprBoomHandsTryout",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":12.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprBoomHandsTryout",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"78ace55a-a7e7-4034-81c2-2e05560e0bf9","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9f7a4dbe-7742-46e0-90bc-ae15b78d9fe6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"273afcba-3914-49c2-a7ae-86ba892f925b","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5343bd5b-aeff-4df1-94a7-490d5efe8279","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"05a2d5a6-c545-4f21-aa91-6834c1d3e9eb","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f9ec74ee-a27a-46b5-b84b-ff27a2a7b22b","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5cf29562-ef41-4c47-ad70-2698d2d8d73d","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea73ea0a-0550-4621-9fb5-f38b402c02c2","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5d57e385-9eb9-4ca5-a378-d2939ac1c26a","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"42eda790-de22-4713-a411-cb479dbbf5ea","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3f435863-d7e5-471c-96fc-29a00b12e740","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a1b79002-71f9-4e25-9690-788bddf01e82","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2f33a09-f37b-4812-92f4-07c3cf10644b","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b4554255-f1ac-4a19-97c1-b065e9f05c49","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"88d7487e-8fe2-41f2-bac2-cb2fe72af074","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"490dd24f-7c28-43eb-9740-ac7ae414121b","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c2a615fb-c588-4a82-8ac4-7f0425a46309","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dc015f50-698b-4a50-8e72-2cb1f0d4287e","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6761f66d-934e-4868-81bd-a54693f1b243","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af847b71-dfc4-4310-8448-9760760ca422","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"85da263e-0e0e-4935-8e54-69dd5766f2c1","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b756872b-286a-43b7-8e5f-c466e8b3506c","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9bbe4212-fee7-40c3-ad3b-cf9bb9d69818","path":"sprites/sprBoomHandsTryout/sprBoomHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"91e4f1a9-dce0-42e8-bf08-f5b44ef90ad2","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"MainMenu",
    "path":"texturegroups/MainMenu",
  },
  "type":0,
  "VTile":false,
  "width":24,
}