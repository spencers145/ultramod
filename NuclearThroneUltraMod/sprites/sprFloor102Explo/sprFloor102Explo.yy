{
  "$GMSprite":"",
  "%Name":"sprFloor102Explo",
  "bboxMode":1,
  "bbox_bottom":17,
  "bbox_left":0,
  "bbox_right":17,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"fa097668-f8be-4a0e-b6f7-7c9421c43c92","name":"fa097668-f8be-4a0e-b6f7-7c9421c43c92","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"27fcfab4-c5a5-4713-8c24-d7a4d9660e11","name":"27fcfab4-c5a5-4713-8c24-d7a4d9660e11","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1be01bcf-58fa-48e3-a39c-09963cbc0746","name":"1be01bcf-58fa-48e3-a39c-09963cbc0746","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"11c58daf-0564-4eab-9a93-49ad1dfd5681","name":"11c58daf-0564-4eab-9a93-49ad1dfd5681","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":18,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"06ff6a1f-7068-4e2d-9dd2-30e180a05637","blendMode":0,"displayName":"default","isLocked":false,"name":"06ff6a1f-7068-4e2d-9dd2-30e180a05637","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprFloor102Explo",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"PizzaSewers",
    "path":"folders/Sprites/Enviroment/Tiles/Sewers/PizzaSewers.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fa097668-f8be-4a0e-b6f7-7c9421c43c92","path":"sprites/sprFloor102Explo/sprFloor102Explo.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9cb6c3d3-3a23-42c5-ac0a-ad4ac28e3ecd","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"27fcfab4-c5a5-4713-8c24-d7a4d9660e11","path":"sprites/sprFloor102Explo/sprFloor102Explo.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"30dbf9db-eb6b-4cda-8ce2-297376483ae1","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1be01bcf-58fa-48e3-a39c-09963cbc0746","path":"sprites/sprFloor102Explo/sprFloor102Explo.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b1acc14f-3238-4935-ab5a-748ae3ccb7b7","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"11c58daf-0564-4eab-9a93-49ad1dfd5681","path":"sprites/sprFloor102Explo/sprFloor102Explo.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"702f65a6-ba00-421b-a719-7d1898e72efb","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":1,
    "yorigin":1,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Sewers",
    "path":"texturegroups/Sewers",
  },
  "type":0,
  "VTile":false,
  "width":18,
}