{
  "$GMSprite":"",
  "%Name":"sprDmgNumbersBig",
  "bboxMode":0,
  "bbox_bottom":14,
  "bbox_left":0,
  "bbox_right":12,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"f5c35444-1937-48ef-b0bc-8ef85d89b28b","name":"f5c35444-1937-48ef-b0bc-8ef85d89b28b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f6d76440-fccc-47a8-ac77-1d616f5dd35c","name":"f6d76440-fccc-47a8-ac77-1d616f5dd35c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6b5835c6-cd9e-4297-b9ea-8b35321bda9f","name":"6b5835c6-cd9e-4297-b9ea-8b35321bda9f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3c757e91-54a1-49bf-8828-13b0c75555eb","name":"3c757e91-54a1-49bf-8828-13b0c75555eb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"442ddb78-5c28-4201-8d9a-257010a4435d","name":"442ddb78-5c28-4201-8d9a-257010a4435d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e89b2f09-0501-4fe7-ae3c-e29550b01913","name":"e89b2f09-0501-4fe7-ae3c-e29550b01913","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"318c664f-fe5a-4860-bce6-5745b6b56b2e","name":"318c664f-fe5a-4860-bce6-5745b6b56b2e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a05afe6-43d6-47e3-9f7d-ee6de8b3bd2f","name":"1a05afe6-43d6-47e3-9f7d-ee6de8b3bd2f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6f97ae78-5273-4a9c-9eac-61490ffaebbc","name":"6f97ae78-5273-4a9c-9eac-61490ffaebbc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"51a7f556-266c-44e5-9f0f-56a37916a501","name":"51a7f556-266c-44e5-9f0f-56a37916a501","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":15,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5cec997e-f261-4eb8-99ea-4e650d7ae45a","blendMode":0,"displayName":"default","isLocked":false,"name":"5cec997e-f261-4eb8-99ea-4e650d7ae45a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprDmgNumbersBig",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"Menu",
    "path":"folders/Sprites/Menu/Menu.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprDmgNumbersBig",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":10.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprDmgNumbersBig",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f5c35444-1937-48ef-b0bc-8ef85d89b28b","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8b4b10bd-0fae-433d-bb92-616c3a60bc31","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f6d76440-fccc-47a8-ac77-1d616f5dd35c","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"33c1cdd4-7b56-40d0-a22c-91438d829245","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6b5835c6-cd9e-4297-b9ea-8b35321bda9f","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e7bc9b91-4f80-4cd9-abf7-0fe7ca1a3714","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3c757e91-54a1-49bf-8828-13b0c75555eb","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae026bed-14b2-43a6-a780-5e0ab92c6157","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"442ddb78-5c28-4201-8d9a-257010a4435d","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"29720a14-8244-4ea3-8777-6064273ed6ff","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e89b2f09-0501-4fe7-ae3c-e29550b01913","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6331910b-30fc-4d70-8de0-be21fa97d86c","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"318c664f-fe5a-4860-bce6-5745b6b56b2e","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"70ce78e3-e070-4a1c-bdc7-f6c01a6178e8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a05afe6-43d6-47e3-9f7d-ee6de8b3bd2f","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"88973056-9c91-4c8a-bd42-e94c7deecf9f","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6f97ae78-5273-4a9c-9eac-61490ffaebbc","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"15384562-edaf-4c91-9fd7-dfdacf7a3736","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"51a7f556-266c-44e5-9f0f-56a37916a501","path":"sprites/sprDmgNumbersBig/sprDmgNumbersBig.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6962128a-e962-4123-8f38-c42edeef81cd","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":13,
}