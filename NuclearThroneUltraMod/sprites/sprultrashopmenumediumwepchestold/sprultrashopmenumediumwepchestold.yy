{
  "$GMSprite":"",
  "%Name":"sprUltraShopMenuMediumWepChestOld",
  "bboxMode":0,
  "bbox_bottom":83,
  "bbox_left":1,
  "bbox_right":37,
  "bbox_top":25,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"b3c881f0-57f0-4604-a437-ba6af04b40de","name":"b3c881f0-57f0-4604-a437-ba6af04b40de","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"91807d24-c179-4ea5-9a8f-10d8ae5cbbae","name":"91807d24-c179-4ea5-9a8f-10d8ae5cbbae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":166,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"068e5b4e-f7f8-4f55-b703-f89caecfbd33","blendMode":0,"displayName":"default","isLocked":false,"name":"068e5b4e-f7f8-4f55-b703-f89caecfbd33","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprUltraShopMenuMediumWepChestOld",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BusinessHog",
    "path":"folders/Sprites/Player/Custom/BusinessHog.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprUltraShopMenuMediumWepChestOld",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":2.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprUltraShopMenuMediumWepChestOld",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b3c881f0-57f0-4604-a437-ba6af04b40de","path":"sprites/sprUltraShopMenuMediumWepChestOld/sprUltraShopMenuMediumWepChestOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5a1d53df-32f2-4cdf-aa89-09313414527b","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91807d24-c179-4ea5-9a8f-10d8ae5cbbae","path":"sprites/sprUltraShopMenuMediumWepChestOld/sprUltraShopMenuMediumWepChestOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"511acbbf-1d1d-4455-997c-a2c89e65635c","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":83,
    "yorigin":83,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":166,
}