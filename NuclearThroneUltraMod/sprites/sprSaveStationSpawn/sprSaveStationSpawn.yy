{
  "$GMSprite":"",
  "%Name":"sprSaveStationSpawn",
  "bboxMode":0,
  "bbox_bottom":40,
  "bbox_left":20,
  "bbox_right":43,
  "bbox_top":16,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6429de1a-3b14-4a70-8b2e-dd4c30f4b5e2","name":"6429de1a-3b14-4a70-8b2e-dd4c30f4b5e2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f0c2999a-8d55-41f0-ab2a-84467b57b611","name":"f0c2999a-8d55-41f0-ab2a-84467b57b611","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ba5e26a2-c926-43d7-b127-09ba12b8beb2","name":"ba5e26a2-c926-43d7-b127-09ba12b8beb2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1ef71ebb-3e84-40b7-80a3-4a1ab1d6ec57","name":"1ef71ebb-3e84-40b7-80a3-4a1ab1d6ec57","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fb891be6-819f-4126-b029-f6d79e034740","name":"fb891be6-819f-4126-b029-f6d79e034740","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"217c03e6-a1d0-4747-aee0-af7b0f70f777","name":"217c03e6-a1d0-4747-aee0-af7b0f70f777","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ec84ae0d-258e-418c-b113-4676f57eedc2","name":"ec84ae0d-258e-418c-b113-4676f57eedc2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"03b4f13c-a602-4593-902c-e264b8d8c7f1","name":"03b4f13c-a602-4593-902c-e264b8d8c7f1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"996092fe-9956-4513-af08-d8c200b83a2d","name":"996092fe-9956-4513-af08-d8c200b83a2d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"da5d05c5-ae68-4917-a8cb-b959a83562c4","name":"da5d05c5-ae68-4917-a8cb-b959a83562c4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"761dedf7-f3da-458f-8a26-5134614dba41","name":"761dedf7-f3da-458f-8a26-5134614dba41","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"81611f8b-c9bc-4f8e-b84c-ed6e5ecbff1c","name":"81611f8b-c9bc-4f8e-b84c-ed6e5ecbff1c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b65d0bf3-6116-48a4-9fcc-b825b7e4e26c","name":"b65d0bf3-6116-48a4-9fcc-b825b7e4e26c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0961fa70-7a65-48a3-91a0-c1305675c908","name":"0961fa70-7a65-48a3-91a0-c1305675c908","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"77681d2e-733f-48c0-9bee-d63ba64332e8","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"77681d2e-733f-48c0-9bee-d63ba64332e8","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"f8da4e70-5a07-45d6-86f0-47aee321d4bd","blendMode":0,"displayName":"default","isLocked":false,"name":"f8da4e70-5a07-45d6-86f0-47aee321d4bd","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSaveStationSpawn",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Statues",
    "path":"folders/Sprites/Enemies/Boss/Statues.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprSaveStationSpawn",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":14.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprSaveStationSpawn",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6429de1a-3b14-4a70-8b2e-dd4c30f4b5e2","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"71024aed-6a35-49dc-99d0-ffac88dc5912","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f0c2999a-8d55-41f0-ab2a-84467b57b611","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c5cad75b-84ab-41a2-b3a7-6bc2501a93d0","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ba5e26a2-c926-43d7-b127-09ba12b8beb2","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d42b073c-43b6-4559-aced-aec4a9a4eedc","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1ef71ebb-3e84-40b7-80a3-4a1ab1d6ec57","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d867f613-fd1e-4686-825f-7cdfaf0c0b7c","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fb891be6-819f-4126-b029-f6d79e034740","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"827d61fd-fcfe-45de-9924-363cdc9ce3f3","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"217c03e6-a1d0-4747-aee0-af7b0f70f777","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dd0a5ad7-7bdb-48fc-a19f-9dd6ebba382e","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ec84ae0d-258e-418c-b113-4676f57eedc2","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b78ce35c-c60f-42a4-85cc-0b1080093a65","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"03b4f13c-a602-4593-902c-e264b8d8c7f1","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7bea40bb-005f-4a86-8a1e-49e3d0b97c60","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"996092fe-9956-4513-af08-d8c200b83a2d","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af0158b2-4e6d-4292-99af-2ee4e751ab9f","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"da5d05c5-ae68-4917-a8cb-b959a83562c4","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e8dcb707-df40-409c-90e4-53b621df9551","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"761dedf7-f3da-458f-8a26-5134614dba41","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"07c5f651-518b-40d6-9cb9-d6910f9f53f1","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"81611f8b-c9bc-4f8e-b84c-ed6e5ecbff1c","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"38326b69-b560-47df-989e-a20875a6ada0","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b65d0bf3-6116-48a4-9fcc-b825b7e4e26c","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c3697f8f-87ed-4cf0-937c-22157fe2aad6","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0961fa70-7a65-48a3-91a0-c1305675c908","path":"sprites/sprSaveStationSpawn/sprSaveStationSpawn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b0186b38-eef0-48be-8e62-f553043781cc","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":32,
    "yorigin":32,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":64,
}