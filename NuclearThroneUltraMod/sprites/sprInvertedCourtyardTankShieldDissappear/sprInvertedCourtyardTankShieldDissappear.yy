{
  "$GMSprite":"",
  "%Name":"sprInvertedCourtyardTankShieldDissappear",
  "bboxMode":0,
  "bbox_bottom":24,
  "bbox_left":5,
  "bbox_right":20,
  "bbox_top":6,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":31,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"64d0a591-b748-4b28-892d-db16e7c71470","blendMode":0,"displayName":"default","isLocked":false,"name":"64d0a591-b748-4b28-892d-db16e7c71470","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedCourtyardTankShieldDissappear",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"CrownCourtyard",
    "path":"folders/Objects/Enemies/CrownCourtyard.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedCourtyardTankShieldDissappear",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedCourtyardTankShieldDissappear",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","path":"sprites/sprInvertedCourtyardTankShieldDissappear/sprInvertedCourtyardTankShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6056ce16-4745-45c6-b7ce-bc37f3cc710e","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","path":"sprites/sprInvertedCourtyardTankShieldDissappear/sprInvertedCourtyardTankShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e0306c03-57bc-42e6-898d-96337c49901a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","path":"sprites/sprInvertedCourtyardTankShieldDissappear/sprInvertedCourtyardTankShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5bcc1afd-d248-4ad8-ada5-03580b711b3e","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":15,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":26,
}