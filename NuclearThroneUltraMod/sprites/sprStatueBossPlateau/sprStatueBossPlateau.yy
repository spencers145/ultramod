{
  "$GMSprite":"",
  "%Name":"sprStatueBossPlateau",
  "bboxMode":0,
  "bbox_bottom":63,
  "bbox_left":0,
  "bbox_right":63,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"652feb38-4774-48d0-b2cd-9b8a121dc3fa","name":"652feb38-4774-48d0-b2cd-9b8a121dc3fa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"229a0e6e-5398-4192-9fde-11c59ec6ffc1","name":"229a0e6e-5398-4192-9fde-11c59ec6ffc1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"832d8c3c-3557-4781-98b0-6a8791f8fd4f","name":"832d8c3c-3557-4781-98b0-6a8791f8fd4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2e7bd949-6fe2-4c69-a1db-c8c63fb713cd","name":"2e7bd949-6fe2-4c69-a1db-c8c63fb713cd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"71e8e5e7-4d5e-4167-8b61-1896dc1582a8","name":"71e8e5e7-4d5e-4167-8b61-1896dc1582a8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e9aa8d79-62f4-4966-9c1c-324cdb8807da","name":"e9aa8d79-62f4-4966-9c1c-324cdb8807da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bc5833ec-2237-42c0-a9da-afbae5ceae0b","name":"bc5833ec-2237-42c0-a9da-afbae5ceae0b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f921802b-5432-420b-9d31-8af5ae52593c","name":"f921802b-5432-420b-9d31-8af5ae52593c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"77681d2e-733f-48c0-9bee-d63ba64332e8","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"77681d2e-733f-48c0-9bee-d63ba64332e8","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"f8da4e70-5a07-45d6-86f0-47aee321d4bd","blendMode":0,"displayName":"default","isLocked":false,"name":"f8da4e70-5a07-45d6-86f0-47aee321d4bd","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprStatueBossPlateau",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Statues",
    "path":"folders/Sprites/Enemies/Boss/Statues.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprStatueBossPlateau",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprStatueBossPlateau",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"652feb38-4774-48d0-b2cd-9b8a121dc3fa","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c47040e9-3d71-4ec3-88b4-b6de05e83107","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"229a0e6e-5398-4192-9fde-11c59ec6ffc1","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c1ac56d5-574e-4349-921c-c3ba461e6a38","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"832d8c3c-3557-4781-98b0-6a8791f8fd4f","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b95a8f39-c6a0-4a1e-8ad7-f14c4cce0566","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2e7bd949-6fe2-4c69-a1db-c8c63fb713cd","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d93d8ccf-ecbd-4683-b039-1f03457df22f","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"71e8e5e7-4d5e-4167-8b61-1896dc1582a8","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8c54cd10-0cdd-4634-93d5-263ddcdc05bf","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e9aa8d79-62f4-4966-9c1c-324cdb8807da","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9c7ab164-488a-42a4-80a9-694eb2c81918","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bc5833ec-2237-42c0-a9da-afbae5ceae0b","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2aeda670-02bf-4911-b11d-d13709d2faef","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f921802b-5432-420b-9d31-8af5ae52593c","path":"sprites/sprStatueBossPlateau/sprStatueBossPlateau.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"80499ecd-afe6-4199-828c-4db14cb2db5c","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":32,
    "yorigin":32,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":64,
}