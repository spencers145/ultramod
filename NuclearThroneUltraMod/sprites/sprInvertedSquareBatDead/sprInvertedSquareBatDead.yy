{
  "$GMSprite":"",
  "%Name":"sprInvertedSquareBatDead",
  "bboxMode":1,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c09cc948-bf98-4b33-a7a4-104689104c43","name":"c09cc948-bf98-4b33-a7a4-104689104c43","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"05f03732-0f15-4b88-94f4-fa905310fb33","name":"05f03732-0f15-4b88-94f4-fa905310fb33","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"79cdb932-0132-4404-8eb7-d608e631ca66","name":"79cdb932-0132-4404-8eb7-d608e631ca66","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1252e6fd-6ae7-483b-a12e-a42b1d8ac73c","name":"1252e6fd-6ae7-483b-a12e-a42b1d8ac73c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"76fc40d5-3846-4086-936f-517a13c34db4","name":"76fc40d5-3846-4086-936f-517a13c34db4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0dd21fef-4fa3-4ee1-85f7-fad8530fd10f","name":"0dd21fef-4fa3-4ee1-85f7-fad8530fd10f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c0e00bb1-9abe-48cf-9a10-85b8fe7edcd1","name":"c0e00bb1-9abe-48cf-9a10-85b8fe7edcd1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"62cbd637-1665-4b87-9c89-9a3758beb5c0","name":"62cbd637-1665-4b87-9c89-9a3758beb5c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"57b36235-05db-4f0c-b1d5-c553c7b85655","name":"57b36235-05db-4f0c-b1d5-c553c7b85655","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a4c17321-face-4ad9-b54a-e9e3703640f9","name":"a4c17321-face-4ad9-b54a-e9e3703640f9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"d3779abf-eddf-4feb-8027-e3eb1bb78c76","blendMode":0,"displayName":"default","isLocked":false,"name":"d3779abf-eddf-4feb-8027-e3eb1bb78c76","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedSquareBatDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"SquareBat",
    "path":"folders/Sprites/Enemies/CrystalCaves/SquareBat.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedSquareBatDead",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":10.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedSquareBatDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c09cc948-bf98-4b33-a7a4-104689104c43","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"db44262e-0c0f-47ee-b457-4893f88befac","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"05f03732-0f15-4b88-94f4-fa905310fb33","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7f59486b-bbfa-4c6a-9cb8-9e38cbd0b14a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"79cdb932-0132-4404-8eb7-d608e631ca66","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f76746eb-071e-4c3a-9a2b-26fd296e8c1f","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1252e6fd-6ae7-483b-a12e-a42b1d8ac73c","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"93c3da79-6405-4443-859a-8f34e9a01bad","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"76fc40d5-3846-4086-936f-517a13c34db4","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b1c44cc0-0733-4f77-8be5-28b6b07c68cb","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0dd21fef-4fa3-4ee1-85f7-fad8530fd10f","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"45e9d19d-49ee-4325-8986-aed9b01eb921","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0e00bb1-9abe-48cf-9a10-85b8fe7edcd1","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ccc2b192-1e48-4db4-9778-ecd33c8c1b8b","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"62cbd637-1665-4b87-9c89-9a3758beb5c0","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"37b11366-76a7-4865-881c-2b9cf6d90739","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"57b36235-05db-4f0c-b1d5-c553c7b85655","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"62eb44fd-fca2-458f-b296-e7193797140b","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a4c17321-face-4ad9-b54a-e9e3703640f9","path":"sprites/sprInvertedSquareBatDead/sprInvertedSquareBatDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c14dc754-4e63-4dc2-a054-4ac0c5cb5f67","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Vulcano",
    "path":"texturegroups/Vulcano",
  },
  "type":0,
  "VTile":false,
  "width":32,
}