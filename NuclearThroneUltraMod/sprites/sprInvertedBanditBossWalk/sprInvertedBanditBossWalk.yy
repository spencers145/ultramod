{
  "$GMSprite":"",
  "%Name":"sprInvertedBanditBossWalk",
  "bboxMode":1,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c2c7dbe3-c600-4123-9bb5-f1e09e015f4f","name":"c2c7dbe3-c600-4123-9bb5-f1e09e015f4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b394db65-272c-4db8-9513-db0585e77f78","name":"b394db65-272c-4db8-9513-db0585e77f78","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8d44d437-a597-46ac-9f3a-264a938be2c2","name":"8d44d437-a597-46ac-9f3a-264a938be2c2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"699aa1ec-2fbb-4942-8a4d-ce9d1bac143a","name":"699aa1ec-2fbb-4942-8a4d-ce9d1bac143a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0d96d911-28b6-4be2-95a9-46b4f7d6d98e","name":"0d96d911-28b6-4be2-95a9-46b4f7d6d98e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3fc437ba-215d-4253-a273-7a51fd49310c","name":"3fc437ba-215d-4253-a273-7a51fd49310c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"13e79c89-673c-4906-ae66-e4e8b9d43dbd","name":"13e79c89-673c-4906-ae66-e4e8b9d43dbd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e22ae8b5-bb45-49ad-a4b6-a36c44e3400c","name":"e22ae8b5-bb45-49ad-a4b6-a36c44e3400c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4295f8eb-1175-45e8-8cd3-4c352b99312a","blendMode":0,"displayName":"default","isLocked":false,"name":"4295f8eb-1175-45e8-8cd3-4c352b99312a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedBanditBossWalk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigBandit",
    "path":"folders/Sprites/Enemies/Boss/BigBandit.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c2c7dbe3-c600-4123-9bb5-f1e09e015f4f","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"864dedb6-3911-4190-aeba-f659eae05933","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b394db65-272c-4db8-9513-db0585e77f78","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0d9e7879-57bb-4301-a9a4-a15d2e66d97d","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8d44d437-a597-46ac-9f3a-264a938be2c2","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"880ba2cd-5899-4ac4-8a9f-cd2221dd7951","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"699aa1ec-2fbb-4942-8a4d-ce9d1bac143a","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9e94028a-59f5-43fc-8a26-6501c88e8207","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0d96d911-28b6-4be2-95a9-46b4f7d6d98e","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ab0deb54-d4e7-47de-a35f-0b29723bc982","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3fc437ba-215d-4253-a273-7a51fd49310c","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"14767758-9d82-4fd2-b2f6-6326ded11e1b","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"13e79c89-673c-4906-ae66-e4e8b9d43dbd","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e1e7c21d-2a9e-4dd7-aef2-a5cd3766105d","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e22ae8b5-bb45-49ad-a4b6-a36c44e3400c","path":"sprites/sprInvertedBanditBossWalk/sprInvertedBanditBossWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3afe903f-a2a6-478b-911c-e37347023921","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedDesert",
    "path":"texturegroups/InvertedDesert",
  },
  "type":0,
  "VTile":false,
  "width":32,
}