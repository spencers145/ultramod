{
  "$GMSprite":"",
  "%Name":"sprCrown18Walk",
  "bboxMode":1,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":15,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"dd929a79-a5a0-48a3-9d2b-bd1a55195630","name":"dd929a79-a5a0-48a3-9d2b-bd1a55195630","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a842ffbb-0083-46ca-b149-61573b7a9680","name":"a842ffbb-0083-46ca-b149-61573b7a9680","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ad726b11-d5f1-4245-83a0-d70dec0ab424","name":"ad726b11-d5f1-4245-83a0-d70dec0ab424","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a8aadfed-4862-40af-bc89-cf1e8f420f72","name":"a8aadfed-4862-40af-bc89-cf1e8f420f72","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"999dbbe8-91c4-46d3-90e1-f0f704663e90","name":"999dbbe8-91c4-46d3-90e1-f0f704663e90","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b1866f13-fd40-4222-9861-c070690334e9","name":"b1866f13-fd40-4222-9861-c070690334e9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"d9aa6a09-0042-438d-82b2-ed4cd7024add","blendMode":0,"displayName":"default","isLocked":false,"name":"d9aa6a09-0042-438d-82b2-ed4cd7024add","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprCrown18Walk",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"CrownVault",
    "path":"folders/Sprites/CrownVault.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprCrown18Walk",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprCrown18Walk",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dd929a79-a5a0-48a3-9d2b-bd1a55195630","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"738943a9-e84c-4e05-8617-addc6fd1efd3","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a842ffbb-0083-46ca-b149-61573b7a9680","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"687978c7-9b81-4dd8-b8bc-5f2488b71d5a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ad726b11-d5f1-4245-83a0-d70dec0ab424","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea2e1900-8d1d-43df-a677-56b7880407d5","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a8aadfed-4862-40af-bc89-cf1e8f420f72","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed95f999-ec1f-45f9-a9cb-de711ae2518d","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"999dbbe8-91c4-46d3-90e1-f0f704663e90","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"91ec5aba-2be2-4897-b250-7fffa21909bf","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b1866f13-fd40-4222-9861-c070690334e9","path":"sprites/sprCrown18Walk/sprCrown18Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fa46454f-e558-4026-a4a8-6675200955b8","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":9,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"CrownVault",
    "path":"texturegroups/CrownVault",
  },
  "type":0,
  "VTile":false,
  "width":16,
}