{
  "$GMSprite":"",
  "%Name":"sprInvertedBigVultureChargeStop",
  "bboxMode":0,
  "bbox_bottom":29,
  "bbox_left":0,
  "bbox_right":45,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"9d0cb4be-daf3-40bd-be40-ab1f60d66faf","name":"9d0cb4be-daf3-40bd-be40-ab1f60d66faf","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"505648ce-c50b-4f85-b127-64f25b3c2a05","name":"505648ce-c50b-4f85-b127-64f25b3c2a05","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f005ebcd-4d49-40f2-a009-9d26330f5579","name":"f005ebcd-4d49-40f2-a009-9d26330f5579","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7aab16c2-434e-48f2-9e5a-c9bc1000e13b","name":"7aab16c2-434e-48f2-9e5a-c9bc1000e13b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6723486b-ae73-4ace-89ef-19b53cbeb23c","name":"6723486b-ae73-4ace-89ef-19b53cbeb23c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0ff033c7-4c9e-4928-bc6d-bea68866b1d2","name":"0ff033c7-4c9e-4928-bc6d-bea68866b1d2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"eded0091-8869-40df-b8b3-521251ce5568","name":"eded0091-8869-40df-b8b3-521251ce5568","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ccaca764-3e3c-4d46-84ca-33625209dbc1","name":"ccaca764-3e3c-4d46-84ca-33625209dbc1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5dc8fbde-5d80-4302-a1e8-eeb44c92777d","name":"5dc8fbde-5d80-4302-a1e8-eeb44c92777d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"adfa40f3-7887-4ee3-9dc2-766bf1cc6db4","name":"adfa40f3-7887-4ee3-9dc2-766bf1cc6db4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"a409d97e-bae6-427d-ae12-3bd004c25d8c","blendMode":0,"displayName":"default","isLocked":false,"name":"a409d97e-bae6-427d-ae12-3bd004c25d8c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedBigVultureChargeStop",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigVulture",
    "path":"folders/Sprites/Enemies/Boss/BigVulture.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedBigVultureChargeStop",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":10.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedBigVultureChargeStop",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9d0cb4be-daf3-40bd-be40-ab1f60d66faf","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0aac41ec-a697-4151-b1b3-7f389a2448d0","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"505648ce-c50b-4f85-b127-64f25b3c2a05","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"46235090-7775-43a4-a5c9-c1653cdcf8dd","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f005ebcd-4d49-40f2-a009-9d26330f5579","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6f8a0e2d-391b-44b1-9bc7-df92f976d1d9","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7aab16c2-434e-48f2-9e5a-c9bc1000e13b","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3c70f7a8-6f80-451f-85c9-8144dbd8b435","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6723486b-ae73-4ace-89ef-19b53cbeb23c","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"df195e21-9b57-4fe0-963b-fde42743026d","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0ff033c7-4c9e-4928-bc6d-bea68866b1d2","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c711d359-f25f-45f8-b6d6-6f29d7249e0d","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"eded0091-8869-40df-b8b3-521251ce5568","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1a7fb4ea-d605-47e4-bd44-27575414b2ad","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ccaca764-3e3c-4d46-84ca-33625209dbc1","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d954dd74-3bc4-48fe-aea9-3d958a2c390e","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5dc8fbde-5d80-4302-a1e8-eeb44c92777d","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"89ee1eb1-5bc5-4b49-996b-335ba0b1cdc5","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"adfa40f3-7887-4ee3-9dc2-766bf1cc6db4","path":"sprites/sprInvertedBigVultureChargeStop/sprInvertedBigVultureChargeStop.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f3e691bf-b9db-4cc9-a598-19cac6e17047","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":48,
}