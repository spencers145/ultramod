{
  "$GMSprite":"",
  "%Name":"sprSuperFlakBulletDeflected",
  "bboxMode":1,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"7c246fdd-5a36-43a4-9faa-2ea6f6c5a5d3","name":"7c246fdd-5a36-43a4-9faa-2ea6f6c5a5d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"41bbd20d-dda0-4279-b00d-73ced86f5b6c","name":"41bbd20d-dda0-4279-b00d-73ced86f5b6c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"6a7f90f6-3c4d-4e3c-94d1-efacb09ac8f0","blendMode":0,"displayName":"default","isLocked":false,"name":"6a7f90f6-3c4d-4e3c-94d1-efacb09ac8f0","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSuperFlakBulletDeflected",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Projectiles",
    "path":"folders/Sprites/Projectiles.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprSuperFlakBulletDeflected",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":2.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprSuperFlakBulletDeflected",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7c246fdd-5a36-43a4-9faa-2ea6f6c5a5d3","path":"sprites/sprSuperFlakBulletDeflected/sprSuperFlakBulletDeflected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12f4ad7f-d713-44ad-8a07-5397553402d6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"41bbd20d-dda0-4279-b00d-73ced86f5b6c","path":"sprites/sprSuperFlakBulletDeflected/sprSuperFlakBulletDeflected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e2f70114-5645-4789-a7f0-a1bdfb3d66af","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":32,
}