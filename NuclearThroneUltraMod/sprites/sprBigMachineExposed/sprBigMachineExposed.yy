{
  "$GMSprite":"",
  "%Name":"sprBigMachineExposed",
  "bboxMode":0,
  "bbox_bottom":123,
  "bbox_left":0,
  "bbox_right":157,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"ec276878-6f0d-41c5-91e6-6c1e9fb3b3a5","name":"ec276878-6f0d-41c5-91e6-6c1e9fb3b3a5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7013047d-317b-4929-ba8c-22e37ab5e4d9","name":"7013047d-317b-4929-ba8c-22e37ab5e4d9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e78ac56e-f874-4687-9b78-711e36c3ee29","name":"e78ac56e-f874-4687-9b78-711e36c3ee29","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"22d1fe92-83d8-4800-a705-cb85a9f92e31","name":"22d1fe92-83d8-4800-a705-cb85a9f92e31","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":124,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"00d98cfa-9a7c-44a3-a842-46ba882642c0","blendMode":0,"displayName":"default","isLocked":false,"name":"00d98cfa-9a7c-44a3-a842-46ba882642c0","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprBigMachineExposed",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigMachine",
    "path":"folders/Sprites/Enemies/Boss/BigMachine.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ec276878-6f0d-41c5-91e6-6c1e9fb3b3a5","path":"sprites/sprBigMachineExposed/sprBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1d26011d-0cc0-482f-b9c4-92da66ecaae1","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7013047d-317b-4929-ba8c-22e37ab5e4d9","path":"sprites/sprBigMachineExposed/sprBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8a3c2447-eef8-4454-be58-974a9fce76ae","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e78ac56e-f874-4687-9b78-711e36c3ee29","path":"sprites/sprBigMachineExposed/sprBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e7b46d42-2f61-4661-8bad-913dfc69f24d","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"22d1fe92-83d8-4800-a705-cb85a9f92e31","path":"sprites/sprBigMachineExposed/sprBigMachineExposed.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e898ce84-4d34-4d21-96cb-a47997b70b6e","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":79,
    "yorigin":62,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Labs",
    "path":"texturegroups/Labs",
  },
  "type":0,
  "VTile":false,
  "width":158,
}