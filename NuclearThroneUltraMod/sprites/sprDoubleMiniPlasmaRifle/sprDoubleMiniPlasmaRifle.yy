{
  "$GMSprite":"",
  "%Name":"sprDoubleMiniPlasmaRifle",
  "bboxMode":1,
  "bbox_bottom":19,
  "bbox_left":0,
  "bbox_right":16,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"1e523ed2-d209-4575-9b4c-5a3596b6597f","name":"1e523ed2-d209-4575-9b4c-5a3596b6597f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"26500656-6420-429c-94cc-80f7aaa2a3c8","name":"26500656-6420-429c-94cc-80f7aaa2a3c8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7894dbc7-d1d7-448e-a91f-c16b38d8b268","name":"7894dbc7-d1d7-448e-a91f-c16b38d8b268","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ea458ee-df3e-463c-861c-dafb0fc3e3c7","name":"4ea458ee-df3e-463c-861c-dafb0fc3e3c7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"73bb31a4-24dd-4fcd-a244-696b048400b4","name":"73bb31a4-24dd-4fcd-a244-696b048400b4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"de12e600-e2aa-4223-9a48-cfe04f46bd7d","name":"de12e600-e2aa-4223-9a48-cfe04f46bd7d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f61bdaf7-8892-4b88-a5ab-74029ebcd063","name":"f61bdaf7-8892-4b88-a5ab-74029ebcd063","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":20,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"3416c834-87ee-4cdc-b42a-163a935b0a3f","blendMode":0,"displayName":"default","isLocked":false,"name":"3416c834-87ee-4cdc-b42a-163a935b0a3f","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprDoubleMiniPlasmaRifle",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Custom",
    "path":"folders/Sprites/Weapons/Custom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprDoubleMiniPlasmaRifle",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprDoubleMiniPlasmaRifle",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1e523ed2-d209-4575-9b4c-5a3596b6597f","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6584c35b-17fb-498b-b5b1-8051230965de","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"26500656-6420-429c-94cc-80f7aaa2a3c8","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"977522e1-2de9-4e88-8d83-4b20f665c977","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7894dbc7-d1d7-448e-a91f-c16b38d8b268","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fdb5bfa9-2fbc-4b71-97b4-094421fdd710","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ea458ee-df3e-463c-861c-dafb0fc3e3c7","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dcd2ab08-eece-4fbd-86ad-35cbbb2119e6","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"73bb31a4-24dd-4fcd-a244-696b048400b4","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5ae23d08-1366-44c4-b00b-d74c69f18026","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"de12e600-e2aa-4223-9a48-cfe04f46bd7d","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f115f920-cfb3-498f-a159-27b5bc435efd","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f61bdaf7-8892-4b88-a5ab-74029ebcd063","path":"sprites/sprDoubleMiniPlasmaRifle/sprDoubleMiniPlasmaRifle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"25bd6b63-cb42-4f54-99cf-072c4e0ba8e6","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":4,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":17,
}