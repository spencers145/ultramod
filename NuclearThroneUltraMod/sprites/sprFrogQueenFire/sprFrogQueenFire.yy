{
  "$GMSprite":"",
  "%Name":"sprFrogQueenFire",
  "bboxMode":0,
  "bbox_bottom":41,
  "bbox_left":1,
  "bbox_right":45,
  "bbox_top":3,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"91f00aab-44dc-44c1-bd56-b8960a532522","name":"91f00aab-44dc-44c1-bd56-b8960a532522","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"969de4c9-f970-4088-9d18-cb1f638a6dc8","name":"969de4c9-f970-4088-9d18-cb1f638a6dc8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"371b2f60-fc95-4f4f-a3bd-8511f7e1d0bb","name":"371b2f60-fc95-4f4f-a3bd-8511f7e1d0bb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"64b27ba3-2fee-4914-9e0b-5374208f0135","name":"64b27ba3-2fee-4914-9e0b-5374208f0135","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"697aedbb-d47e-4b19-b3f0-002cfaeff180","name":"697aedbb-d47e-4b19-b3f0-002cfaeff180","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a9f22217-ae0b-4684-bebf-869238389345","name":"a9f22217-ae0b-4684-bebf-869238389345","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9b8c7918-fa44-4ade-bbac-ead5f3c75737","name":"9b8c7918-fa44-4ade-bbac-ead5f3c75737","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"401b604a-22e9-4010-b49d-2835fdbdcda0","name":"401b604a-22e9-4010-b49d-2835fdbdcda0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"89aa7bce-dca2-4017-a2a3-7af6846ca20c","name":"89aa7bce-dca2-4017-a2a3-7af6846ca20c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"96ffac2d-65de-4b9a-8790-f7af107eafc4","blendMode":0,"displayName":"default","isLocked":false,"name":"96ffac2d-65de-4b9a-8790-f7af107eafc4","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprFrogQueenFire",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BallMom",
    "path":"folders/Sprites/Enemies/Boss/BallMom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprFrogQueenFire",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprFrogQueenFire",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91f00aab-44dc-44c1-bd56-b8960a532522","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2640de30-35a9-45b0-878f-027b9c416aae","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"969de4c9-f970-4088-9d18-cb1f638a6dc8","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12086af1-cba2-48e4-a96f-c11761b88f91","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"371b2f60-fc95-4f4f-a3bd-8511f7e1d0bb","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"698b0023-c48a-44f7-8fac-226d74432727","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"64b27ba3-2fee-4914-9e0b-5374208f0135","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a131e71f-0d37-4a25-ad96-1a10e52f1827","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"697aedbb-d47e-4b19-b3f0-002cfaeff180","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"62e7f457-d355-4e8c-9fec-ea7b415bce12","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a9f22217-ae0b-4684-bebf-869238389345","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c216ce05-056a-439f-8bc0-03650360b2f4","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9b8c7918-fa44-4ade-bbac-ead5f3c75737","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8413778a-0fb0-426a-8247-5b9bf308a0b9","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"401b604a-22e9-4010-b49d-2835fdbdcda0","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"737b42f0-7d5b-422f-a091-cc95c974cf33","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"89aa7bce-dca2-4017-a2a3-7af6846ca20c","path":"sprites/sprFrogQueenFire/sprFrogQueenFire.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"24d6874e-baaa-4b1c-b5d5-86c1afef34c9","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Sewers",
    "path":"texturegroups/Sewers",
  },
  "type":0,
  "VTile":false,
  "width":48,
}