{
  "$GMSprite":"",
  "%Name":"sprMutant7CIdle2820",
  "bboxMode":1,
  "bbox_bottom":18,
  "bbox_left":0,
  "bbox_right":17,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"cc4c3c94-ddc5-4487-b886-3fe356bdbf3b","name":"cc4c3c94-ddc5-4487-b886-3fe356bdbf3b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9f2d6950-c6eb-485c-b975-e634b3f00d3a","name":"9f2d6950-c6eb-485c-b975-e634b3f00d3a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c0bb3d3d-df72-4c52-9fc1-26e18a084d09","name":"c0bb3d3d-df72-4c52-9fc1-26e18a084d09","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"96389d5b-9198-4cda-8ebc-b53ddfc4a36a","name":"96389d5b-9198-4cda-8ebc-b53ddfc4a36a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1496c0cf-ee4a-491a-ac50-991c67962fed","name":"1496c0cf-ee4a-491a-ac50-991c67962fed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"da0bd74e-72d7-4447-a94d-f773bbd9abfc","name":"da0bd74e-72d7-4447-a94d-f773bbd9abfc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7db82b57-7d88-402d-9a10-0b29e9b76786","name":"7db82b57-7d88-402d-9a10-0b29e9b76786","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ed6d9cd8-0fe0-42d6-a7f4-1edcb50b1c25","name":"ed6d9cd8-0fe0-42d6-a7f4-1edcb50b1c25","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"75c36c00-6fb7-4eae-861d-2e33941998ed","name":"75c36c00-6fb7-4eae-861d-2e33941998ed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1c957c7b-6819-473a-9ab4-76872b29bbeb","name":"1c957c7b-6819-473a-9ab4-76872b29bbeb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"86a60596-fd4f-4f57-807e-d155bfd3b317","name":"86a60596-fd4f-4f57-807e-d155bfd3b317","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":19,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"295bddb5-aa02-414d-99d1-ae759698ee50","blendMode":0,"displayName":"default","isLocked":false,"name":"295bddb5-aa02-414d-99d1-ae759698ee50","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant7CIdle2820",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"SteroidsD",
    "path":"folders/Sprites/Player/Steroids/SteroidsD.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc4c3c94-ddc5-4487-b886-3fe356bdbf3b","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dfe758ec-2aac-4eaa-855f-7a47fa1ae973","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9f2d6950-c6eb-485c-b975-e634b3f00d3a","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea4c869a-068b-4849-8b2f-4d819b814480","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0bb3d3d-df72-4c52-9fc1-26e18a084d09","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ff0bd992-3ced-478c-abd4-9b260340687c","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"96389d5b-9198-4cda-8ebc-b53ddfc4a36a","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f199885e-2857-456e-8e48-afd4cdadddfa","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1496c0cf-ee4a-491a-ac50-991c67962fed","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dc617af0-a644-4c9c-a7d4-9ca655b8db50","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"da0bd74e-72d7-4447-a94d-f773bbd9abfc","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"484ffd16-3301-4a76-9749-2224aefa318f","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7db82b57-7d88-402d-9a10-0b29e9b76786","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e81c1f1d-78e8-495d-86b6-1419588db9f8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ed6d9cd8-0fe0-42d6-a7f4-1edcb50b1c25","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d264b213-0918-4a8a-958b-03f1e0712548","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"75c36c00-6fb7-4eae-861d-2e33941998ed","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"54740415-794d-4152-bdf1-c969b9803262","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1c957c7b-6819-473a-9ab4-76872b29bbeb","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12cf50a3-5500-4502-9085-5565153b1488","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"86a60596-fd4f-4f57-807e-d155bfd3b317","path":"sprites/sprMutant7CIdle2820/sprMutant7CIdle2820.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8c7fd29c-31d1-48f4-aad3-db451905fe40","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":9,
    "yorigin":9,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":18,
}