{
  "$GMSprite":"",
  "%Name":"sprCubeGuardianFireHurt",
  "bboxMode":0,
  "bbox_bottom":89,
  "bbox_left":8,
  "bbox_right":68,
  "bbox_top":20,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"056da59c-7d56-4072-b2f2-96ab632a0188","name":"056da59c-7d56-4072-b2f2-96ab632a0188","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"842eac22-b701-496b-8c59-93c5af3174d4","name":"842eac22-b701-496b-8c59-93c5af3174d4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5906e08-4528-4e56-aa43-297681a57764","name":"c5906e08-4528-4e56-aa43-297681a57764","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7cb37eda-60af-48b7-9a41-c837badd99c0","name":"7cb37eda-60af-48b7-9a41-c837badd99c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"de5796cd-6810-40d7-8adb-44b2d0632039","name":"de5796cd-6810-40d7-8adb-44b2d0632039","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cf58d171-6037-4299-91f2-bef5efb53770","name":"cf58d171-6037-4299-91f2-bef5efb53770","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f255bc14-fc31-42dc-b4dd-161971481dc1","name":"f255bc14-fc31-42dc-b4dd-161971481dc1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"91d0c78b-c6df-4467-8194-fb08ec229338","name":"91d0c78b-c6df-4467-8194-fb08ec229338","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a0f553ee-d159-48e0-9549-756d74de7eb6","name":"a0f553ee-d159-48e0-9549-756d74de7eb6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"543a64cb-551c-4806-8e1d-12f21e58e283","name":"543a64cb-551c-4806-8e1d-12f21e58e283","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5faf92c3-7b2f-4f74-909d-09f971b57259","name":"5faf92c3-7b2f-4f74-909d-09f971b57259","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d661dc90-f157-462d-be8e-eac8d87dff60","name":"d661dc90-f157-462d-be8e-eac8d87dff60","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":100,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5269b1c6-2687-4cc8-aa83-d0485794dc3c","blendMode":0,"displayName":"default","isLocked":false,"name":"5269b1c6-2687-4cc8-aa83-d0485794dc3c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprCubeGuardianFireHurt",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Palace Enemy",
    "path":"folders/Sprites/Palace/Palace Enemy.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprCubeGuardianFireHurt",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":28.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprCubeGuardianFireHurt",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1be59fda-33df-4d56-a92b-e3e7eb899ecb","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ab314949-d085-4b9b-990e-5698cef66b89","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a2f79f91-18f3-461a-9912-7c4aba8ea08a","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a617ba57-ea0a-488c-90b7-1d12d9f8f407","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"056da59c-7d56-4072-b2f2-96ab632a0188","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"985e03f0-2777-489d-a0d1-f70df81a9866","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"842eac22-b701-496b-8c59-93c5af3174d4","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"55062511-3484-48f8-b0a6-a1b53d590701","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5906e08-4528-4e56-aa43-297681a57764","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a2b2a11a-ac5b-4031-ba96-1ff5d0788f3f","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0f18769-508b-4bc3-b298-24a8ccfe7c0b","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"224e724a-d5a3-45f8-a9c5-501194dfeed3","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7cb37eda-60af-48b7-9a41-c837badd99c0","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3aeb39a8-45de-4fb4-adc6-e93d7d8a7ceb","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"58d49824-d17f-4d65-87d8-9bcb48dbc919","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5d613a43-9ead-4c27-ab5f-b6c1fdcbafbd","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"de5796cd-6810-40d7-8adb-44b2d0632039","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3887abcf-5b06-42e8-b2dc-8fda8985e570","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cf58d171-6037-4299-91f2-bef5efb53770","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ef40cc7a-9b11-4250-b2dc-f9fecb980fdb","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f255bc14-fc31-42dc-b4dd-161971481dc1","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b0faca11-7260-4f7f-96cc-4c78be9f69e3","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b44b5e00-fb5a-4000-80c7-f4ffbd7babe5","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4d76a0e8-e865-4058-ba0f-b610f673f0e1","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b3afdd70-7bba-4692-afcb-f31a42b3ba36","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fb1997b6-a0a3-438e-939d-1bf9bcdda405","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15b42623-3f99-4376-bbf8-fb4a4de16cc4","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dd7f656e-2116-4fe8-9f8b-e93a4c133fb7","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91d0c78b-c6df-4467-8194-fb08ec229338","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bbe8e179-2012-4cbc-98e1-708be38f9f88","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"451655bb-be9c-4a0e-9c07-a0111d42a2d3","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"389b2561-9d86-4f82-9a39-60d34ca8cc40","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a0f553ee-d159-48e0-9549-756d74de7eb6","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"68ca61d4-9bca-4c1c-8912-1f57c5262a9c","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3eee646b-bdc2-442f-92c9-3c050f6390c5","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"593a7547-cdc9-4118-a841-f10f35d04717","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15969f9e-1b3e-43b7-8cd5-c65788a31aed","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"35691f1b-eac6-40c4-a109-aa514fa40cbe","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"543a64cb-551c-4806-8e1d-12f21e58e283","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"52eeb0e4-e5ab-4658-87a9-15d61cc30fdc","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d8696a39-b8fc-445e-a8bb-b73c1fcba74a","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7b3074f6-e730-4b70-8cbb-3c21d4ec47a6","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d5b0e3d9-508a-4ca7-8900-0b5dfced693f","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6e50273c-0bca-40d4-a1ed-755db12476c5","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4bcaa918-bd19-4b2c-a3ac-000f33e5a11d","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b073b19c-5a04-4315-aa29-e144a1f365d7","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"80b7df85-d262-4857-a9f5-db8a6ad1b8ae","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"642b29a6-3f04-4d3b-9bec-5c7c52da73a5","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5faf92c3-7b2f-4f74-909d-09f971b57259","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2383cb17-9c3f-4796-b9d1-38cb5ce45af9","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e67b29ef-d9be-4e14-a297-6f21f4d34da2","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dc8bfc23-c560-4796-9143-d43486020d2a","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d661dc90-f157-462d-be8e-eac8d87dff60","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3b34627c-1ec8-42e1-974b-b9447f8474ed","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e1e267b0-b08f-4f73-a5bf-3fcb09f32f85","path":"sprites/sprCubeGuardianFireHurt/sprCubeGuardianFireHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4c33cb3e-59f1-4525-8569-ec3a2fa75e9e","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":41,
    "yorigin":70,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":80,
}