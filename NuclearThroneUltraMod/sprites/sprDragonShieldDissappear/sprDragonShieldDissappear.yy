{
  "$GMSprite":"",
  "%Name":"sprDragonShieldDissappear",
  "bboxMode":2,
  "bbox_bottom":33,
  "bbox_left":-2,
  "bbox_right":27,
  "bbox_top":-3,
  "collisionKind":3,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7853021a-7089-4a53-b319-6d79d4388548","name":"7853021a-7089-4a53-b319-6d79d4388548","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":31,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"64d0a591-b748-4b28-892d-db16e7c71470","blendMode":0,"displayName":"default","isLocked":false,"name":"64d0a591-b748-4b28-892d-db16e7c71470","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprDragonShieldDissappear",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"HotDrake",
    "path":"folders/Sprites/Enemies/Boss/HotDrake.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprDragonShieldDissappear",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprDragonShieldDissappear",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7a1cd2e5-6d96-4e92-b2a7-9e4f90f28ca3","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"454d640f-9761-432d-b2c9-717f188e0556","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"46b4a376-039c-49d7-99f9-c1cca4b6d906","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"811e937b-5fb0-4c3f-98fe-fc3fcb431d4a","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f0223cfc-087f-4308-bd4a-8444bf4edf9c","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7853021a-7089-4a53-b319-6d79d4388548","path":"sprites/sprDragonShieldDissappear/sprDragonShieldDissappear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a74794ad-9bcd-4991-865e-4671f9341c46","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":15,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":26,
}