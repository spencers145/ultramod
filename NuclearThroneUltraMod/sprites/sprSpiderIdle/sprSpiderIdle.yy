{
  "$GMSprite":"",
  "%Name":"sprSpiderIdle",
  "bboxMode":0,
  "bbox_bottom":21,
  "bbox_left":2,
  "bbox_right":23,
  "bbox_top":3,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"df8dd501-d04d-4771-8a10-b736c3dfe06a","name":"df8dd501-d04d-4771-8a10-b736c3dfe06a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f7e7a912-e06c-4d20-bb5f-b1e2f1b584ed","name":"f7e7a912-e06c-4d20-bb5f-b1e2f1b584ed","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5a6e2807-dc8d-4aa8-a561-c6649405438d","name":"5a6e2807-dc8d-4aa8-a561-c6649405438d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b4dfbf64-58c7-41e7-b7dc-d67474decf1e","name":"b4dfbf64-58c7-41e7-b7dc-d67474decf1e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"08805f14-67ff-4059-a2f1-a650a63acb86","name":"08805f14-67ff-4059-a2f1-a650a63acb86","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f6db3765-7887-4008-8027-b8f970e25bcd","name":"f6db3765-7887-4008-8027-b8f970e25bcd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"750bea54-9f37-47d0-975c-a23f51bcbd75","name":"750bea54-9f37-47d0-975c-a23f51bcbd75","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6e7c94d9-7701-498f-8c90-43f52d80c564","name":"6e7c94d9-7701-498f-8c90-43f52d80c564","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"c4a121fb-67f2-458e-a0ac-1fd6535a5a21","blendMode":0,"displayName":"default","isLocked":false,"name":"c4a121fb-67f2-458e-a0ac-1fd6535a5a21","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSpiderIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"CrystalCaves",
    "path":"folders/Sprites/Enemies/CrystalCaves.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"df8dd501-d04d-4771-8a10-b736c3dfe06a","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1e5a6851-0a48-4b35-8d15-7963497efdda","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f7e7a912-e06c-4d20-bb5f-b1e2f1b584ed","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7bfe0298-23cb-4a1d-933e-5905e8f1b212","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5a6e2807-dc8d-4aa8-a561-c6649405438d","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"34fa1665-6f06-4b44-8921-b9e24002debc","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b4dfbf64-58c7-41e7-b7dc-d67474decf1e","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d1aa53a4-8a89-4556-9eda-771cadaac4d2","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"08805f14-67ff-4059-a2f1-a650a63acb86","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8784069c-a57c-4e2b-b46e-b17375703451","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f6db3765-7887-4008-8027-b8f970e25bcd","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5fe0fb91-e31a-445d-852b-d0372eda8eec","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"750bea54-9f37-47d0-975c-a23f51bcbd75","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"48a23550-1953-4840-8086-f90a37f19ff4","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6e7c94d9-7701-498f-8c90-43f52d80c564","path":"sprites/sprSpiderIdle/sprSpiderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b7b29ace-74b5-469b-9ac4-35852a2d561a","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"CrystalCaves",
    "path":"texturegroups/CrystalCaves",
  },
  "type":0,
  "VTile":false,
  "width":24,
}