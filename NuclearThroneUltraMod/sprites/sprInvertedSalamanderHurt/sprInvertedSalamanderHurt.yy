{
  "$GMSprite":"",
  "%Name":"sprInvertedSalamanderHurt",
  "bboxMode":0,
  "bbox_bottom":32,
  "bbox_left":6,
  "bbox_right":41,
  "bbox_top":10,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"4dbced14-11e6-4383-bf61-136229bda390","name":"4dbced14-11e6-4383-bf61-136229bda390","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"34afedd3-b482-49b3-90e2-d739c654e3dc","name":"34afedd3-b482-49b3-90e2-d739c654e3dc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1c20dd13-522b-4629-a11b-b2a181d22a91","name":"1c20dd13-522b-4629-a11b-b2a181d22a91","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"86ffa880-5fd6-48c5-bec7-f5ba2787f5a3","blendMode":0,"displayName":"default","isLocked":false,"name":"86ffa880-5fd6-48c5-bec7-f5ba2787f5a3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedSalamanderHurt",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Salamander",
    "path":"folders/Sprites/Enemies/Salamander.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4dbced14-11e6-4383-bf61-136229bda390","path":"sprites/sprInvertedSalamanderHurt/sprInvertedSalamanderHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9fee2f95-ca50-4d1d-89cc-a16cdfa22b7c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"34afedd3-b482-49b3-90e2-d739c654e3dc","path":"sprites/sprInvertedSalamanderHurt/sprInvertedSalamanderHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"537e790f-0039-4851-909c-7c92dc424bc7","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1c20dd13-522b-4629-a11b-b2a181d22a91","path":"sprites/sprInvertedSalamanderHurt/sprInvertedSalamanderHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"683f199a-a19d-4ff9-a44d-acedd02aa7e4","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedScrapyard",
    "path":"texturegroups/InvertedScrapyard",
  },
  "type":0,
  "VTile":false,
  "width":48,
}