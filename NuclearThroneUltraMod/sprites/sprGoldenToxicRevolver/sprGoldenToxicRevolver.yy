{
  "$GMSprite":"",
  "%Name":"sprGoldenToxicRevolver",
  "bboxMode":1,
  "bbox_bottom":11,
  "bbox_left":0,
  "bbox_right":13,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"31ed40a8-d4f0-4079-86cd-dbbca373b3a7","name":"31ed40a8-d4f0-4079-86cd-dbbca373b3a7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"efe36d25-898a-49ef-973a-d69881ae0009","name":"efe36d25-898a-49ef-973a-d69881ae0009","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"52fb4340-8868-4d69-a904-b928da7c4986","name":"52fb4340-8868-4d69-a904-b928da7c4986","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0ffa01df-44ed-4290-9b4f-dd7b234f2e87","name":"0ffa01df-44ed-4290-9b4f-dd7b234f2e87","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"158fb391-e321-453a-94cd-53e9902cb81a","name":"158fb391-e321-453a-94cd-53e9902cb81a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c795d1eb-f422-4aac-95d8-a0b7c78eb36a","name":"c795d1eb-f422-4aac-95d8-a0b7c78eb36a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b207c9dd-d0f5-4c18-8b76-c938613270fc","name":"b207c9dd-d0f5-4c18-8b76-c938613270fc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":12,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"eb3702ef-5df0-4700-8136-43af1f20a3c3","blendMode":0,"displayName":"default","isLocked":false,"name":"eb3702ef-5df0-4700-8136-43af1f20a3c3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprGoldenToxicRevolver",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Gold",
    "path":"folders/Sprites/Weapons/Gold.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprGoldenToxicRevolver",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprGoldenToxicRevolver",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"31ed40a8-d4f0-4079-86cd-dbbca373b3a7","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"235f909a-c7cf-4237-a786-8a10a7c8dda9","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"efe36d25-898a-49ef-973a-d69881ae0009","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b6035baa-2ea8-4652-9852-9ea7b4203f7f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"52fb4340-8868-4d69-a904-b928da7c4986","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bf7cf3fc-caee-4e84-bde1-9fdbddefaa06","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0ffa01df-44ed-4290-9b4f-dd7b234f2e87","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"db137467-484d-47c4-940c-465442ba4494","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"158fb391-e321-453a-94cd-53e9902cb81a","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"eea9ab4c-6537-49fa-b939-957ceccd25ce","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c795d1eb-f422-4aac-95d8-a0b7c78eb36a","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"818e7c0e-834d-4c8c-bc14-4100af8e4437","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b207c9dd-d0f5-4c18-8b76-c938613270fc","path":"sprites/sprGoldenToxicRevolver/sprGoldenToxicRevolver.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c24fbea5-283f-4030-8ee0-b28058c46f2c","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":-2,
    "yorigin":4,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":14,
}