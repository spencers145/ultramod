{
  "$GMSprite":"",
  "%Name":"sprMutant7BIdle",
  "bboxMode":0,
  "bbox_bottom":18,
  "bbox_left":0,
  "bbox_right":17,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"9131f440-c5e3-4e07-801c-4fc38f9d19fb","name":"9131f440-c5e3-4e07-801c-4fc38f9d19fb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"19641ea8-4409-495f-9783-03dfa241e0af","name":"19641ea8-4409-495f-9783-03dfa241e0af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"096d2a96-5646-4a33-99fd-b6df65411a26","name":"096d2a96-5646-4a33-99fd-b6df65411a26","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a5b64020-b5a2-4c52-a93a-f1e315a12ac1","name":"a5b64020-b5a2-4c52-a93a-f1e315a12ac1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d0e5a871-753c-4f06-9937-4d181482971a","name":"d0e5a871-753c-4f06-9937-4d181482971a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8b13f79f-ad20-430f-8cca-b75a626c405c","name":"8b13f79f-ad20-430f-8cca-b75a626c405c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9a2ed4b2-2815-4be7-9c07-1f64643ee853","name":"9a2ed4b2-2815-4be7-9c07-1f64643ee853","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"58ab970d-9f8b-4d12-9708-125ebd7ea473","name":"58ab970d-9f8b-4d12-9708-125ebd7ea473","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f6cc4e02-cbac-4453-b00a-a414d692b4db","name":"f6cc4e02-cbac-4453-b00a-a414d692b4db","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fc2b372f-d4df-4861-b4fc-94c9a1446391","name":"fc2b372f-d4df-4861-b4fc-94c9a1446391","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bf0df962-3949-45e9-8335-d42bf0a7a3bc","name":"bf0df962-3949-45e9-8335-d42bf0a7a3bc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":19,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"29c8d1fc-e80e-4071-b6cf-8a7fdb3add4c","blendMode":0,"displayName":"default","isLocked":false,"name":"29c8d1fc-e80e-4071-b6cf-8a7fdb3add4c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant7BIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"SteroidsB",
    "path":"folders/Sprites/Player/Steroids/SteroidsB.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9131f440-c5e3-4e07-801c-4fc38f9d19fb","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4915ea34-13cd-4200-bf32-b63ab3e81e4c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"19641ea8-4409-495f-9783-03dfa241e0af","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4054bcaa-33f7-4a65-87b9-a08a1e3ef531","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"096d2a96-5646-4a33-99fd-b6df65411a26","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"eefed73b-85e7-4fc9-b43b-063f7d3231f2","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a5b64020-b5a2-4c52-a93a-f1e315a12ac1","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7e1c2502-f9be-4df2-9d62-bc48183e0d15","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d0e5a871-753c-4f06-9937-4d181482971a","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0481b141-77f3-4523-ae29-4746eb2d130b","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8b13f79f-ad20-430f-8cca-b75a626c405c","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8ed32a32-2899-4e11-9414-4102fc7ef8f5","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9a2ed4b2-2815-4be7-9c07-1f64643ee853","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f3064347-11f8-49ff-a34a-0e402d883b71","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"58ab970d-9f8b-4d12-9708-125ebd7ea473","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7d4657ca-d5f5-4cb8-8c8b-e008dbe197b9","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f6cc4e02-cbac-4453-b00a-a414d692b4db","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f1188015-af4f-4a7b-87cd-aa7ea3df55c5","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fc2b372f-d4df-4861-b4fc-94c9a1446391","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d4a95f55-73de-4b21-9732-7eb7b7cbd91c","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bf0df962-3949-45e9-8335-d42bf0a7a3bc","path":"sprites/sprMutant7BIdle/sprMutant7BIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2d135335-3397-4742-9415-42da5e04c35c","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":9,
    "yorigin":9,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":18,
}