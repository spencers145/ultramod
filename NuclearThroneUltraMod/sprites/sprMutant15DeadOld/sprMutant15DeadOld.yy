{
  "$GMSprite":"",
  "%Name":"sprMutant15DeadOld",
  "bboxMode":1,
  "bbox_bottom":48,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"4d2a4a29-ee1d-4b72-907e-2e8e50a0cbc3","name":"4d2a4a29-ee1d-4b72-907e-2e8e50a0cbc3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1b6975e4-a3a8-43da-8fb7-e20692531c54","name":"1b6975e4-a3a8-43da-8fb7-e20692531c54","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6caa0089-3848-49e6-a5a3-b099e8411173","name":"6caa0089-3848-49e6-a5a3-b099e8411173","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"64e2d065-fbcd-4a43-ace8-ca2a098fea50","name":"64e2d065-fbcd-4a43-ace8-ca2a098fea50","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cc3be7d4-1100-4c90-a637-ffbda6b88ed6","name":"cc3be7d4-1100-4c90-a637-ffbda6b88ed6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"339c5737-ab30-44e7-b510-3e3ccf95786f","name":"339c5737-ab30-44e7-b510-3e3ccf95786f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7deb3387-c7f0-4593-83a0-77650cc32cde","name":"7deb3387-c7f0-4593-83a0-77650cc32cde","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7e9fff5b-b6e0-4e57-933d-9685fe4aafae","name":"7e9fff5b-b6e0-4e57-933d-9685fe4aafae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c58bac5c-f0d0-4ab2-bab9-b2ad94fee2cd","name":"c58bac5c-f0d0-4ab2-bab9-b2ad94fee2cd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"196ba8c4-786e-4cce-8c5e-c54161e095f3","name":"196ba8c4-786e-4cce-8c5e-c54161e095f3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"24cdcb16-5296-429f-a75e-e3421ed5f4d1","name":"24cdcb16-5296-429f-a75e-e3421ed5f4d1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"947e42fe-faa7-4208-a7a9-04cd431fac1a","name":"947e42fe-faa7-4208-a7a9-04cd431fac1a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3e08f5c9-5024-44ce-9c23-5d0c9a99c80b","name":"3e08f5c9-5024-44ce-9c23-5d0c9a99c80b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":49,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"2c67fc6d-1161-4b91-9630-e4edb010701a","blendMode":0,"displayName":"default","isLocked":false,"name":"2c67fc6d-1161-4b91-9630-e4edb010701a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant15DeadOld",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Atom",
    "path":"folders/Sprites/Player/Custom/Atom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant15DeadOld",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":13.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant15DeadOld",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4d2a4a29-ee1d-4b72-907e-2e8e50a0cbc3","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dbbd3a52-e9b0-4f37-b21b-a8ff81c5641c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1b6975e4-a3a8-43da-8fb7-e20692531c54","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"de9b37cb-b704-453a-adf6-2a98d303c5ee","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6caa0089-3848-49e6-a5a3-b099e8411173","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bc7ec4f0-ba75-4db5-bec2-b33127778ce1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"64e2d065-fbcd-4a43-ace8-ca2a098fea50","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e6548088-b6e5-4053-98f1-8c601bb69f32","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc3be7d4-1100-4c90-a637-ffbda6b88ed6","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c1da2992-3f36-4868-820b-b51e36d593d7","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"339c5737-ab30-44e7-b510-3e3ccf95786f","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed2231df-7621-4742-8f62-08cca137c0b9","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7deb3387-c7f0-4593-83a0-77650cc32cde","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"89ade525-f5d1-46c1-9abd-29782f4a37c2","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7e9fff5b-b6e0-4e57-933d-9685fe4aafae","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5c1af69f-1878-4d74-af6d-a7842a923db6","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c58bac5c-f0d0-4ab2-bab9-b2ad94fee2cd","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5c504bde-11e0-4a16-ba78-dae9f3cae413","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"196ba8c4-786e-4cce-8c5e-c54161e095f3","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3a04da8b-fc23-4c76-b118-e5be877ff33a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"24cdcb16-5296-429f-a75e-e3421ed5f4d1","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d2c80b8a-b206-45eb-bc6d-65accceda54c","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"947e42fe-faa7-4208-a7a9-04cd431fac1a","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9f8e0040-068a-4a37-b6c7-4fd27abfb406","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3e08f5c9-5024-44ce-9c23-5d0c9a99c80b","path":"sprites/sprMutant15DeadOld/sprMutant15DeadOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f6335160-7274-4f16-9544-48d34db0c49e","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":23,
    "yorigin":28,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":48,
}