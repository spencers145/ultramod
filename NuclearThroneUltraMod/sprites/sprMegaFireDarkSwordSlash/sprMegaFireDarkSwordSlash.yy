{
  "$GMSprite":"",
  "%Name":"sprMegaFireDarkSwordSlash",
  "bboxMode":0,
  "bbox_bottom":103,
  "bbox_left":8,
  "bbox_right":101,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"5789269c-b939-4d7f-a5b9-467936fa5f2d","name":"5789269c-b939-4d7f-a5b9-467936fa5f2d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6927732b-51bc-442e-8a40-5fa977875df4","name":"6927732b-51bc-442e-8a40-5fa977875df4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"28f7da69-2bc4-4762-8fb6-74922764e0a8","name":"28f7da69-2bc4-4762-8fb6-74922764e0a8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d23c256c-43a2-4538-80e3-4bb749e9b15a","name":"d23c256c-43a2-4538-80e3-4bb749e9b15a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":104,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5c833306-5bad-48c8-9282-0522a76a2d68","blendMode":0,"displayName":"default","isLocked":false,"name":"5c833306-5bad-48c8-9282-0522a76a2d68","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMegaFireDarkSwordSlash",
  "nineSlice":null,
  "origin":3,
  "parent":{
    "name":"Melee Atacks",
    "path":"folders/Sprites/Projectiles/Melee Atacks.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMegaFireDarkSwordSlash",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMegaFireDarkSwordSlash",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5789269c-b939-4d7f-a5b9-467936fa5f2d","path":"sprites/sprMegaFireDarkSwordSlash/sprMegaFireDarkSwordSlash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d993992c-b388-4c67-bf4a-bf771d6f62a6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6927732b-51bc-442e-8a40-5fa977875df4","path":"sprites/sprMegaFireDarkSwordSlash/sprMegaFireDarkSwordSlash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cbebfd76-0482-4a53-bfe8-6d7de1b7ff7b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"28f7da69-2bc4-4762-8fb6-74922764e0a8","path":"sprites/sprMegaFireDarkSwordSlash/sprMegaFireDarkSwordSlash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f8bd265f-198d-4e1e-ae55-fca9f455cee4","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d23c256c-43a2-4538-80e3-4bb749e9b15a","path":"sprites/sprMegaFireDarkSwordSlash/sprMegaFireDarkSwordSlash.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"97585cd2-feb0-47a5-8e5c-2eb1e527566b","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":52,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":102,
}