{
  "$GMSprite":"",
  "%Name":"sprInvertedNuclearPillar",
  "bboxMode":0,
  "bbox_bottom":44,
  "bbox_left":8,
  "bbox_right":39,
  "bbox_top":6,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"f2653606-892e-4b4d-b3f1-10e879ad73fb","name":"f2653606-892e-4b4d-b3f1-10e879ad73fb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"217daa2a-69d7-4a47-b488-359b19688b16","name":"217daa2a-69d7-4a47-b488-359b19688b16","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"175ea82c-a3ff-4201-827e-1fe821432bff","name":"175ea82c-a3ff-4201-827e-1fe821432bff","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b41e63d6-c8ed-4f87-8971-de5e504bbfa1","name":"b41e63d6-c8ed-4f87-8971-de5e504bbfa1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7d3fc9b9-e5b6-40e7-a859-f5ba3234fd9e","name":"7d3fc9b9-e5b6-40e7-a859-f5ba3234fd9e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fff068a1-bdf1-4a93-8de2-a9fc560a9b68","name":"fff068a1-bdf1-4a93-8de2-a9fc560a9b68","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dd08af7b-d041-4925-8f55-489f9e02c4d8","name":"dd08af7b-d041-4925-8f55-489f9e02c4d8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d7682aa2-9457-4e98-b253-29090a4e333e","name":"d7682aa2-9457-4e98-b253-29090a4e333e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c519d950-2f0d-45f4-abbf-0cfcb9642f91","name":"c519d950-2f0d-45f4-abbf-0cfcb9642f91","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cc393e1c-a872-43ad-ad8d-028047a90636","name":"cc393e1c-a872-43ad-ad8d-028047a90636","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2504245-c49b-4b38-b319-bc40ab3cd5bf","name":"d2504245-c49b-4b38-b319-bc40ab3cd5bf","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"95306261-c560-409c-a08d-0f4ae1e146e9","blendMode":0,"displayName":"default","isLocked":false,"name":"95306261-c560-409c-a08d-0f4ae1e146e9","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedNuclearPillar",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"InvertedPalace",
    "path":"folders/Sprites/Palace/PalaceEnviroment/InvertedPalace.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedNuclearPillar",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedNuclearPillar",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f2653606-892e-4b4d-b3f1-10e879ad73fb","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0c1d2c75-96f5-4adf-8c67-1c55977fe5ff","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"217daa2a-69d7-4a47-b488-359b19688b16","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2088b76d-d52d-491f-8df2-1d0dbed63c94","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"175ea82c-a3ff-4201-827e-1fe821432bff","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d71967a6-2692-47bb-8b02-7f045ef72597","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b41e63d6-c8ed-4f87-8971-de5e504bbfa1","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"23a94578-c727-458c-888b-44885ae71165","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7d3fc9b9-e5b6-40e7-a859-f5ba3234fd9e","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"617e6761-081d-4615-b3e4-1067f50a7a5d","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fff068a1-bdf1-4a93-8de2-a9fc560a9b68","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2f66ff97-375e-4dcb-a529-8664414dcc4c","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dd08af7b-d041-4925-8f55-489f9e02c4d8","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1fcf6490-fadd-4a9d-a3a5-ed865ccabfa9","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d7682aa2-9457-4e98-b253-29090a4e333e","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b766b527-fbf8-43c1-b941-a99fad5d750a","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c519d950-2f0d-45f4-abbf-0cfcb9642f91","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d3be1fc1-5f33-4660-9f75-56316a7ff5f2","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc393e1c-a872-43ad-ad8d-028047a90636","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fa380e4e-4ab0-4994-8838-bcadd49373bc","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2504245-c49b-4b38-b319-bc40ab3cd5bf","path":"sprites/sprInvertedNuclearPillar/sprInvertedNuclearPillar.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dff18798-37a5-4f5b-848b-f2947ee569a5","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Palace",
    "path":"texturegroups/Palace",
  },
  "type":0,
  "VTile":false,
  "width":48,
}