{
  "$GMSprite":"",
  "%Name":"sprMutant16Walk",
  "bboxMode":1,
  "bbox_bottom":25,
  "bbox_left":0,
  "bbox_right":21,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"d1ce9541-a4cd-4026-b29a-0352797281c9","name":"d1ce9541-a4cd-4026-b29a-0352797281c9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"68610619-db83-463c-8bf7-339972a45b65","name":"68610619-db83-463c-8bf7-339972a45b65","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"08430826-c06b-4cb3-b876-b414ef98ed68","name":"08430826-c06b-4cb3-b876-b414ef98ed68","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"be7218e0-ecf1-4da9-98c9-7d08403821cb","name":"be7218e0-ecf1-4da9-98c9-7d08403821cb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"13e372ad-d097-4752-8039-4434fba840f2","name":"13e372ad-d097-4752-8039-4434fba840f2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"24086bdd-fc7c-4356-aa7f-f06f864943a4","name":"24086bdd-fc7c-4356-aa7f-f06f864943a4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":26,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"fe8c6fd8-b602-4e34-8cfc-6c5e12178071","blendMode":0,"displayName":"default","isLocked":false,"name":"fe8c6fd8-b602-4e34-8cfc-6c5e12178071","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant16Walk",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Viking",
    "path":"folders/Sprites/Player/Custom/Viking.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant16Walk",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant16Walk",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d1ce9541-a4cd-4026-b29a-0352797281c9","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"99f6a792-9ce0-4071-865f-183dadd84999","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"68610619-db83-463c-8bf7-339972a45b65","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"052f8215-4f3f-4b23-b239-ac2a84ec35bb","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"08430826-c06b-4cb3-b876-b414ef98ed68","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2628eb43-0b12-4b9f-a647-458ca7f52cc1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"be7218e0-ecf1-4da9-98c9-7d08403821cb","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2ffe62c1-797d-44d3-928f-e59da4b0ff3b","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"13e372ad-d097-4752-8039-4434fba840f2","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ce308c44-29cf-4dda-be45-5bcd485d6222","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"24086bdd-fc7c-4356-aa7f-f06f864943a4","path":"sprites/sprMutant16Walk/sprMutant16Walk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6baca48f-89a9-490d-86da-a1510051482a","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":11,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":22,
}