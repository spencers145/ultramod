{
  "$GMSprite":"",
  "%Name":"sprThroneStatue",
  "bboxMode":0,
  "bbox_bottom":39,
  "bbox_left":4,
  "bbox_right":19,
  "bbox_top":8,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"7d74654c-a497-4fa6-bee2-22e6b264c638","name":"7d74654c-a497-4fa6-bee2-22e6b264c638","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"743ab362-cf25-4e73-9a8d-ea2bedf7d70e","name":"743ab362-cf25-4e73-9a8d-ea2bedf7d70e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0f78a0f8-d4df-4fe4-a53e-e1e2d9011c21","name":"0f78a0f8-d4df-4fe4-a53e-e1e2d9011c21","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2dd11e5a-2f6d-4221-a4a2-987799e6abd3","name":"2dd11e5a-2f6d-4221-a4a2-987799e6abd3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cabafd57-5e20-44d0-b722-9370522b7b09","name":"cabafd57-5e20-44d0-b722-9370522b7b09","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0d9c0dea-ce5b-442b-bfd2-73d05a994733","name":"0d9c0dea-ce5b-442b-bfd2-73d05a994733","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0fcfab66-4250-4dab-9d29-69704117ad3c","name":"0fcfab66-4250-4dab-9d29-69704117ad3c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3cfeeec4-b9ba-4ccb-be31-793870c5cae6","name":"3cfeeec4-b9ba-4ccb-be31-793870c5cae6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9ffb7a03-d130-42b6-9f31-668b4146d5e7","name":"9ffb7a03-d130-42b6-9f31-668b4146d5e7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dfdbeda1-66a7-457a-8f65-3a0ea16b877d","name":"dfdbeda1-66a7-457a-8f65-3a0ea16b877d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"037e2a38-8af4-4df1-a5a8-59bbe5e25c1a","name":"037e2a38-8af4-4df1-a5a8-59bbe5e25c1a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a07eaf90-19dd-4a96-889c-edb0442c59b3","name":"a07eaf90-19dd-4a96-889c-edb0442c59b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e77d3c28-f29e-456c-8357-2b6b05362062","name":"e77d3c28-f29e-456c-8357-2b6b05362062","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7dcdec73-2b05-45ab-9c7b-fe83351a1fb6","name":"7dcdec73-2b05-45ab-9c7b-fe83351a1fb6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"532ee959-fed6-4edf-94c4-0b8ab2d975d1","blendMode":0,"displayName":"default","isLocked":false,"name":"532ee959-fed6-4edf-94c4-0b8ab2d975d1","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprThroneStatue",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Throne1",
    "path":"folders/Sprites/Palace/Palace Enemy/TheThrone/Throne1.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprThroneStatue",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":14.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprThroneStatue",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7d74654c-a497-4fa6-bee2-22e6b264c638","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"212db1cb-5063-4b7d-97cf-5fecda284e29","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"743ab362-cf25-4e73-9a8d-ea2bedf7d70e","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9fbc01bc-ce27-4869-9fed-2cccf9ac7f4f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0f78a0f8-d4df-4fe4-a53e-e1e2d9011c21","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3968db5c-3a6b-4864-8240-d98e51aee44c","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2dd11e5a-2f6d-4221-a4a2-987799e6abd3","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8107ab5d-a174-468a-8133-3fee18cf6bc5","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cabafd57-5e20-44d0-b722-9370522b7b09","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6cc8bebb-07bf-4e08-831f-bf243f9a8e69","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0d9c0dea-ce5b-442b-bfd2-73d05a994733","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b005a8ee-3836-4816-bfde-f7cdad059df1","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0fcfab66-4250-4dab-9d29-69704117ad3c","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cb32ac4b-0848-43a5-a93e-46bed86d1892","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3cfeeec4-b9ba-4ccb-be31-793870c5cae6","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c61a84e9-c17b-4fc2-9ca6-366b651fd5f9","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9ffb7a03-d130-42b6-9f31-668b4146d5e7","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af837f9f-513a-4b8c-ab51-b123106ee645","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dfdbeda1-66a7-457a-8f65-3a0ea16b877d","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bb36c662-14bd-4770-a780-fa37bf5d8876","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"037e2a38-8af4-4df1-a5a8-59bbe5e25c1a","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b5436bb5-ee73-4872-9ff9-7bb57f49c9be","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a07eaf90-19dd-4a96-889c-edb0442c59b3","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3d2e5535-5114-48f0-8e55-ee9f63cecd98","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e77d3c28-f29e-456c-8357-2b6b05362062","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cda356b2-353a-40ca-a0d7-5af045775e74","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7dcdec73-2b05-45ab-9c7b-fe83351a1fb6","path":"sprites/sprThroneStatue/sprThroneStatue.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a2508a1f-41a6-4894-be46-237f2bdfaff6","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Palace",
    "path":"texturegroups/Palace",
  },
  "type":0,
  "VTile":false,
  "width":24,
}