{
  "$GMSprite":"",
  "%Name":"sprHotHandsTryout",
  "bboxMode":0,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"0ea53807-2809-4310-9894-a64ce9e1a08e","name":"0ea53807-2809-4310-9894-a64ce9e1a08e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"33d4a824-d337-4bd0-b765-3a0e02a7e653","name":"33d4a824-d337-4bd0-b765-3a0e02a7e653","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3fddbfa8-6fe4-4e8d-ad09-386cc576e821","name":"3fddbfa8-6fe4-4e8d-ad09-386cc576e821","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3ad3007f-3b36-4668-95e2-b8848e769213","name":"3ad3007f-3b36-4668-95e2-b8848e769213","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"59bd2e88-4acc-411c-be3a-e3ea8fc3e7e1","name":"59bd2e88-4acc-411c-be3a-e3ea8fc3e7e1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"23d6d357-3477-4629-b22a-f0bc806ebbdd","name":"23d6d357-3477-4629-b22a-f0bc806ebbdd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1b69d190-86d3-4112-ae9b-06d31b9799d0","name":"1b69d190-86d3-4112-ae9b-06d31b9799d0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"251c2398-0c9a-4d30-9de9-dfe54457a6c0","name":"251c2398-0c9a-4d30-9de9-dfe54457a6c0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"099fdd4d-d24e-4160-9033-953fe8df5146","name":"099fdd4d-d24e-4160-9033-953fe8df5146","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"8fea9336-17fc-4a7b-a616-84f98fb56472","blendMode":0,"displayName":"default","isLocked":false,"name":"8fea9336-17fc-4a7b-a616-84f98fb56472","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"ce55eff5-d180-4705-bb7d-ba577d86d8e6","blendMode":0,"displayName":"default","isLocked":false,"name":"ce55eff5-d180-4705-bb7d-ba577d86d8e6","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprHotHandsTryout",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Ultras",
    "path":"folders/Sprites/Menu/Ultras.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprHotHandsTryout",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprHotHandsTryout",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0ea53807-2809-4310-9894-a64ce9e1a08e","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5669e6d2-bda6-4706-b220-018992237f17","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"33d4a824-d337-4bd0-b765-3a0e02a7e653","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7b0151ba-64a7-4a18-8ca5-6f0585d04569","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3fddbfa8-6fe4-4e8d-ad09-386cc576e821","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c761fa85-7fd6-40d4-bfe8-fe7d2ddef366","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3ad3007f-3b36-4668-95e2-b8848e769213","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0089ef24-6400-4957-981c-db48702a7a64","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"59bd2e88-4acc-411c-be3a-e3ea8fc3e7e1","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cff9189c-6dfb-4001-9696-526a6f09e1d1","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"23d6d357-3477-4629-b22a-f0bc806ebbdd","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"522d0224-1bc0-45d4-9aa1-d42a0c102a09","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1b69d190-86d3-4112-ae9b-06d31b9799d0","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"133b89b0-d2db-4f03-b131-2c156e7665f5","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"251c2398-0c9a-4d30-9de9-dfe54457a6c0","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f71e0884-00b3-4894-8c5f-a90ceac595fe","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"099fdd4d-d24e-4160-9033-953fe8df5146","path":"sprites/sprHotHandsTryout/sprHotHandsTryout.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"42eed063-eac9-40c2-8b2c-4b3835a088cc","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"MainMenu",
    "path":"texturegroups/MainMenu",
  },
  "type":0,
  "VTile":false,
  "width":24,
}