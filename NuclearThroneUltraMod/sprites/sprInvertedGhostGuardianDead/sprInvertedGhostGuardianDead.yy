{
  "$GMSprite":"",
  "%Name":"sprInvertedGhostGuardianDead",
  "bboxMode":0,
  "bbox_bottom":62,
  "bbox_left":0,
  "bbox_right":60,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"63dc1715-3e63-444e-9f4f-f8f4868b6ffa","name":"63dc1715-3e63-444e-9f4f-f8f4868b6ffa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d576e74e-620b-4803-885a-7bb63d9acdbb","name":"d576e74e-620b-4803-885a-7bb63d9acdbb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9f407672-1b68-46f7-a2a4-b07e0749cb6e","name":"9f407672-1b68-46f7-a2a4-b07e0749cb6e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1c9e252c-df29-403a-b851-21eee0683b92","name":"1c9e252c-df29-403a-b851-21eee0683b92","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"89d7e70b-57f0-4d41-9eee-ccec49f2d2de","name":"89d7e70b-57f0-4d41-9eee-ccec49f2d2de","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b0f3023c-1efe-4643-bef5-3a2ee9053e0c","name":"b0f3023c-1efe-4643-bef5-3a2ee9053e0c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"77259800-9901-4a15-84cb-dce9cd9f1c01","name":"77259800-9901-4a15-84cb-dce9cd9f1c01","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9bedf686-263e-428d-92c7-5bec1493bf00","name":"9bedf686-263e-428d-92c7-5bec1493bf00","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"ab1e6184-dd1b-4362-987f-4017496a400a","blendMode":0,"displayName":"default","isLocked":false,"name":"ab1e6184-dd1b-4362-987f-4017496a400a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedGhostGuardianDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"InvertedPalaceEnemies",
    "path":"folders/Sprites/Palace/Palace Enemy/InvertedPalaceEnemies.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedGhostGuardianDead",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedGhostGuardianDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"63dc1715-3e63-444e-9f4f-f8f4868b6ffa","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"71ee8526-c587-46e9-a67b-28179144e580","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d576e74e-620b-4803-885a-7bb63d9acdbb","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f6a2bb7c-8700-4b9c-a69d-0ed2091b3765","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9f407672-1b68-46f7-a2a4-b07e0749cb6e","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"34ac9621-4b05-4b65-ba8c-c75d7d52faa2","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1c9e252c-df29-403a-b851-21eee0683b92","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b225a71b-4f1a-4036-93ae-67fa47ce9bb8","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"89d7e70b-57f0-4d41-9eee-ccec49f2d2de","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"093d7078-ed0d-443e-a4dc-01ace91c74b3","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b0f3023c-1efe-4643-bef5-3a2ee9053e0c","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3a259772-3722-4937-95bc-c606244e566e","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"77259800-9901-4a15-84cb-dce9cd9f1c01","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dac847e8-41a7-419a-9236-de5978312bb0","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9bedf686-263e-428d-92c7-5bec1493bf00","path":"sprites/sprInvertedGhostGuardianDead/sprInvertedGhostGuardianDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c1619d34-05c8-4e2a-aa5b-60b119b5ce27","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":32,
    "yorigin":32,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":64,
}