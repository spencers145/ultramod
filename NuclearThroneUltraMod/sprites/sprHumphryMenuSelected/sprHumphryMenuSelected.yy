{
  "$GMSprite":"",
  "%Name":"sprHumphryMenuSelected",
  "bboxMode":0,
  "bbox_bottom":29,
  "bbox_left":10,
  "bbox_right":23,
  "bbox_top":10,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c1f6abd3-c6a6-4534-9c84-2be029de19d7","name":"c1f6abd3-c6a6-4534-9c84-2be029de19d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5c585bce-300f-41f8-b957-53b086281b2e","name":"5c585bce-300f-41f8-b957-53b086281b2e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ccf399d7-e1db-476b-9c8f-ccee712e81b3","name":"ccf399d7-e1db-476b-9c8f-ccee712e81b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b5682ecb-7547-4e37-84d6-87a4d989af2e","name":"b5682ecb-7547-4e37-84d6-87a4d989af2e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"547a2e27-8558-43f8-a261-400607871ff5","name":"547a2e27-8558-43f8-a261-400607871ff5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a21f2d46-6436-4e15-9e5a-3deb798bd9ce","name":"a21f2d46-6436-4e15-9e5a-3deb798bd9ce","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5208dab-c71f-4ab1-8565-4ed94c6f1179","name":"c5208dab-c71f-4ab1-8565-4ed94c6f1179","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"413f4eaa-f9b7-4ac6-92bb-380cb4db5fe1","name":"413f4eaa-f9b7-4ac6-92bb-380cb4db5fe1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":40,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"75780274-210a-4acc-a81c-801f1f6627a6","blendMode":0,"displayName":"default","isLocked":false,"name":"75780274-210a-4acc-a81c-801f1f6627a6","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprHumphryMenuSelected",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"MenuChar",
    "path":"folders/Sprites/Menu/MenuChar.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c1f6abd3-c6a6-4534-9c84-2be029de19d7","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"77bbf0ab-0bd2-46fc-98ef-98784c25ada1","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5c585bce-300f-41f8-b957-53b086281b2e","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b46e3b6a-1b77-4aa9-bf70-5b2d7bc1b19f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ccf399d7-e1db-476b-9c8f-ccee712e81b3","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fa0d55a2-f43f-474e-bcd3-f6058b32544a","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b5682ecb-7547-4e37-84d6-87a4d989af2e","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cb48dfaf-c4e8-473d-8696-2d192d47e475","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"547a2e27-8558-43f8-a261-400607871ff5","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5caeac44-14de-42b2-a1a5-5764365747ad","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a21f2d46-6436-4e15-9e5a-3deb798bd9ce","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"25db0e37-fa5b-4d34-bfe3-6d81f7426e62","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5208dab-c71f-4ab1-8565-4ed94c6f1179","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"761404c1-ec48-4390-855e-dbd89130998f","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"413f4eaa-f9b7-4ac6-92bb-380cb4db5fe1","path":"sprites/sprHumphryMenuSelected/sprHumphryMenuSelected.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"80424f44-3115-411d-8187-f21909e1c2d8","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":17,
    "yorigin":22,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":34,
}