{
  "$GMSprite":"",
  "%Name":"sprSuperBow",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":11,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"40943bcd-c9a0-404b-942f-d971d87a8cd7","name":"40943bcd-c9a0-404b-942f-d971d87a8cd7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c73f6b01-48bb-4857-8340-6d29b451c46c","name":"c73f6b01-48bb-4857-8340-6d29b451c46c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"13ef3a35-e98a-440b-9c26-6f44ce077c3c","name":"13ef3a35-e98a-440b-9c26-6f44ce077c3c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ce0ad110-f0af-404b-ac9b-f5826ff228b4","name":"ce0ad110-f0af-404b-ac9b-f5826ff228b4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"524c7a1c-c63f-4f89-b785-11c36b57e84a","name":"524c7a1c-c63f-4f89-b785-11c36b57e84a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3ff1c219-b820-4c82-b8be-1a88a83a9be5","name":"3ff1c219-b820-4c82-b8be-1a88a83a9be5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d3647b09-571e-424d-9deb-d8c9c78fcd14","name":"d3647b09-571e-424d-9deb-d8c9c78fcd14","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"13f9a81f-397f-4d51-92db-1751a8848c41","blendMode":0,"displayName":"default","isLocked":false,"name":"13f9a81f-397f-4d51-92db-1751a8848c41","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSuperBow",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Custom",
    "path":"folders/Sprites/Weapons/Custom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprSuperBow",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprSuperBow",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"40943bcd-c9a0-404b-942f-d971d87a8cd7","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a7e6d9cd-17b9-4f15-92c5-6f4ab109c2dc","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c73f6b01-48bb-4857-8340-6d29b451c46c","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"83b206e9-3681-499c-abb3-53bf037b4d69","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"13ef3a35-e98a-440b-9c26-6f44ce077c3c","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6b8b85d7-739f-44c8-9bdc-7682d48dd4d8","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ce0ad110-f0af-404b-ac9b-f5826ff228b4","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae91a7e6-3a23-43a2-b017-095469eb0431","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"524c7a1c-c63f-4f89-b785-11c36b57e84a","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e792077c-7590-40f9-a9c5-ad64614233a3","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3ff1c219-b820-4c82-b8be-1a88a83a9be5","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6041cd7c-3e1b-4671-a4a4-866ec68f8148","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d3647b09-571e-424d-9deb-d8c9c78fcd14","path":"sprites/sprSuperBow/sprSuperBow.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b2a7575a-2be3-47dd-b3dc-cd613063f1af","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":-3,
    "yorigin":11,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":12,
}