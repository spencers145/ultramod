{
  "$GMSprite":"",
  "%Name":"sprInvertedScrapBossHurtSpin",
  "bboxMode":1,
  "bbox_bottom":95,
  "bbox_left":0,
  "bbox_right":95,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"5b9d3bfb-0254-4a4d-81f0-a6a127e1288b","name":"5b9d3bfb-0254-4a4d-81f0-a6a127e1288b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f088bc5c-5548-413c-bfcd-eecc04c7cbd5","name":"f088bc5c-5548-413c-bfcd-eecc04c7cbd5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e52d31a2-de0e-4e16-9bcc-0afecb6bc7fa","name":"e52d31a2-de0e-4e16-9bcc-0afecb6bc7fa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":96,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"319afc11-c611-42c9-9eff-56943210ad8a","blendMode":0,"displayName":"default","isLocked":false,"name":"319afc11-c611-42c9-9eff-56943210ad8a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedScrapBossHurtSpin",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigDog",
    "path":"folders/Sprites/Enemies/Boss/BigDog.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5b9d3bfb-0254-4a4d-81f0-a6a127e1288b","path":"sprites/sprInvertedScrapBossHurtSpin/sprInvertedScrapBossHurtSpin.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"60b89bce-7777-4723-a0a6-a234b4ead56c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f088bc5c-5548-413c-bfcd-eecc04c7cbd5","path":"sprites/sprInvertedScrapBossHurtSpin/sprInvertedScrapBossHurtSpin.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"86469823-025b-419a-a8fe-75c88636cec5","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e52d31a2-de0e-4e16-9bcc-0afecb6bc7fa","path":"sprites/sprInvertedScrapBossHurtSpin/sprInvertedScrapBossHurtSpin.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ce031418-1be9-406d-bb94-c33dcd620cd3","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":48,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedScrapyard",
    "path":"texturegroups/InvertedScrapyard",
  },
  "type":0,
  "VTile":false,
  "width":96,
}