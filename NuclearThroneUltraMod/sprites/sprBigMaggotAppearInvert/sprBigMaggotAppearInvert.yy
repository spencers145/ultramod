{
  "$GMSprite":"",
  "%Name":"sprBigMaggotAppearInvert",
  "bboxMode":1,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"f8a76e9b-bdd9-4d9c-996d-3671ba309cd8","name":"f8a76e9b-bdd9-4d9c-996d-3671ba309cd8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8ea384c4-a593-469e-af7b-e6c5c5ee98d6","name":"8ea384c4-a593-469e-af7b-e6c5c5ee98d6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"763d32c0-183c-46a9-b68f-612732093f20","name":"763d32c0-183c-46a9-b68f-612732093f20","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f7aa9f73-6e4c-4205-b44f-1b0454ac4954","name":"f7aa9f73-6e4c-4205-b44f-1b0454ac4954","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"17563c73-0672-44bb-8245-339ed9fea6c7","name":"17563c73-0672-44bb-8245-339ed9fea6c7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f2f00ef3-3b7f-4d82-9bab-dc6520831445","name":"f2f00ef3-3b7f-4d82-9bab-dc6520831445","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"935c534b-5c20-490a-9a29-98b657a31984","name":"935c534b-5c20-490a-9a29-98b657a31984","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b19d57dc-5656-434a-95f6-d3f8b2473995","name":"b19d57dc-5656-434a-95f6-d3f8b2473995","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d397dcf5-2a56-451b-9569-babea1d3632f","name":"d397dcf5-2a56-451b-9569-babea1d3632f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"58dce23d-b0b7-4c2e-9b7d-0035e72ac77f","name":"58dce23d-b0b7-4c2e-9b7d-0035e72ac77f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"65faf3e6-9ddc-46b9-8855-c34180c19edf","name":"65faf3e6-9ddc-46b9-8855-c34180c19edf","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"41cfff2f-959d-4e81-8b17-0a25f7c08456","blendMode":0,"displayName":"default","isLocked":false,"name":"41cfff2f-959d-4e81-8b17-0a25f7c08456","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprBigMaggotAppearInvert",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BigMaggots",
    "path":"folders/Sprites/Enemies/Maggots/BigMaggots.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f8a76e9b-bdd9-4d9c-996d-3671ba309cd8","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ccca3782-e283-4f7d-a278-756decca6441","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8ea384c4-a593-469e-af7b-e6c5c5ee98d6","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"110d35db-7d4d-449d-9bef-ab2895e3c747","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"763d32c0-183c-46a9-b68f-612732093f20","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95580957-99db-4346-8c88-358f8ff8fed7","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f7aa9f73-6e4c-4205-b44f-1b0454ac4954","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0d251255-988a-4d68-b45f-ae4720e0d090","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"17563c73-0672-44bb-8245-339ed9fea6c7","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6f0c1601-e5d7-4f9a-b8a4-db962c310889","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f2f00ef3-3b7f-4d82-9bab-dc6520831445","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1fa00387-e3e5-4ea0-93be-848bbc011f39","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"935c534b-5c20-490a-9a29-98b657a31984","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"178fad1d-c73b-4acd-9ed1-2162b514ee75","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b19d57dc-5656-434a-95f6-d3f8b2473995","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae7598ea-9982-4794-aa11-e760d53f7c38","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d397dcf5-2a56-451b-9569-babea1d3632f","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cceff2fe-214a-4939-b9dc-0fd8a8be1767","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"58dce23d-b0b7-4c2e-9b7d-0035e72ac77f","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e4e152a0-0ddd-4892-88db-284c054a0f63","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"65faf3e6-9ddc-46b9-8855-c34180c19edf","path":"sprites/sprBigMaggotAppearInvert/sprBigMaggotAppearInvert.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7c09a3e3-02d7-4851-9b9a-35a0ccb13b21","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedDesert",
    "path":"texturegroups/InvertedDesert",
  },
  "type":0,
  "VTile":false,
  "width":32,
}