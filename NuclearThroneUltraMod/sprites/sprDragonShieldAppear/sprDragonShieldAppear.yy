{
  "$GMSprite":"",
  "%Name":"sprDragonShieldAppear",
  "bboxMode":2,
  "bbox_bottom":33,
  "bbox_left":-2,
  "bbox_right":27,
  "bbox_top":-3,
  "collisionKind":3,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"7853021a-7089-4a53-b319-6d79d4388548","name":"7853021a-7089-4a53-b319-6d79d4388548","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":31,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"64d0a591-b748-4b28-892d-db16e7c71470","blendMode":0,"displayName":"default","isLocked":false,"name":"64d0a591-b748-4b28-892d-db16e7c71470","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprDragonShieldAppear",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"HotDrake",
    "path":"folders/Sprites/Enemies/Boss/HotDrake.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprDragonShieldAppear",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprDragonShieldAppear",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7853021a-7089-4a53-b319-6d79d4388548","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"85f9cea3-5875-45ca-99f5-d2d095bfdaa3","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9a96f3bd-9a22-4455-a349-c3dbcba4754c","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8767d7cf-9fc3-4e40-81c6-9f1ed19a7a0c","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fbff0e53-e7a6-4473-86b2-a746fbee091c","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95cf26a2-cb1d-489c-b8a1-1051a714d5aa","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a57bbd80-f4f9-4939-af4f-1619aa43c5f5","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3c3d1f17-cada-4098-bde3-4469da8151aa","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d61e5ba0-66ee-4e5f-a64a-e4c8b15b6d64","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"33fb10a0-3cf0-4bad-a70c-2d017b302c16","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6aec0bcf-e642-4c08-98be-037b3f5d6ffd","path":"sprites/sprDragonShieldAppear/sprDragonShieldAppear.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4b0ba94b-6c28-424e-b1c4-45f376371b9e","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":15,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":26,
}