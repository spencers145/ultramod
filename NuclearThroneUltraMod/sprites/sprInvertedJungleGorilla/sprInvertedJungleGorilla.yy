{
  "$GMSprite":"",
  "%Name":"sprInvertedJungleGorilla",
  "bboxMode":0,
  "bbox_bottom":44,
  "bbox_left":9,
  "bbox_right":41,
  "bbox_top":15,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"1b92ecfd-2fce-4332-9692-76428cd94372","name":"1b92ecfd-2fce-4332-9692-76428cd94372","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ef5ecfb-9e62-4535-8ae4-5b20f0616316","name":"4ef5ecfb-9e62-4535-8ae4-5b20f0616316","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ce7bc29e-99bc-4c2f-895b-178d86494b79","name":"ce7bc29e-99bc-4c2f-895b-178d86494b79","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"be0fbc6c-b7ff-4f31-9ccf-a990676f3b95","name":"be0fbc6c-b7ff-4f31-9ccf-a990676f3b95","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"520aa8e2-30a6-40fc-ac2d-59fb58daff8d","name":"520aa8e2-30a6-40fc-ac2d-59fb58daff8d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"43f1d5ea-d89c-4951-8775-e448b9070d49","blendMode":0,"displayName":"default","isLocked":false,"name":"43f1d5ea-d89c-4951-8775-e448b9070d49","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedJungleGorilla",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"JungleEnemy",
    "path":"folders/Sprites/Enemies/JungleEnemy.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedJungleGorilla",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":5.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedJungleGorilla",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1b92ecfd-2fce-4332-9692-76428cd94372","path":"sprites/sprInvertedJungleGorilla/sprInvertedJungleGorilla.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"70e62688-0bfb-486f-a6e9-2d28c567283f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ef5ecfb-9e62-4535-8ae4-5b20f0616316","path":"sprites/sprInvertedJungleGorilla/sprInvertedJungleGorilla.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6bdb830d-e813-4b90-b183-f57345ad1827","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ce7bc29e-99bc-4c2f-895b-178d86494b79","path":"sprites/sprInvertedJungleGorilla/sprInvertedJungleGorilla.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3e320bde-7861-4265-91be-76fc354de0af","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"be0fbc6c-b7ff-4f31-9ccf-a990676f3b95","path":"sprites/sprInvertedJungleGorilla/sprInvertedJungleGorilla.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"52170625-51cb-4827-9a08-ca0574712a08","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"520aa8e2-30a6-40fc-ac2d-59fb58daff8d","path":"sprites/sprInvertedJungleGorilla/sprInvertedJungleGorilla.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e1e6b32-bd49-41c5-8765-aa0585165a06","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":35,
  },
  "swatchColours":[
    4278190335,4278255615,4278255360,4294967040,4294901760,4294902015,4294967295,4293717228,4293059298,4292335575,
    4291677645,4290230199,4287993237,4280556782,4278252287,4283540992,4293963264,4287770926,4287365357,4287203721,
    4286414205,4285558896,4284703587,4283782485,4281742902,4278190080,4286158839,4286688762,4287219453,4288280831,
    4288405444,4288468131,4288465538,4291349882,4294430829,4292454269,4291466115,4290675079,4290743485,4290943732,
    4288518390,4283395315,4283862775,4284329979,4285068799,4285781164,4285973884,4286101564,4290034460,4294164224,
    4291529796,4289289312,4289290373,4289291432,4289359601,4286410226,4280556782,4280444402,4280128760,4278252287,
    4282369933,4283086137,4283540992,4288522496,4293963264,4290540032,4289423360,4289090560,4287770926,4287704422,
    4287571858,4287365357,4284159214,4279176094,4279058848,4278870691,4278231211,4281367321,
  ],
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":48,
}